infix 69 ++ 68
infix 79 * 80

type Tree
= a . T a (Forest a)

type Forest
= a . F (List (Tree a))

type Mu
= f . a . Inn (f a (Mu f a))

type L
= a . b . (N | L a b)

type Flip
= a . b . (Flop a b (Flip b a))

type Big
= a . (One a | Many (Big (a * a)))
!--
type NewTriple 
= a . b . c . (a ; b) c
--!

type Fix
= a . Fix (Fix a -> a)

apply
= (Fix f) . x . f x

selfapply
= x . apply x x

fix
= f . { h = x . f (selfapply x) } h (Fix h)

last
= fix
  $ last . xs . run
  $ xs => (z , zs) ? (zs => Nil ? return z | return (last zs))

alternate
= x . y . x , alternate' x y

alternate'
= x . y . y , alternate x y
