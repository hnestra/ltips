infix 5 $ 0
infix 12 => 13
infix 12 >> 13
infix 13 =< 12
infix 13 << 12
infix 21 ; 20
infix 29 , 28
infix 37 || 36
infix 45 && 44
infix 60 == 60
infix 60 /= 60
infix 60 <= 60
infix 60 >= 60
infix 60 < 60
infix 60 > 60
infix 65 ++ 64
infix 68 + 69
infix 68 - 69
infix 76 * 77
infix 76 / 77
infix 76 % 77
infix 85 ^ 84

type Bool 
= False | True

type List
= a . 
  ( Nil 
  | a , List a
  )

type (*)
= a . b . a ; b

type Triple
= a . b . c . Triple a b c

run = primRunExn
return = primRetExn
(>>) = primBndExn
(|) = primElsExn

(+) = primAddInt
(-) = primSubInt
(*) = primMulInt
abs = primAbsInt

-------------------------------------------------------------------

fn
= f . x . run (f x)

($)
= f . x . f x

(=>)
= x . f . f x

id
= x . x

const
= x . y . x

diag
= h . x . h x x

fix
= f . { x = f x } x

div
= x . y . run
  $ primDivInt x y

signum
= n . run
  $ primDivInt n (abs n) 
  | return 0

not
= b . run
  $ b => True ? return False 
  | return True

(&&)
= a . b . run
  $ a => False ? return False
  | return b

(||)
= a . b . run
  $ a => True ? return True
  | return b

(==)
= x . y . run
  $ primDivInt 0 (y - x) >> _ ? return False
  | return True

(<)
= x . y .
  signum (y - x) == 1

(<=)
= x . y .
  x < y || x == y

even
= n . { q = div n 2 } q * 2 == n

odd
= n . not (even n)

head
= (x , xs) . x

tail
= (x , xs) . xs

(++)
= xs . ys . run 
  $ xs => (a , as) ? return (a , as ++ ys)
  | return ys

map
= f . xs . run
  $ xs => (z , zs) ? return (f z , map f zs)
  | return Nil
!--
-- Alternative:
map
= f . fix
  $ map . xs . run
  $ xs => (z , zs) ? return (f z , map zs)
  | return Nil
--!
dropWhile
= p . xs . run
  $ xs => (z , zs) ? p z => True ? return (dropWhile p zs)
  | return xs
!--
-- Alternative:
dropWhile
= fn p ? return $
  fn xs ?
  ( xs => (z , zs) ? p z => True ? return (dropWhile p zs)
  | return xs
  )
--!
takeWhile
= p . xs . run
  $ xs => (z , zs) ? p z => True ? return (z , takeWhile p zs)
  | return Nil

reverse
= Nil => fix 
  ( rev . us . vs . run
    $ vs => (a , as) ? return (rev (a , us) as)
    | return us
  )

repeat
= x . { xs = x , xs } xs

iterate
= f . x . { zs = x , map f zs } zs
!--
-- Alternative:
iterate
= f . x . x , iterate f (f x)
--!
from
= iterate ((+) 1)

fromTo
= n . m . takeWhile (k . not (k == m)) (from n)

init
= xs . reverse (tail (reverse xs))

last
= xs . run
  $ xs => (z , zs) ? (zs => Nil ? return z | return (last zs))

take
= n . xs . run
  $ 0 < n => True ? xs => (z , zs) ? return (z , take (n - 1) zs)
  | return Nil

fac
= n . run
  $ 0 < n => True ? return (n * fac (n - 1))
  | return 1

pow
= a . n . { 
    q = div n 2
    r = n - 2 * q
    h = pow a q 
  } run
  $ n == 0 => True ? return 1
  | r == 0 => True ? return (h * h)
  | return (a * h * h)
