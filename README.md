# λ-tips language documentation

## Motivation

The purpose of λ-tips has been to have a programming language that maximizes technical simplicity of the language with minimal penalty in usability. The original aim arose from a postgraduate course in semantics of programming languages that I led when I was working in the University of Tartu. I dreamed about something like a "functional While" which should have been the example language in teaching semantics of functional languages just like While is in teaching semantics of imperative languages. Textbooks typically use the plain lambda calculus for this purpose which I did not like as the lambda calculus too little resembles real programming languages. Teaching semantics of functional languages on the lambda calculus is like teaching semantics of imperative languages on Turing machines. One does not do it, one uses While whose structure is immediately recognized and close to the underlying structure of real programming languages. In my plans, the "functional While" should similarly have had an appearance close to real functional programming languages and enabled one to be frank about important aspects of the computation process such as graph reduction (which is regrettably renounced in all treatments of lambda calculus that I know).

To be sure that I know what I will be teaching, I decided to actually implement my language. I started with development but soon withdraw the original aim simply because I realized that functional languages would not have fit into the course due to the given number of hours of labour. Nevertheless, I continued developing the language during free hours (which I have had too seldom). The current state of the art is barely usable for the original purpose (I have not written down the semantics but certain parts of the source code can be used as such). Anyway, I am no more sure if my idea about teaching students functional programming semantics while being frank about graph reduction is reasonable indeed, or would it be too complicated for beginning.

## Usage

The source files have been tested with GHC version 9.8.4 and Stack version 3.1.1.

Install using Stack:

```ShellPrompt/ltips$ stack install```

Assume that the directory where Stack copies the executables is on the PATH. Then

```ShellPrompt/ltips$ ltips```

runs the REP loop. As there is no command line editor, Linux users might want to use `rlwrap` tool for convenient editing and tab completion:

```ShellPrompt/ltips$ rlwrap -Ac ltips```

In the REP loop, run `:help` for the list of available commands. All commands can be shortened down to the shortest prefix unique to the particular command.

The subdirectory `tps` contains some example files that one can load, for instance:

```
λ-tips> :l tps/prelude
Reading file tps/prelude.tps ... done
λ-tips prelude> 2+3+5
10
λ-tips prelude>
```

## Getting started

The technical simplicity is achieved by the following decisions:

* A few powerful and most essential syntactic constructs and built-in functions which cover all needs.

* No syntactic sugar if omitting it does not lose much usability/readability. In particular, functional left-hand side is not allowed.

* All expressions and patterns are built up using infix operators or constructs that can be seen as infix operators. Also prefix applications, anonymous functions and local declaration blocks are introduced by special binary infix constructs. Also branching is introduced via built-in functions and a special non-standard construct of anonymous functions. There are no `if` or `case` constructs, nor can a function be defined with multiple equations.

For an introductory example, consider the following definition of function `dropWhile` that occurs in `tps/prelude.tps`:

```
dropWhile
= p . xs . run
  $ xs => (z , zs) ? p z => True ? return (dropWhile p zs)
  | return xs
```

This function does the same as the function with the same name in the standard library of Haskell. Namely, it takes two arguments (denoted by `p` and `xs` in the code) and runs the following tests: If `xs` matches the pattern `(z , zs)` which denotes a list with head `z` and tail `zs` (a comma separates the head and the tail of a list), and, moreover, the value of `p` on the head `z` is true, then it returns the result of the recursive call of `dropWhile p` on the tail `zs`, otherwise it returns the whole list `xs`.

Let me now explain the definition in more details.

* First of all, the dots after `p` and `xs` in the second line introduce anonymous functions. A dot has basically the same meaning as in the lambda calculus, but one does not have to write lambda. Instead, the dot is treated as an infix operator that binds with maximum tightness in the left (even tigher than function application, so affecting only one variable or literal or a parenthesized pattern) but very lax in the right. In the example code, both dots scope until the end of the code in the right.
* Secondly, the code in lines 3-4 that looks like comprehension is actually built up from infix operator applications, too. Like the dot, the question mark separates the left-hand side and the right-hand side of a anonymous function. It also has exactly the same left and right binding power. So the first question mark scopes only the pattern `(z , zs)` in the left (which also explains why this pattern must have parentheses), but in the right, it scopes until the end of the line (more precisely, until the bar in the following line). Likewise, the second question mark scopes the pattern True in the left and the remaining part of the third line in the right.
* The arrow `=>` is an operator of postfix application defined in `prelude.tps`. So, in conclusion, the third line applies the anonymous function constructed by the first question mark to the list `xs` (which triggers matching of the list with the pattern `(z , zs)`). Similarly, the anonymous function constructed by the second question mark is applied to `p z` (note that juxtaposition between `p` and `z` binds tighter than the arrow) and this triggers matching of `p z` with `True`.
* The question mark has subtle semantic differences from the dot which actually enables backtracking and branching. The main difference arises if the function is applied to an argument that fails to match the pattern of the formal parameter. In the usual form (the dot), pattern match failure means that the function application results in the bottom value which breaks out to the topmost level and causes the whole execution to halt. In the non-standard form (the question mark), pattern match failure causes the value of the function application to be an exception that can be caught at any upper level. Due to this difference, functions constructed via the question mark cannot produce values of all types but only types of the form `Exn a`. This is the reason why `return` must be used, which is a built-in wrapper that can take arguments from any type `a` and turns them into values of the corresponding type `Exn a`. This way, the third line makes an insistent impression that it is a comprehension for the Maybe monad of Haskell but, as you can understand now, it is not. It is a proof that comprehension is not needed for writing readable code.
* The bar `|` is a built-in operator of type `Exn a -> Exn a -> Exn a` that selects the first normal value among its arguments. In other words, it equals `mplus` of the `Maybe` monad of Haskell.
* Finally, `run` is a built-in function that performs the type conversion opposite to `return`. It is necessary in our case since the computation provides a value of type `Exn (List a)` but we want to obtain a value of type `List a`. If the argument of `run` is exceptional, the value of `run` is bottom (and the exception can no more be caught). As you probably have guessed, the dollar `$` at the beginning of the third line has exactly the same meaning as in Haskell, i.e., it denotes a function application. It is defined in `prelude.tps`. Priorities are set in such a way that it scopes the whole remaining code in the right (even beyond bars).

This way, the usual big syntactic constructs like comprehensions, `case` expressions and guarded right-hand sides are eliminated, reducing the semantics of the language to the semantics of constructs and built-in operators that are at most binary.

One could wonder if we indeed need two distinct constructs for anonymous functions. The construct with the question mark creates functions of type `a -> Exn b`, which is subsumed by the general function type `a -> b`. So could we just use the dot for both? I preferred to distinguish the two forms of anonymous functions for two reasons:

* If the two forms are not distinguished then the evaluator must find out the right way of application according to type information. This violates universal parametricity (evaluation of functions with type parameters being independent on the actual types) which otherwise holds in my language.
* Having two distinguished forms helps the reader of the code to understand without tracking the types where are the exceptions that can be caught thrown and where are bottom values introduced.

However, we indeed could drop the dot and live with the question mark, if we had a built-in higher order function that was able to eliminate the `Exn` type from the right hand side. In the presence of the dot and the built-in function `run`, this function is definable (and the definition occurs in `prelude.tps`):

```
fn
= f . x . run (f x)
```

Using this function, `dropWhile` is definable without the dot (and also `run`) as follows:

```
dropWhile
= fn p ? return $
  fn xs ?
  ( xs => (z , zs) ? p z => True ? return (dropWhile p zs)
  | return xs
  )
```

This is not catastrophically more verbose than the previous definition of `dropWhile`. Much of the bare acceptability for me is due to the name `fn` which makes it look like a keyword that is commonly used for starting a function definition. Note that `fn` in both places applies to the whole anonymous function starting from the following variable, not just the variable alone, since the question mark binds more tightly in the left than juxtaposition!

I decided in favour of two forms of anonymous functions for the following reasons:

* The dot form is used at the type level already. Clearly the variant with the question mark makes no sense at the type level.
* The evaluator code, which also pretends to present the semantics, would essentially define both variants of function applications even if the standard form did not exist in the syntax. It seems to me that the current set of primitive constructs and operations leads to the easiest representation of the semantics.

One can have local declarations in curly braces:
```
iterate
= f . x . { zs = x , map f zs } zs
```
In this example, the result list is defined recursively in the local declaration block.

Currently, the implementation of λ-tips has the following shortcomings that I have noticed:

* Definitions that rely on list recursion like the `iterate` example above work in quadratic time although they should be linear. I will try to find and fix the bug when I will have free time again.
* The REP loop fails to output infinite lists. This touches upon printing only. Printing of the result of an evaluation starts after completing the evaluation. Infinite lists can be used as intermediate results if they are never completely evaluated, for instance:
```
λ-tips prelude> take 10 (iterate ((*) 2) 1)
1 , 2 , 4 , 8 , 16 , 32 , 64 , 128 , 256 , 512 , Nil
λ-tips prelude> 
```
* There is no tracing of function calls in the case of run time errors. Messages produced in the case of run time errors either completely fail to point to any locations of code or do not specify the code file.

## General

Basically, the language is the λ-calculus plus types plus constants (built-ins) plus patterns plus explicit substitutions (local declarations) whose usability is increased by indentation and alignment rules, infix applications and a powerful priority system. The first letters of some of the main features (**[t]**ypes, **[i]**nfix operators, **[p]**atterns, explicit **[s]**ubstitutions) originally gave the language its name.

Other features include:

* Comments respect lexemes and comment nesting is allowed for both block comments and line comments.

  Advantages:
  * Commenting out a part of code doesn't interfere with string literals or any other comments. 
  * Commenting out a multi-line string is possible using line commenting.
  * Line commenting does not accidentally uncomment a block by abolishing its beginning delimiter.
 
  Drawbacks:
  * Requires lexing the whole file, even contents of comments. This assumes a somewhat higher level of correctness of comments than usually.
  * Uncommenting a block by line commenting the delimiters is impossible. Uncommenting is possible by erasing only.

* Local declaration blocks are written in curly braces. The braces do not mark the scope of the defined variables; their scope extend to everything that follows the block within the current subexpression. The extent of the subexpression is determined by parentheses and priorities. Curly braces replace the usual **`let`** and **`in`** keywords, saving two short names for being used for variables.

* Priority system allows unbalanced priorities, i.e., distinct left and right priorities. (Advocated by [Pratt [2]](https://dl.acm.org/doi/10.1145/512927.512931).) One cannot declare left and right associativity but the corresponding effects can be achieved by making the left or the right priority less than the other priority. Non-associativity is achieved when the left and right priorities are equal. Normal behaviour of anonymous functions without lambda at the beginning of the code is achieved by letting the left priority of the dot be very high and the right priority be very low. (Anonymous functions without lambda symbol were advocated by [Odersky et al. [1]](https://dl.acm.org/doi/10.1145/158511.158521).)

  Advantages:
  * One does not have to write lambdas and other unnecessary keywords;
  * Anonymous functions can be used to write code that looks like comprehension.
 
  Drawbacks:
  * Lambda dot looking like an ordinary operator may decrease readability in the case of unsuitable layout;
  * Using unbalanced priorities instead of comprehension may cause trouble with understanding how the code is actually parsed, especially since the expected layout of code resembles comprehension syntax and may give wrong suggestions. 

* There are two different constructs for anonymous functions. Their semantics differ by behaviour in the case of pattern mismatch. In one construct, pattern mismatch causes an irrefutable error (bottom value). In the case of the other construct, the pattern match exception is one possible outcome (the result type of the function must allow it) and can be caught elsewhere. 

The implementation consists of the following parts, corresponding to phases of static analysis performed during execution:

* Lexer - grouping characters of the input program to lexemes and classifying the lexemes.

* Decommenter - finding code comments and removing them.

* Parser - extracting declarations from the sequence of lexemes and expressing the left and right hand sides of equations as sequences of infix or prefix applications and parenthesized units.

* Disambiguator - finding out the application tree based on infix operator priorities from the infix and prefix application sequences.

* Developer - extracting the final abstract syntax tree (mainly finding out which application trees are expressions and which are patterns).

* Scope inferencer - checking definedness of names and deducing their scopes.

* Type inferencer - deducing type information for all expressions and patterns.

* Interpreter - evaluator of expressions.

In addition, the implementation contains a REP loop. 

The language uses lazy evaluation in the case of definitions, but arguments of functions are evaluated to barely match the pattern of the formal parameter before reducing the function application. So the language uses the same evaluation policy as Haskell, but it does not support as-patterns and has no means to force subpatterns irrefutable (like the tilde operator of Haskell).

## Lexer

* Legal names must consist of either letters and digits or operator characters like in Haskell. Underscore is considered as a lowercase letter.

* There are separate name spaces for data and type level. In the data level name space, distinction between variable and constructor names consisting of letters and digits is the same as in Haskell. In the case of names consisting of operator characters, those that start with colon, semicolon or comma denote constructors. In the type level name space, variable names cannot consist of operator characters and must start with lowercase. Other names denote constructors. Hence, e.g., * can identify a constructor at the type level and a variable at the data level.

* Non-standard characters are not allowed in the file (even in comments since they are lexed, too). In particular, tabulators are disallowed.

  Advantages:
  * Using tabulators may cause text editor showing the program differently from the compiler view. Programming in Haskell causes much headache for newbies because of that.

* Single dot separates the left-hand side and the right-hand side of a standard anonymous function like in the lambda calculus. Single question mark replaces the dot in anonymous function expressions with capability of recovering from pattern match errors. Single exclamation mark was reserved for a third kind of anonymous function where pattern match is performed by need (like with irrefutable patterns in Haskell) but it was decided to be dropped as its effect can be achieved in combination with local declarations.

* String literals may contain unescaped newline characters. If newline is backslash escaped, it is skipped (i.e., not included in the string value).
 
  Advantages:
  * Multi-line string encoding is easy, as well as encoding very long strings on multiple lines of code.

## Decommenter

* Line comments begin with two or more consecutive dashes that form a separate lexeme (like in Haskell). Block comments are delimited by tokens `!--` and `--!` that form separate lexemes.

* String literals may safely contain block comment end tokens. This is unlike in most languages where such a sequence of characters always ends the current comment.
 
* Line comments do not include the following newline character, unless the newline is in a string literal (either escaped or not). A line comment expands forward to the next newline that is not part of a string literal or a block comment. As a consequence, both quotes and comment beginning and ending tokens must be balanced in all comments. For example, line comments must not contain lonely end-of-comment tokens.

## Parser

* A program consists of a block of infix declarations, type definitions and equations in this order. All declarations, type definitions and equations in a block must be aligned below each other according to their first character. Other tokens, except equality signs separating the left-hand side and the right-hand side of equations, must occur farther in the right. The equality sign is allowed to occur in the same column as the first character of the equation.

* Infix declarations start with keyword **`infix`**, which is followed by an operator (without parentheses) between two integers. The integer given in the left becomes the left priority and that in the right becomes the right priority of this operator. Both priorities must be non-negative and less than 128.

* A type definition consists of a keyword **`type`** followed by a defining equation for a new type constructor. These definitions correspond to the Haskell declarations starting with **`data`**. There are no type synonyms. The syntax of type definitions is similar to standard Haskell but functional left-hand side is not allowed (the type parameters must be introduced in the right-hand side via the anonymous function syntax). GADTs are not supported.

* An equation consists of its left-hand side, equality sign, and the right-hand side. Both left-hand side and right-hand side must be sequences of infix applications (possibly containing 0 applications). Each operand in this sequence is itself a sequence of "arguments". More precisely, sequences of infix applications can be specified by the following grammar:

```
InfixSeq -> ( (LocalSeq ArgSeq)+ InfixOp )* (LocalSeq ArgSeq)+
LocalSeq -> (\{ (Block)> \})*
ArgSeq   -> Arg+
Arg      -> Atom 
          | \( InfixOpOtherThanLambdaDot \)
          | \( InfixSeq \)
```
 
(Backslash escaped characters stand for these characters in the target language. `(_)>` denotes an indented block w.r.t. the current indentation baseline to the right by at least 1 character.)

* Local declaration blocks cannot contain type definitions. Otherwise, local declaration blocks follow the same syntax rules as the whole program.

* There is currently no way of writing type signatures or type annotations.

* Names consisting of operator characters can be infix applied or, being surrounded by parentheses, prefix applied or used otherwise. Names consisting of letters and digits can not be infix applied.
 
  Advantages:
  * Preventing names consisting of letters from being infix applied removes a source of misunderstanding caused by choosing names whose usage in everyday life matches one preferred usage (either prefix or infix) but is counterintuitive when used in the other way. For instance, suppose we have a `copyTo` function copying the contents of one file to another. The prefix usage `copyTo file1 file2` suggests that the second file is copied to the first one, but an infix usage ``file1 `copyTo` file2`` suggests that the first file is copied to the second one. This feature is aimed to remove this ambiguity. Instead of infix application, one can use postfix application operator as in `file2 => copyTo file1`, which maintains the first argument lexically following the operator and therefore does not cause misleading intuitions.

## Disambiguator

* Extracting the operator application tree proceeds by Dijkstra's shunting yard algorithm. If two operators have priority clash, an error occurs. More precisely, a priority clash arises if the right priority of the operator immediately preceding a subexpression is equal to the left priority of the operator immediately following this subexpression. This is analogous to the situation of sequentially applied non-associative operators.

* The dot, the question mark and the exclamaiton mark all have the same priority pair. Juxtaposition that denotes local declaration block "application" has the same priority pair, too. The left priority is the highest possible, which is even higher than juxtaposition that denotes ordinary prefix function application. The right priority is very low but leaves space for defining operators with even lower priorities.
 
  Advantages: 
  * Maximally high left priority of lambda dot enables one to write `forall a . p a`, meaning `forall (a . p a)`. Very low right priority means that normally a lambda expression extends to the right until the end of the right-hand side. Nevertheless, one can define operators with lower priority, so for instance, the bar operator that separates alternatives can end the scope of a lambda expression.
 
  Drawbacks:
  * The dot with a higher priority than juxtaposition perhaps makes understanding the code harder for humans if this feature is exploited more than just in some standard circumstances.

* In one block, priorities of every infix operator can be declared at most once. In local blocks, priorities of the same operator can be redefined, no matter of whether this operator is itself redefined or not. Priorities associate to names and the scopes of declaration blocks, not to variables or constructors that the names refer to.

  Advantages:
  * Provides flexibility a programmer needs in the case of local operators.

  Drawbacks:
  * An ambiguous scope can arise from redefining priorities of a known operator to a value lower than the right priority of the dot. Ambiguity occurs if such an operator either ends a scope or does not end it depending on whether it is assumed itself to be in this scope or not. The implementation does not do anything special to resolve this ambiguity, the result is just how the shunting yard algorithm happens to behave. (This means basically that in the described case the scope is ended, so the left priority of the operator is that defined locally, while the operator itself stays outside this scope and its right priority is that of the outer scope.)

* Operators whose priorities are declared in a block do not have to be used in this block. On the other hand, priorities of every infix operator must be declared in either this block or a block whose scope extends to the place where the infix application occurs. Priorities of the dot, the question mark, the exclamation mark and juxtapositions do not have to and cannot be declared.

## Developer

* Infix applications and parenthesized operators possibly prefix applied can occur in both expressions and patterns.

* The left-hand side of an expression describing an anonymous function is a pattern, the left-hand side of a juxtaposition can be a declaration block. Otherwise, expressions consist of expressions and patterns consist of patterns. 

* A pattern is either a variable, a wildcard, or a constructor applied to 0 or more patterns. Literals, anonymous function constructors and local declaration blocks cannot occur in patterns.

* A wildcard cannot be used where an expression is expected.

* The left-hand side of a type definition must consist of one type constructor (parenthesized if it is an infix operator). The type parameters in the right-hand side can be either variables or wildcards. Anonymous function constructors other than the dot are not allowed in type definitions.

* Anonymous functions and local declaration blocks are not allowed in type expressions.

## Scope inferencer

* Any variable used in an expression must be defined and its scope must extend to the place it is used.

* Variable redefinition within the same scope is not allowed. This implies that a pattern must not contain a variable repeatedly and, likewise, a declaration block must not define a variable repeatedly. A nested local declaration block or a anonymous function may redefine variables defined outside because these syntactic units create local scopes.

* Scopes of infix declarations and variable definitions are treated independently. For instance, if a variable is redefined in a nested scope that contains no infix declaration for this variable then the priorities from the parent scope apply in the nested scope.

## Type inferencer

* For variables defined in anonymous functions or local declaration blocks, types are not generalized. This means that the type of a function defined locally is the same for all its uses within its scope, for one particular reading of the scope. (Not generalizing types of locally defined functions follows the proposal made by [Vytiniotis et al. [3]](https://dl.acm.org/doi/10.1145/1708016.1708023).)

* Global variables are treated differently. The declarations in a global declaration block are divided into mutually recursive groups of definitions: Within each group, types are not generalized, but the types of each group as a whole are generalized. For instance, the type of all occurrences of a recursive function in its definition (and in mutually recursive definitions if any) must be instantiated equally, while every occurrence outside its definition (or the group of mutually recursive definitions) is instantiated independently.

* Kind inference for type definitions follow the same principles.

## Separate modules

Declarations can be placed into separate files. Currently, there are no named modules. Contents of files are operated stackwise based on the order of loading/unloading.

The following rules apply to distributing code into separate files: Any declaration block or comment must be located entirely in one file. Each file can contain just one (global) declaration block.  Indentation and alignment in different files are independent. Scope of a name can extend to the subsequent files but never to previous files. Files loaded later can silently redefine names defined earlier, scope clashes never occur (they do if names are redefined within the same block/file). In this sense, file sequences act like nested declaration blocks. There is one exception: new type constructors can be introduced in subsequent files, although type constructors already defined or built in cannot be redefined. Nevertheless, type inference treats names defined at the outermost level as global regardless of the file where the definitions are placed.

## Future developments

I have dreamed about writing a compiler, extending the type system with type classes, and other extensions that would together make the language usable for programming in practice. There are examples in the history of computer science about programming languages like Pascal that was created for educational purposes but broke through to the world of software development. However, it is hard for me to find a motivation to start doing it. So I definitely want to fix the bugs I have found but I can give no other promises. When I first started developing λ-tips, an interpreter was my ultimate goal. This is now achieved (or will be after the bugs will be fixed).

## References

1. [Odersky, M., Rabin, D., Hudak, P.: *Call by name, assignment, and the lambda calculus.* In: Proceedings of the 20th ACM SIGPLAN-SIGACT symposium on Principles of Programming Languages, pp. 43-56 (1993)](https://dl.acm.org/doi/10.1145/158511.158521)
2. [Pratt, V.: *Top down operator precedence.* In: Proceedings of the 1st annual ACM SIGACT-SIGPLAN symposium on Principles of Programming Languages, pp. 41-51 (1973)](https://dl.acm.org/doi/10.1145/512927.512931)
3. [Vytiniotis, D., Peyton Jones, S., Schrijvers, T.: *`let` should not be generalized.* In: Proceedings of the 5th ACM SIGPLAN workshop on Types in Language Design and Implementation, pp. 39-50 (2010)](https://dl.acm.org/doi/10.1145/1708016.1708023)

