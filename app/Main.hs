module Main
  ( main
  ) where


import Control.Diapplicative
import UI


main
  :: IO (Either UIError ())
main
  = runSymmetric UI.start
