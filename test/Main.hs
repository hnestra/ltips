{-# LANGUAGE
      TypeOperators
    #-}
module Main where


import System.IO
import System.IO.Error
import System.Environment
import Control.Diapplicative
import Control.Diapplicative.Trans
import Data.Maybe
import Stack
import Address
import qualified Data.SetUtils as Set
import qualified Data.MapUtils as Map
import Syntax
import Lexer
import Decommenter
import Parser
import Disambiguator
import Developer
import ScopeInferencer
import TypeInferencer
import Primitives


type Map = Map.Map


testLexer
  :: (MonadFail m)
  => String -> m [Lexeme :@ Addr]
testLexer input
  = case fellLexerT (lexAll input) of
      Left e
        -> fail (show e)
      Right lexs
        -> return lexs

testDecommenter
  :: (MonadFail m)
  => [Lexeme :@ Addr] -> m [Lexeme :@ (Addr, Addr)]
testDecommenter input
  = case fst **> fellDecommenterT decommentAll input of
      Left e
        -> fail (show e)
      Right p
        -> return p

testParser
  :: (MonadFail m)
  => [Lexeme :@ (Addr , Addr)] -> m (ProgAST String [InfixElem :@ Addr] [InfixElem :@ Addr] [InfixElem :@ Addr] :@ Addr)
testParser input
  = let indentAnal = fellIndentT parseAll defaultRel (defaultRange , NoAlign) in
    case fst . fst . fst **> fellParserT indentAnal input of
      Left e
        -> fail (show e)
      Right p
        -> return p

testDisambiguator
  :: (MonadFail m)
  => ProgAST String [InfixElem :@ Addr] [InfixElem :@ Addr] [InfixElem :@ Addr] :@ Addr -> 
     m (ProgAST String (InfixTree :@ Addr) (InfixTree :@ Addr) (InfixTree :@ Addr) :@ Addr , [OpMap])
testDisambiguator input
  = case fellDisambiguatorT (disambiguateProg input) [primOpMap] of
      Left e
        -> fail (show e)
      Right p
        -> return p

testDeveloper
  :: (MonadFail m)
  => ProgAST String (InfixTree :@ Addr) (InfixTree :@ Addr) (InfixTree :@ Addr) :@ Addr ->
     m (ProgAST String (TyRHSAST String :@ Addr) (PatAST String :@ Addr) (ExprAST String :@ Addr) :@ Addr)
testDeveloper input
  = case fst **> fellDeveloperT (developProg input) of
      Left e
        -> fail (show e)
      Right p
        -> return p

testScopeInferencer
  :: (MonadFail m)
  => String -> 
     ProgAST String (TyRHSAST String :@ Addr) (PatAST String :@ Addr) (ExprAST String :@ Addr) :@ Addr -> 
     m ((ProgAST UId (TyRHSAST UId :@ Addr) (PatAST UId :@ Addr) (ExprAST UId :@ Addr) :@ Addr , (UIdEnv , UIdEnv)) , [(String , UIdMap)])
testScopeInferencer fn input
  = case fellScopeInferencerT (scopeInferProg fn input) ([primEnv] , [primTyEnv]) [("" , primUIdMap)] of
      Left e
        -> fail (show e)
      Right p
        -> return p

testTypeInferencer
  :: (MonadFail m)
  => [OpMap] -> ProgAST String (TyRHSAST String :@ Addr) (PatAST String :@ Addr) (ExprAST String :@ Addr) :@ Addr -> m String
testTypeInferencer openv input
  = let
      prog
        = typeInferProg input `bind` \ (tenv , kenv) ->
          unparseTyEnv tenv `bind` \ x ->
          unparseTyEnv kenv `bind` \ y ->
          invoke ("(" ++ x ++ "," ++ y ++ ")")
    in
    case fst **> fellTypeInferencerT prog 0 (Map.fromList (primCons ++ primVars) , Map.fromList primTyCons) openv of
      Left e
        -> fail (show e)
      Right s
        -> return s

handler e
  | isUserError e
    = putStrLn (ioeGetErrorString e)
  | otherwise
    = ioError e

analyze file
  = do
      let fn = trunk file
      conts <- readFile file
      lexs <- testLexer conts
      lexfile <- openFile (fn ++ "-lex") WriteMode
      mapM_ (hPrint lexfile) lexs
      hClose lexfile
      implexs <- testDecommenter lexs
      decommentfile <- openFile (fn ++ "-decomment") WriteMode
      mapM_ (hPrint decommentfile) implexs
      hClose decommentfile
      iseq <- testParser implexs
      parsfile <- openFile (fn ++ "-pars") WriteMode
      hPrint parsfile iseq
      hClose parsfile
      (itree , openv) <- testDisambiguator iseq
      disambiguatfile <- openFile (fn ++ "-disambiguat") WriteMode
      hPrint disambiguatfile (itree , openv)
      hClose disambiguatfile
      ast <- testDeveloper itree
      developfile <- openFile (fn ++ "-develop") WriteMode
      hPrint developfile ast
      hClose developfile
      uidast <- testScopeInferencer fn ast
      scopeinferfile <- openFile (fn ++ "-scopeinfer") WriteMode
      hPrint scopeinferfile uidast
      hClose scopeinferfile
      tymap <- testTypeInferencer openv ast
      typeinferfile <- openFile (fn ++ "-typeinfer") WriteMode
      hPutStrLn typeinferfile tymap
      hClose typeinferfile

analyzeWithIO file
  = analyze file `catchIOError` handler

main
  = do 
      hSetBuffering stdout NoBuffering
      args <- getArgs
      mapM_ analyzeWithIO args
