{-# LANGUAGE
      GeneralizedNewtypeDeriving,
      MultiParamTypeClasses,
      FlexibleInstances
    #-}
module Pool (
    module Pool.Classes,
    module Pool
  ) where


import Control.Diapplicative
import Control.Diapplicative.Trans
import Pool.Classes


newtype PoolT c f e a
  = PoolT { runPoolT :: StateT c f e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, Reader r)

fellPoolT
  :: PoolT c f e a -> c -> f e (a , c)
fellPoolT
  = runStateT . runPoolT


instance Transformer (PoolT c) where
  
  lift
    = PoolT . lift
  

instance (Bifunctor f, Dipointed f, Enum c) => Pool c (PoolT c f) where
  
  next
    = PoolT $ StateT $ \ c ->
      invoke (c , succ c)
  

