{-# LANGUAGE
      TypeOperators,
      GeneralizedNewtypeDeriving,
      FlexibleInstances,
      FlexibleContexts,
      MultiParamTypeClasses,
      UndecidableInstances
  #-}
module Interpreter 
(
  module Interpreter.Classes,
  module Interpreter.Data,
  module Interpreter
) where


import Prelude hiding (lookup)
import Utils
import Data.Utils
import Data.RefUtils
import Data.MapUtils (Map, lookupM, fromList, empty, singleton, union, unions, restrictKeys, withoutKeys)
import Data.Bifunctor
import Data.Function
import Control.Diapplicative
import Control.Diapplicative.Trans
import Address
import Syntax.AST
import Primitives
import TypeInferencer.TyAST
import Interpreter.Data
import Interpreter.Classes


newtype InterpreterT m p e a
  = InterpreterT { runInterpreterT :: ReaderT m p e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, Reader m)

fellInterpreterT
  :: InterpreterT m p e a -> m -> p e a
fellInterpreterT p m
  = runReaderT (runInterpreterT p) m

withLocalMap
  :: (m -> m') -> InterpreterT m' p e a -> InterpreterT m p e a
withLocalMap f p
  = InterpreterT . ReaderT $ \ m ->
    fellInterpreterT p (f m)


instance Transformer (InterpreterT m) where
  
  lift
    = InterpreterT . lift
  

-- This instance declaration requires UndecidableInstances
-- Although it seems to me that InterpreterT m p determines p and p determines r
-- So the pragma should not be necessary
instance (Mut r p) => Mut r (InterpreterT m p) where
  
  newRef
    = lift . newRef
  readRef
    = lift . readRef
  writeRef x
    = lift . writeRef x
  modifyRef f
    = lift . modifyRef f
  

instance (Mut ref p) => Interpreter nam ref (InterpreterT (LValMap nam ref) p) where
  
  askConLValMap
    = ask
  

newtype RenameT nam ref p e a
  = RenameT { runRenameT :: ReaderT (LValMap nam ref) p e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, Reader (LValMap nam ref))

fellRenameT
  :: RenameT nam ref p e a -> LValMap nam ref -> p e a
fellRenameT p lvalmap
  = runReaderT (runRenameT p) lvalmap


instance Transformer (RenameT nam ref) where
  
  lift
    = RenameT . lift
  

instance (Mut r p) => Mut r (RenameT nam ref p) where
  
  newRef
    = lift . newRef
  readRef
    = lift . readRef
  writeRef x
    = lift . writeRef x
  modifyRef f
    = lift . modifyRef f
  

exprToTerm
  :: ExprAST nam -> TermAST nam ref
exprToTerm (AtomExpr (atom :@ addr))
  = case atom of
      VarExpr (Var name :@ _)
        -> AtomTerm (Bound name :@ addr)
      ConExpr (Con name :@ _)
        -> AtomTerm (Bound name :@ addr)
      Lit (lit :@ _)
        -> let
             val
               = case lit of
                   Char c
                     -> c
                   String s
                     -> s
                   Num n
                     -> n
           in
           ValTerm (ConVal val [] :@ addr)
exprToTerm (App (expr1 :@ addr1) (expr2 :@ addr2))
  = AppTerm (exprToTerm expr1 :@ addr1) (exprToTerm expr2 :@ addr2)
exprToTerm (Lam mode (pat :@ addr1) (expr :@ addr2))
  = ValTerm (LamVal mode (pat :@ addr1) (exprToTerm expr :@ addr2) :@ addr1 <> addr2)
exprToTerm (Local (block :@ addr1) (expr :@ addr2))
  = case block of
      Block idecls eqs
        -> let
             ((lhss , rhss) , addrs)
               = bimap separate id (separate eqs)
           in
           LocalTerm (Block idecls (zipWith (:@) (zipWith Eq lhss (fmap (bimap exprToTerm id) rhss)) addrs) :@ addr1) (exprToTerm expr :@ addr2)

renameAtom
  :: (Ord nam, Dimonad f)
  => AtomAST nam ref -> RenameT nam ref f e (AtomAST nam ref)
renameAtom atom
  = case atom of
      Bound name
        -> raise (dummyCast "renameAtom") ///
           Free **> (ask `bind` lookupM name) ///
           invoke atom
      _ -> invoke atom

renameTerm
  :: (Ord nam, Mut ref f)
  => TermAST nam ref -> RenameT nam ref f e (TermAST nam ref)
renameTerm term
  = case term of
      AtomTerm (atom :@ addr)
        -> invoke AtomTerm ***
           second (:@ addr) (renameAtom atom)
      AppTerm (term1 :@ addr1) (term2 :@ addr2)
        -> invoke AppTerm ***
           second (:@ addr1) (renameTerm term1) ***
           second (:@ addr2) (renameTerm term2)
      LocalTerm (block :@ addr1) (term :@ addr2)
        -> case block of
             Block idecls eqs
               -> let
                    ((lhss , rhss) , addrs)
                      = bimap separate id (separate eqs)
                    defvars
                      = foldl (flip bvarsHelper) mempty lhss
                  in
                  withoutKeys defvars **> ask `bind` \ newlvalmap ->
                  let
                    rhss'
                      = local (const newlvalmap) (mapC (\ (term :@ addr) -> second (:@ addr) (renameTerm term)) rhss)
                    block'
                      = Block idecls **>
                        second (flip (zipWith (:@)) addrs . zipWith Eq lhss) rhss'
                    term'
                      = local (const newlvalmap) (renameTerm term)
                  in
                  invoke LocalTerm ***
                  second (:@ addr1) block' ***
                  second (:@ addr2) term'
      PrimTerm name atoms
        -> invoke (PrimTerm name) *** 
           mapC renameAtom atoms
      ValTerm (val :@ addr)
        -> invoke ValTerm ***
           second (:@ addr) (renameVal val)

renameVal
  :: (Ord nam, Mut ref f)
  => ValAST nam ref -> RenameT nam ref f e (ValAST nam ref)
renameVal val
  = case val of
      LamVal mode pat (term :@ addr2)
        -> invoke (LamVal mode pat) ***
           second (:@ addr2) (local (withoutKeys (bvars pat)) (renameTerm term))
      ConVal name args
        -> invoke (ConVal name) ***
           mapC renameAtom args
      _ -> invoke val

updatePatList
  :: (Mut ref f)
  => [EvalCxt nam ref] -> [PatAST (nam , ref (EvalCxt nam ref)) :@ Addr] -> f e ()
updatePatList ecxts args
  = let
      (pats , _)
        = separate args
    in
    ignore (zipWithC updatePat ecxts pats)

updatePat
  :: (Mut ref f)
  => EvalCxt nam ref -> PatAST (nam , ref (EvalCxt nam ref)) -> f e ()
updatePat ecxt pat
  = case pat of
      VarPat (Var (_ , r) :@ _)
        -> writeRef ecxt r
      ConPat (Con _ :@ _) args
        -> updatePatList (repeat ecxt) args
      _ -> invoke ()

initPatList
  :: (Interpreter String ref f)
  => [PatAST String :@ Addr] -> f e ([PatAST (String , ref (EvalCxt String ref)) :@ Addr] , LValMap String ref)
initPatList args
  = let
      (pats , addrs)
        = separate args
    in
    invoke (bimap (flip (zipWith (:@)) addrs) unions . separate) ***
    mapC initPat pats

initPat
  :: (Interpreter String ref f)
  => PatAST String -> f e (PatAST (String , ref (EvalCxt String ref)) , LValMap String ref)
initPat pat
  = case pat of
      VarPat (Var name :@ addr)
        -> invoke (\ r -> (VarPat (Var (name , r) :@ addr) , singleton name r)) ***
           newRef undefined
      ConPat (Con name :@ addr) args
        -> invoke (\ r (pats , lvalmap) -> (ConPat (Con (name , r) :@ addr) pats , lvalmap)) ***
           askConLVal name ***
           initPatList args
      _ -> invoke (WildPat , empty)

eqsAsEvalCxts
  :: (Interpreter String ref f)
  => Bool -> [EqAST (PatAST String :@ Addr) (TermAST String ref :@ Addr)] -> f e ([PatAST (String , ref (EvalCxt String ref)) :@ Addr] , LValMap String ref)
eqsAsEvalCxts isrec eqs
  = let
      (lhss , rhss)
        = separate eqs
      (terms , termaddrs)
        = separate rhss
    in
    initPatList lhss `bind` \ res@(lhss' , lvalmap) ->
    (if isrec then second (flip (zipWith (:@)) termaddrs) (fellRenameT (mapC renameTerm terms) lvalmap) else invoke rhss) `bind` \ rhss' ->
    let ecxts = zipWith EvalCxt lhss' rhss' in
    updatePatList ecxts lhss' `bind` \ _ ->
    invoke res

evalCxtRefOf
  :: AtomAST nam ref -> ref (EvalCxt nam ref)
evalCxtRefOf (Bound _)
  = error "findEvalCxtRef: Panic! Bound variable during evaluation!"
evalCxtRefOf (Free r)
  = r

evalAtom
  :: (Interpreter String ref f)
  => AtomAST String ref -> f InterpretationError (ValAST String ref)
evalAtom atom
  = let
      r = evalCxtRefOf atom
    in
    readRef r `bind` \ ecxt@(EvalCxt lhs rhs@(_ :@ addr)) ->
    case lhs of
      VarPat _ :@ _
        -> evalToOuterCon rhs `bind` \ val ->
           let res = ValTerm (val :@ addr) :@ addr in
           writeRef (EvalCxt lhs res) r `bind` \ _ ->
           invoke val
      _ -> (either id id //> match ecxt) ~**
           evalAtom (Free r)

evalToOuterCon
  :: (Interpreter String ref f)
  => TermAST String ref :@ Addr -> f InterpretationError (ValAST String ref)
evalToOuterCon (AtomTerm (atom :@ _) :@ _)
  = evalAtom atom
evalToOuterCon (AppTerm (term1 :@ addr1) (term2 :@ addr2) :@ _)
  = evalToOuterCon (term1 :@ addr1) `bind` \ val1 ->
    case val1 of
      LamVal mode arg (term :@ addr)
        -> eqsAsEvalCxts False [Eq arg (term2 :@ addr2)] `bind` \ ([arg'] , lvalmap) ->
           let
             matchtask
               = match (EvalCxt arg' (term2 :@ addr2))
             evaltask
               = fellRenameT (renameTerm term) lvalmap `bind` \ term' ->
                 evalToOuterCon (term' :@ addr)
             wholetask
               = matchtask `bind` \ _ -> Left //> evaltask
           in
           case mode of
             Normal
               -> either id id //> wholetask
             Backtrack
               -> fromEither **> turnr wholetask
             _ -> error "evalToOuterCon: Panic! Postpone mode of lambda!"
      _ -> error "evalToOuterCon: Panic! Non-lambda application!"
evalToOuterCon (LocalTerm (block :@ _) (term :@ addr) :@ _)
  = case block of
      Block _ decls
        -> let
             (eqs , _)
               = separate decls
           in
           eqsAsEvalCxts True eqs `bind` \ (_ , lvalmap) ->
           fellRenameT (renameTerm term) lvalmap `bind` \ term' ->
           evalToOuterCon (term' :@ addr)
evalToOuterCon (PrimTerm name args :@ addr)
  = dummyCast "evalToOuterCon" //> lookupM name primEvalMap `bind` \ h ->
    h addr args
evalToOuterCon (ValTerm (val :@ _) :@ _)
  = invoke val

evalToMatch
  :: (Interpreter String ref f)
  => PatAST (String , ref (EvalCxt String ref)) :@ Addr -> TermAST String ref :@ Addr -> f (Either InterpretationError InterpretationError) (TermAST String ref)
evalToMatch (pat :@ _) (term :@ termaddr)
  = case pat of
      ConPat (Con (n , _) :@ conaddr) patargs
        -> Left //> evalToOuterCon (term :@ termaddr) `bind` \ val ->
           case val of
             ConVal name atoms
               | name == n
                 -> zipWithC evalToMatch patargs (fmap ((:@ Unspec) . AtomTerm . (:@ Unspec)) atoms) `bind` \ _ ->
                    invoke (ValTerm (val :@ termaddr))
               | otherwise
                 -> raise (Right (PatternMatchFailure name conaddr termaddr))
             _ -> error "evalToMatch: Panic! A non-constructed value is matched with a constructed pattern!"
      _ -> invoke term

evalFullHelper
  :: (Interpreter String ref f)
  => ValAST String ref -> f InterpretationError ()
evalFullHelper val
  = case val of
      ConVal _ args
        -> let
             op atom
               = evalAtom atom `bind` \ val ->
                 evalFullHelper val
           in
           ignore (mapC op args)
      _ -> invoke ()

evalFull
  :: (Interpreter String ref f)
  => TermAST String ref :@ Addr -> (ValAST String ref -> f InterpretationError ()) -> f InterpretationError (ValAST String ref)
evalFull term op
  = evalToOuterCon term `bind` \ val ->
    evalFullHelper val `bind` \ _ ->
    op val ~**
    invoke val

evalProg
  :: (Mut ref f) 
  => [ProgAST String tyrhs (PatAST String :@ Addr) (ExprAST String :@ Addr) :@ Addr] -> ExprAST String :@ Addr -> (ValAST String ref -> f InterpretationError ()) -> InterpreterT (TyEnv String) f InterpretationError (ValAST String ref)
evalProg progs expr op
  = let
      block
        = Block undefined (progs >>= \ (Prog _ _ eqs :@ _) -> eqs) :@ Unspec
      term
        = exprToTerm (Local block expr)
    in
    buildPrimLValMap primVarTyEnv `bind` \ primlvalmap ->
    restrictKeys (cons (Local block expr :@ Unspec)) **> InterpreterT ask `bind` \ contyenv ->
    buildConLValMap contyenv `bind` \ conlvalmap ->
    let lvalmap = union primlvalmap conlvalmap in
    fellRenameT (renameTerm term) lvalmap `bind` \ fvterm ->
    withLocalMap (const conlvalmap) (evalFull (fvterm :@ Unspec) (lift . op))

refineEvalCxts
  :: (Mut ref f)
  => PatAST (nam , ref (EvalCxt nam ref)) :@ Addr -> TermAST nam ref :@ Addr -> f e ()
refineEvalCxts (pat :@ pataddr) (term :@ termaddr)
  = case pat of
      VarPat (Var (_ , r) :@ _)
        -> writeRef (EvalCxt (pat :@ pataddr) (term :@ termaddr)) r
      ConPat (Con _ :@ _) patargs
        -> case term of
             ValTerm (ConVal _ atoms :@ _)
               -> let
                    refs
                      = fmap evalCxtRefOf atoms
                  in
                  fmap evalCxtTerm **> mapC readRef refs `bind` \ termargs ->
                  ignore (zipWithC refineEvalCxts patargs termargs)
             _ -> error "refineEvalCxts: Panic! A value that just matched does not match anymore!"
      _ -> invoke ()

match
  :: (Interpreter String ref f)
  => EvalCxt String ref -> f (Either InterpretationError InterpretationError) ()
match (EvalCxt (pat :@ pataddr) (term :@ termaddr))
  = evalToMatch (pat :@ pataddr) (term :@ termaddr) `bind` \ term' ->
    refineEvalCxts (pat :@ pataddr) (term' :@ termaddr)

primEvalMap
  :: (Interpreter String ref f)
  => Map String (Addr -> [AtomAST String ref] -> f InterpretationError (ValAST String ref))
primEvalMap
  = fromList 
    [ ("primAddInt" , evalAddInt)
    , ("primSubInt" , evalSubInt)
    , ("primMulInt" , evalMulInt)
    , ("primDivInt" , evalDivInt)
    , ("primAbsInt" , evalAbsInt)
    , ("primRunExn" , evalRunExn)
    , ("primRetExn" , evalRetExn)
    , ("primBndExn" , evalBndExn)
    , ("primElsExn" , evalElsExn)
    ]

rawRet
  :: (Dipointed f)
  => AtomAST String ref -> f e (ValAST String ref)
rawRet
  = invoke . ConVal successName . return

rawBnd
  :: (Interpreter String ref f)
  => AtomAST String ref -> (AtomAST String ref -> f InterpretationError a) -> f (Either InterpretationError InterpretationError) a
rawBnd atom f
  = turnl (toEither **> evalAtom atom) `bind` \ val ->
    case val of
      ConVal _ [arg]
        -> Left //> f arg
      ConVal con args
        -> error ("rawBnd: Panic! Value constructed by " ++ con ++ " has " ++ show (length args) ++ " arguments!")
      _ -> error ("rawBnd: Panic! Type error!")

evalUnArithInt
  :: (Interpreter String ref f)
  => (Integer -> Integer) -> Addr -> [AtomAST String ref] -> f InterpretationError (ValAST String ref)
evalUnArithInt f _ [atom]
  = invoke (flip ConVal [] . show . f . read . conNameOf) ***
    evalAtom atom
evalUnArithInt _  _ _
  = error "evalUnArithInt: Panic! An unary arithmetic operation is given an improper number of arguments!"

evalBinArithInt
  :: (Interpreter String ref f)
  => (Integer -> Integer -> Integer) -> Addr -> [AtomAST String ref] -> f InterpretationError (ValAST String ref)
evalBinArithInt op _ [atom1, atom2]
  = invoke (\ val1 val2 -> ConVal (show ((op `on` read . conNameOf) val1 val2)) []) ***
    evalAtom atom1 ***
    evalAtom atom2
evalBinArithInt _  _ _
  = error "evalBinArithInt: Panic! A binary arithmetic operation is given an improper number of arguments!"

evalAddInt
  :: (Interpreter String ref f)
  => Addr -> [AtomAST String ref] -> f InterpretationError (ValAST String ref)
evalAddInt
  = evalBinArithInt (+)

evalSubInt
  :: (Interpreter String ref f)
  => Addr -> [AtomAST String ref] -> f InterpretationError (ValAST String ref)
evalSubInt
  = evalBinArithInt (-)

evalMulInt
  :: (Interpreter String ref f)
  => Addr -> [AtomAST String ref] -> f InterpretationError (ValAST String ref)
evalMulInt
  = evalBinArithInt (*)

evalDivInt
  :: (Interpreter String ref f)
  => Addr -> [AtomAST String ref] -> f InterpretationError (ValAST String ref)
evalDivInt addr [atom1, atom2]
  = read . conNameOf **> evalAtom atom1 `bind` \ val1 ->
    read . conNameOf **> evalAtom atom2 `bind` \ val2 ->
    if val2 == 0
      then invoke (ErrVal (DivisionByZero addr))
      else let
             pat
               = VarPat (Var "result" :@ Unspec)
             term
               = ValTerm (ConVal (show (val1 `div` val2)) [] :@ addr)
           in
           eqsAsEvalCxts False [Eq (pat :@ Unspec) (term :@ Unspec)] `bind` \ ([VarPat (Var (_ , r) :@ _) :@ _], _) ->
           rawRet (Free r)
evalDivInt _    _
  = error "evalDivInt: Panic! An improper number of arguments!"

evalAbsInt
  :: (Interpreter String ref f)
  => Addr -> [AtomAST String ref] -> f InterpretationError (ValAST String ref)
evalAbsInt
  = evalUnArithInt abs

evalRetExn
  :: (Interpreter String ref f)
  => Addr -> [AtomAST String ref] -> f InterpretationError (ValAST String ref)
evalRetExn _ [atom]
  = rawRet atom
evalRetExn _ _
  = error "evalRetExn: Panic! An improper number of arguments!"

evalRunExn
  :: (Interpreter String ref f)
  => Addr -> [AtomAST String ref] -> f InterpretationError (ValAST String ref)
evalRunExn _ [atom]
  = either id id //> rawBnd atom evalAtom
evalRunExn _ _
  = error "evalRunExn: Panic! An improper number of arguments!"

evalBndExn
  :: (Interpreter String ref f)
  => Addr -> [AtomAST String ref] -> f InterpretationError (ValAST String ref)
evalBndExn addr [atom1, atom2]
  = (fromEither **>) . turnr . rawBnd atom1 $ \ arg -> 
    evalToOuterCon (AppTerm (termOfAtom atom2) (termOfAtom arg) :@ addr)
evalBndExn _    _
  = error "evalBndExn: Panic! An improper number of arguments!"

evalElsExn
  :: (Interpreter String ref f)
  => Addr -> [AtomAST String ref] -> f InterpretationError (ValAST String ref)
evalElsExn _ [atom1, atom2]
  = (fromEither **>) . turnr . (either Right Left //>) $ 
    raise (Left (const id)) |||
    either Right Left //> rawBnd atom1 rawRet |||
    either Right Left //> rawBnd atom2 rawRet
evalElsExn _ _
  = error "evalElsExn: Panic! An improper number of arguments!"
