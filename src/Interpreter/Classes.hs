{-# LANGUAGE
      MultiParamTypeClasses,
      TypeOperators,
      FlexibleContexts
  #-}
module Interpreter.Classes
  ( Interpreter(..)
  , buildPrimLValMap
  , buildConLValMap
  , askConLVal
  ) where


import Prelude hiding (lookup)
import Utils
import Control.Diapplicative
import Data.MapUtils
import Data.RefUtils
import Address
import Syntax.AST
import TypeInferencer.TyAST
import Interpreter.Data


class (Mut ref p) => Interpreter nam ref p where
  
  askConLValMap
    :: p e (LValMap nam ref)
  

termAfterTyHelper
  :: Type -> ([PatAST String] -> [AtomAST String ref] -> (PatAST String -> TermAST String ref :@ Addr -> TermAST String ref :@ Addr) -> o) -> o
termAfterTyHelper ty cont
  = let
      names
        = fmap (('x' :) . show) [0 .. arity ty - 1]
      pats
        = fmap (VarPat . (:@ Unspec) . Var) names
      atoms
        = fmap Bound names
      op pat acc
        = ValTerm (LamVal Normal (pat :@ Unspec) acc :@ Unspec) :@ Unspec
    in
    cont pats atoms op

termAfterPrimTy
  :: String -> Type -> TermAST String ref :@ Addr
termAfterPrimTy primname primty
  = termAfterTyHelper primty $ \ pats atoms op ->
    foldr op (PrimTerm primname atoms :@ Unspec) pats

termAfterConTy
  :: String -> Type -> TermAST String ref :@ Addr
termAfterConTy conname conty
  = termAfterTyHelper conty $ \ pats atoms op ->
    foldr op (ValTerm (ConVal conname atoms :@ Unspec) :@ Unspec) pats

newTrivEvalCxt
  :: (Mut ref f)
  => nam -> TermAST nam ref :@ Addr -> f e (ref (EvalCxt nam ref))
newTrivEvalCxt n term
  = newRef undefined `bind` \ r ->
    writeRef (EvalCxt (VarPat (Var (n , r) :@ Unspec) :@ Unspec) term) r `bind` \ _ ->
    invoke r

buildPrimLValMap
  :: (Mut ref f)
  => TyEnv String -> f e (LValMap String ref)
buildPrimLValMap
  = mapWithKeyC newTrivEvalCxt .
    mapWithKey termAfterPrimTy

buildConLValMap
  :: (Mut ref f)
  => TyEnv String -> f e (LValMap String ref)
buildConLValMap
  = mapWithKeyC newTrivEvalCxt .
    mapWithKey termAfterConTy

askConLVal
  :: (Interpreter String ref f)
  => String -> f e (ref (EvalCxt String ref))
askConLVal n
  = dummyCast "askConLVal" //> 
    (askConLValMap `bind` lookupM n)

