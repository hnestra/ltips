{-# LANGUAGE
      TypeOperators,
      FlexibleContexts,
      FlexibleInstances,
      MultiParamTypeClasses,
      UndecidableInstances
  #-}
module Interpreter.Data
  ( module Interpreter.Data
  ) where


import Control.Diapplicative
import Control.Diapplicative.Trans
import Data.MapUtils (Map)
import Data.RefUtils
import Syntax.Token
import Syntax.AST
import Phase
import Unparse


data ValAST nam ref
  = LamVal EvalMode (PatAST nam :@ Addr) (TermAST nam ref :@ Addr)
  | ConVal String [AtomAST nam ref]
  | ErrVal InterpretationError

data TermAST nam ref
  = AtomTerm (AtomAST nam ref :@ Addr)
  | AppTerm (TermAST nam ref :@ Addr) (TermAST nam ref :@ Addr)
  | LocalTerm (BlockAST (PatAST nam :@ Addr) (TermAST nam ref :@ Addr) :@ Addr) (TermAST nam ref :@ Addr)
  | PrimTerm String [AtomAST nam ref]
  | ValTerm (ValAST nam ref :@ Addr)

data AtomAST nam ref
  = Bound nam
  | Free (ref (EvalCxt nam ref))

data EvalCxt nam ref
  = EvalCxt (PatAST (nam , ref (EvalCxt nam ref)) :@ Addr) (TermAST nam ref :@ Addr)

type LValMap nam ref
  = Map nam (ref (EvalCxt nam ref))


successName
  :: String
successName
  = "just"

conNameOf
  :: ValAST nam ref -> String
conNameOf (ConVal con _)
  = con
conNameOf _
  = error "conNameOf: Panic! A value with no constructor!"

evalCxtPat
  :: EvalCxt nam ref -> PatAST (nam , ref (EvalCxt nam ref)) :@ Addr
evalCxtPat (EvalCxt pat _)
  = pat

evalCxtTerm
  :: EvalCxt nam ref -> TermAST nam ref :@ Addr
evalCxtTerm (EvalCxt _ term)
  = term

toEither
  :: ValAST nam ref -> Either InterpretationError (ValAST nam ref)
toEither val@(ConVal _ _)
  = Right val
toEither (ErrVal err)
  = Left err
toEither _
  = error "toEither: Panic! Function being transformed to Either!"

fromEither
  :: Either InterpretationError (ValAST nam ref) -> ValAST nam ref
fromEither
  = either ErrVal id

termOfAtom
  :: AtomAST nam ref -> TermAST nam ref :@ Addr
termOfAtom atom
  = AtomTerm (atom :@ Unspec) :@ Unspec


data InterpretationError
  = PatternMatchFailure String Addr Addr
  | DivisionByZero Addr
  deriving (Eq)


instance Show InterpretationError where
  
  show (PatternMatchFailure name conaddr valaddr)
    = formatError Interpretation valaddr $
      "Constructor " ++ name ++ " cannot be matched with the constructor at " ++ show conaddr
  show (DivisionByZero addr)
    = formatError Interpretation addr $
      "Division by zero"


-- This instance declaration requires UndecidableInstances
-- because the coverage condition for Mut fails
-- as UnparseT t p does not determine ref
--
-- Strange, since UnparseT t p determines p and p determines ref
--
instance (Mut ref p) => Mut ref (UnparseT t p) where
  
  newRef
    = lift . newRef
  readRef
    = lift . readRef
  writeRef x r
    = lift (writeRef x r)
  

instance (Mut ref p) => Unparse (ValAST nam ref) (UnparseT (ValAST nam ref) p) where
  
  askOpMaps
    = ask
  
  destruct (LamVal _ _ _)
    = invoke (Primitive "<function>")
  destruct (ConVal con args)
    = let
        f destructedargs
          | length args >= 2 && not (isPrefix con)
            = let
                ([arg1, arg2] , rest)
                  = splitAt 2 destructedargs
              in
              foldl (Compound "") (Compound con arg1 arg2) rest
          | otherwise
            = foldl (Compound "") (Primitive con) destructedargs
        g (Free ref)
          = readRef ref `bind` \ (EvalCxt _ (term :@ _)) ->
            case term of
              ValTerm (val :@ _)
                -> destruct val
              _ -> invoke (Primitive "<unevaluated>")
        g _
          = error "destruct: Panic! A bound variable left unrenamed!"
      in
      invoke f ***
      mapC g args
  destruct (ErrVal _)
    = invoke (Primitive "<exception>")
  

