module InfNum 
  (
  Inf (..),
  InfNum (..),
  mid
  ) where


data Inf a
  = Fin a
  | Inf
  deriving (Eq, Ord)


class (Eq a, Num a) => InfNum a where
  
  inf
    :: a
  

instance (Eq a, Num a) => InfNum (Inf a) where
  
  inf
    = Inf
  

instance (Show a) => Show (Inf a) where
  
  showsPrec n (Fin x) acc
    = showsPrec n x acc
  showsPrec _ Inf     acc
    = "inf" ++ acc


errMsg
  :: String -> String
errMsg str
  = "Indeterminate form " ++ str
  

instance (Eq a, Num a) => Num (Inf a) where
  
  Fin x + Fin y
    = Fin (x + y)
  _     + _
    = Inf
  
  Fin x - Fin y
    = Fin (x - y)
  Inf   - Inf
    = error (errMsg "inf - inf")
  _     - _
    = Inf
  
  Fin x * Fin y
    = Fin (x * y)
  Fin 0 * Inf
    = error (errMsg "0 * inf")
  Inf   * Fin 0
    = error (errMsg "inf * 0")
  _     * _
    = Inf
  
  abs (Fin x)
    = Fin (abs x)
  abs _
    = Inf
  
  signum (Fin x)
    = Fin (signum x)
  signum _
    = error (errMsg "inf / inf")
  
  fromInteger n
    = Fin (fromInteger n)
  

instance (Enum a) => Enum (Inf a) where
  
  toEnum n
    = Fin (toEnum n)
  
  fromEnum (Fin n)
    = fromEnum n
  fromEnum _
    = error "inf"
  
  succ (Fin n)
    = Fin (succ n)
  succ _
    = Inf
  
  pred (Fin n)
    = Fin (pred n)
  pred _
    = Inf
  
  enumFrom (Fin n)
    = map Fin (enumFrom n)
  enumFrom _
    = []
  
  enumFromThen (Fin n) (Fin m)
    = map Fin (enumFromThen n m)
  enumFromThen (Fin n) _
    = [Fin n]
  enumFromThen _       _
    = []
  
  enumFromTo (Fin n) (Fin l)
    = map Fin (enumFromTo n l)
  enumFromTo (Fin n) _
    = enumFrom (Fin n)
  enumFromTo _       _
    = []
  
  enumFromThenTo (Fin n) (Fin m) (Fin l)
    = map Fin (enumFromThenTo n m l)
  enumFromThenTo (Fin n) (Fin m) _
    = enumFromThen (Fin n) (Fin m)
  enumFromThenTo (Fin n) _       _
    = [Fin n]
  enumFromThenTo _       _       _
    = []
  

mid
  :: (Integral a)
  => Inf a -> Inf a -> Inf a
mid (Fin x) (Fin y)
  = Fin $ (x + y) `div` 2
mid _       _
  = Inf
