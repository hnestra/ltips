module Primitives 
  ( module Primitives
  ) where


import Data.MapUtils
import Syntax.Data
import Syntax.AST
import ScopeInferencer.Data
import TypeInferencer.TyAST


primTyCons
  :: [(String , Type)]
primTyCons
  = [
    (arrNam , curriedTy [typeKind, typeKind, typeKind]),
    (exnNam , curriedTy [typeKind, typeKind]),
    (intNam , typeKind),
    (floatNam , typeKind),
    (charNam , typeKind),
    (stringNam , typeKind)
    ]

primCons
  :: [(String , Type)]
primCons
  = []

primVars
  :: [(String , Type)]
primVars
  = [
    ("primRunExn" , curriedTy [exnTy (varTy 0), varTy 0]),
    ("primRetExn" , curriedTy [varTy 0, exnTy (varTy 0)]), 
    ("primBndExn" , curriedTy [exnTy (varTy 0), funTy (varTy 0) (exnTy (varTy 1)), exnTy (varTy 1)]), 
    ("primElsExn" , curriedTy [exnTy (varTy 0), exnTy (varTy 0), exnTy (varTy 0)]), 
    ("primAbsInt" , curriedTy [intTy, intTy]),
    ("primAddInt" , curriedTy [intTy, intTy, intTy]),
    ("primSubInt" , curriedTy [intTy, intTy, intTy]),
    ("primMulInt", curriedTy [intTy, intTy, intTy]), 
    ("primDivInt", curriedTy [intTy, intTy, exnTy intTy])
    ]

primVarTyEnv
  :: Map String Type
primVarTyEnv
  = fromList primVars

primConTyEnv
  :: Map String Type
primConTyEnv
  = fromList primCons

primConKiEnv
  :: Map String Type
primConKiEnv
  = fromList primTyCons


primTyEnv
  :: UIdEnv
primTyEnv
  = fromList $ zip (fmap fst primTyCons) [0 ..]

primEnv
  :: UIdEnv
primEnv
  = fromList $ zip (fmap fst (primCons ++ primVars)) [size primTyEnv ..]

primUIdMap
  :: UIdMap
primUIdMap
  = fmap (:@ Unspec) (invert primEnv) `union`
    fmap (:@ Unspec) (invert primTyEnv)

