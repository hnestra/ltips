{-# LANGUAGE
      TupleSections,
      GeneralizedNewtypeDeriving,
      FlexibleContexts,
      FlexibleInstances,
      MultiParamTypeClasses,
      ScopedTypeVariables,
      TypeOperators,
      UndecidableInstances
    #-}
module UI (
    module UI.Data,
    module UI.Editor,
    module UI
  ) where


import Prelude hiding (readFile, getLine, putStr, putStrLn, print)
import Control.Diapplicative
import Control.Diapplicative.Trans
import Data.Char
import Data.Maybe
import Data.ListUtils as List
import Data.MapUtils
import Data.RefUtils
import Utils
import Stack
import Syntax
import Lexer
import Decommenter
import Parser
import Disambiguator
import Developer
import ScopeInferencer
import Unparse
import TypeInferencer
import Interpreter
import Primitives
import UI.FilePath
import UI.Data
import UI.Classes
import UI.Editor


type Prog
  = ProgAST String (TyRHSAST String :@ Addr) (PatAST String :@ Addr) (ExprAST String :@ Addr) :@ Addr
type SemCxt
  = (Prog , OpMap , UIdEnv , UIdEnv , UIdMap , TyEnv Name , TyEnv Name)

newtype UIT p e a
  = UIT { runUIT :: StackT (FilePath , SemCxt) p e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, Transformer, StackState (FilePath , SemCxt))

fellUIT
  :: UIT p e a -> [(FilePath , SemCxt)] -> p e (a , [(FilePath , SemCxt)])
fellUIT p ss
  = fellStackT (runUIT p) ss


-- This instance declaration requires UndecidableInstances
-- Although UIT p determines p and p determines r
instance (Mut r p) => Mut r (UIT p) where
  
  newRef
    = lift . newRef
  readRef
    = lift . readRef
  writeRef x
    = lift . writeRef x
  


getModCxt
  :: (Bifunctor p, Dipointed p)
  => UIT p e [FilePath]
getModCxt
  = dummyCast "getModCxt" //> fmap fst **> getAll

getSemCxt
  :: (Bifunctor p, Dipointed p)
  => UIT p e [SemCxt]
getSemCxt
  = dummyCast "getSemCxt" //> fmap snd **> getAll

extractInfix
  :: SemCxt -> OpMap
extractInfix (_ , openv , _ , _ , _ , _ , _)
  = openv

extractType
  :: SemCxt -> TyEnv Name
extractType (_ , _ , _ , _ , _ , tenv , _)
  = tenv

extractKind
  :: SemCxt -> TyEnv Name
extractKind (_ , _ , _ , _ , _ , _ , kenv)
  = kenv

unparseTy
  :: forall p e
  .  (Dimonad p)
  => Type -> [OpMap] -> p e String
unparseTy t opmaps 
  = fellUnparseT (unparse t :: UnparseT Type p e String) opmaps

unparseVal
  :: forall nam ref p e
  .  (Mut ref p)
  => ValAST nam ref -> [OpMap] -> p e String
unparseVal v opmaps
  = fellUnparseT (unparse v :: UnparseT (ValAST nam ref) p e String) opmaps

parse
  :: forall r p
  .  (UI p, Mut r p)
  => String -> UIT p UIError ()
parse line
  | null cmd
    = promptAndEdit
  | head cmd /= ':'
    = after (val cmd) $
      promptAndEdit
  | otherwise
    = case select (tail us) vs of
        Quit
          -> quit
        Help
          -> after help $
             promptAndEdit
        Load fps
          -> after (multiload fps) $
             promptAndEdit
        Unload fpo
          -> after (unload fpo) $
             promptAndEdit
        Reload fpo
          -> after (reload fpo) $
             promptAndEdit
        Info nams
          -> after (successes (fmap infixinfo nams)) $
             after (successes (fmap (typeinfo extractType) nams)) $
             after (successes (fmap (typeinfo extractKind) nams)) $
             promptAndEdit
        View
          -> after view $
             promptAndEdit
        Type str
          -> after (typ str) $
             promptAndEdit
        Kind str
          -> after (knd str) $
             promptAndEdit
        Draw str
          -> after (draw str) $
             promptAndEdit
        Exec _
          -> after (tryAndPrintErrMsg (raise NotImplemented)) $
             promptAndEdit
        ErrMsg err
          -> after (print err) $
             promptAndEdit
  where
    cmd
      = dropWhile isSpace line
    (us , vs)
      = bimap id (dropWhile isSpace) $
        span (not . isSpace) $ 
        cmd

draw
  :: (Dimonad p, UI p)
  => String -> UIT p UIError ()
draw input
  = (const () **>) . tryAndPrintErrMsg $
    dummyCast "typ" //> unzip7 **> getSemCxt `bind` \ (_ , openvs , _ , _ , _ , _ , _) ->
    LexErr //> point (fellLexerT (lexAll input)) `bind` \ us ->
    DecommentErr //> point (fst **> fellDecommenterT decommentAll us) `bind` \ lexs ->
    let indentAnal = fellIndentT parseCmdLine defaultRel (defaultRange , NoAlign) in
    ParseErr //> point (fst . fst . fst **> fellParserT indentAnal lexs) `bind` \ iseq ->
    DisambiguationErr //> point (fst **> fellDisambiguatorT (disambiguateInfixTree iseq) openvs) `bind` \ ast ->
    mapC putStrLn (infixTree ast)

knd
  :: (Dimonad p, UI p)
  => String -> UIT p UIError ()
knd input
  = (const () **>) . tryAndPrintErrMsg $
    dummyCast "typ" //> bimap id unzip7 . unzip **> getAll `bind` \ (fps , (_ , openvs , dmaps , tmaps , uidmaps , tenvs , kenvs)) ->
    LexErr //> point (fellLexerT (lexAll input)) `bind` \ us ->
    DecommentErr //> point (fst **> fellDecommenterT decommentAll us) `bind` \ lexs ->
    let indentAnal = fellIndentT parseCmdLine defaultRel (defaultRange , NoAlign) in
    ParseErr //> point (fst . fst . fst **> fellParserT indentAnal lexs) `bind` \ iseq ->
    DisambiguationErr //> point (fst **> fellDisambiguatorT (disambiguateInfixTree iseq) openvs) `bind` \ ast ->
    DevelopmentErr //> point (fst **> fellDeveloperT (developTyExpr ast)) `bind` \ ast' -> 
    ScopeInferErr //> point (fst **> fellScopeInferencerT (scopeInferExpr TypeSpc ast') (dmaps , tmaps) (zip fps uidmaps)) `bind` \ _ ->
    TypeInferErr //> point (fst . fst . fst **> fellTypeInferencerT (runKindInferencerT (fellStackT (runWriterT (typeInferExpr ast')) [])) 0 (unions tenvs , unions kenvs) openvs) `bind` \ ty ->
    unparseTy ty openvs `bind` \ t ->
    putStrLn t

typ
  :: (Dimonad p, UI p)
  => String -> UIT p UIError ()
typ input
  = (const () **>) . tryAndPrintErrMsg $
    dummyCast "typ" //> bimap id unzip7 . unzip **> getAll `bind` \ (fps , (_ , openvs , dmaps , tmaps , uidmaps , tenvs , kenvs)) ->
    LexErr //> point (fellLexerT (lexAll input)) `bind` \ us ->
    DecommentErr //> point (fst **> fellDecommenterT decommentAll us) `bind` \ lexs ->
    let indentAnal = fellIndentT parseCmdLine defaultRel (defaultRange , NoAlign) in
    ParseErr //> point (fst . fst . fst **> fellParserT indentAnal lexs) `bind` \ iseq ->
    DisambiguationErr //> point (fst **> fellDisambiguatorT (disambiguateInfixTree iseq) openvs) `bind` \ ast ->
    DevelopmentErr //> point (fst **> fellDeveloperT (developExpr ast)) `bind` \ ast' -> 
    ScopeInferErr //> point (fst **> fellScopeInferencerT (scopeInferExpr DataSpc ast') (dmaps , tmaps) (zip fps uidmaps)) `bind` \ _ ->
    TypeInferErr //> point (fst . fst . fst **> fellTypeInferencerT (fellStackT (runWriterT (typeInferExpr ast')) []) 0 (unions tenvs , unions kenvs) openvs) `bind` \ ty ->
    unparseTy ty openvs `bind` \ t ->
    putStrLn t

val
  :: forall r p
  .  (UI p, Mut r p)
  => String -> UIT p UIError ()
val input
  = (const () **>) . tryAndPrintErrMsg $
    dummyCast "typ" //> bimap id unzip7 . unzip **> getAll `bind` \ (fps , (progs , openvs , dmaps , tmaps , uidmaps , tenvs , kenvs)) ->
    LexErr //> point (fellLexerT (lexAll input)) `bind` \ us ->
    DecommentErr //> point (fst **> fellDecommenterT decommentAll us) `bind` \ lexs ->
    let indentAnal = fellIndentT parseCmdLine defaultRel (defaultRange , NoAlign) in
    ParseErr //> point (fst . fst . fst **> fellParserT indentAnal lexs) `bind` \ iseq ->
    DisambiguationErr //> point (fst **> fellDisambiguatorT (disambiguateInfixTree iseq) openvs) `bind` \ ast ->
    DevelopmentErr //> point (fst **> fellDeveloperT (developExpr ast)) `bind` \ ast' -> 
    ScopeInferErr //> point (fst **> fellScopeInferencerT (scopeInferExpr DataSpc ast') (dmaps , tmaps) (zip fps uidmaps)) `bind` \ _ ->
    TypeInferErr //> point (fst . fst . fst **> fellTypeInferencerT (fellStackT (runWriterT (typeInferExpr ast')) []) 0 (unions tenvs , unions kenvs) openvs) `bind` \ _ ->
    InterpretErr //> fellInterpreterT (evalProg progs ast' (flip unparseVal openvs @@@ putStrLn)) (unions tenvs) `bind` \ _ ->
    invoke ()

view
  :: (Dimonad p, UI p)
  => UIT p UIError ()
view
  = dummyCast "view" //> unzip3 . fmap (\ (_ , openvs , _ , _ , _ , tenvs , kenvs) -> (openvs , tenvs , kenvs)) **> getSemCxt `bind` \ (openvs , tenvs , kenvs) ->
    putStrLn (intercalate " " (fmap head (group (merge (tail $ keys (unions openvs)) (merge (keys (unions tenvs)) (keys (unions kenvs)))))))

infixinfo
  :: (Dimonad p, UI p)
  => Name -> UIT p UIError ()
infixinfo nam
  = dummyCast "infixinfo, 1" //> fmap (bimap id extractInfix) **> getAll `bind` \ ps ->
    dummyCast "infixinfo, 2" //> mapT (strength . bimap id (lookupM nam)) ps `bind` \ (fp , (lpr , rpr)) ->
    putStrLn ("infix " ++ show lpr ++ " " ++ nam ++ " " ++ show rpr ++ " -- " ++ if null fp then "built-in" else "declared in " ++ fp)

typeinfo
  :: (Dimonad p, UI p)
  => (SemCxt -> TyEnv Name) -> Name -> UIT p UIError ()
typeinfo lev nam
  = dummyCast "typeinfo, 1" //> unzip . fmap (both (bimap id lev) (extractInfix . snd)) **> getAll `bind` \ (ps , openvs) ->
    dummyCast "typeinfo, 2" //> mapT (strength . bimap id (lookupM nam)) ps `bind` \ (fp , ty) ->
    unparseTy ty openvs `bind` \ t ->
    putStrLn ((if isPrefix nam then nam else "(" ++ nam ++ ")") ++ " :: " ++ t ++ " -- " ++ (if null fp then "built-in" else "defined in " ++ fp))

reload
  :: (Dimonad p, UI p)
  => Maybe FilePath -> UIT p UIError ()
reload fpo
  = unload fpo `bind` \ fps ->
    multiload fps

unload
  :: (Dimonad p, UI p)
  => Maybe FilePath -> UIT p UIError [FilePath]
unload fpo
  = (fromMaybe [] **>) . tryAndPrintErrMsg $
    case fpo of
      Just fp
        -> dummyCast "unload, 1" //> getModCxt `bind` \ fps ->
           case List.findIndex (equiv fp) fps of
             Just n
               -> dummyCast "unload, 2" //> reverse . fmap fst **> sequenceC (replicate (n + 1) pop)
             _
               -> raise (NotLoaded (Just fp))
      _
        -> dummyCast "unload, 3" //> pop `bind` \ (fp , _) ->
           if null fp
             then raise (NotLoaded Nothing)
             else invoke [fp]

multiload
  :: (Dimonad p, UI p)
  => [FilePath] -> UIT p UIError ()
multiload
  = let
      op fp rest
        = const () **> tryAndPrintErrMsg (after (load fp) rest)
      e = invoke ()
    in
    foldr op e

load
  :: (Dimonad p, UI p)
  => FilePath -> UIT p UIError ()
load fp
  = head //> mapT (strength . both id readFile) (variants fp) `bind` \ (realfp , input) ->
    after (putStr ("Reading file " ++ realfp ++ " ... ")) $ 
    after flush $
    dummyCast "load, 1" //> bimap id unzip7 . unzip **> getAll `bind` \ (fps , (_ , openvs , dmaps , tmaps , uidmaps , tenvs , kenvs)) ->
    LexErr //> point (fellLexerT (lexAll input)) `bind` \ us ->
    DecommentErr //> point (fst **> fellDecommenterT decommentAll us) `bind` \ lexs ->
    let indentAnal = fellIndentT parseAll defaultRel (defaultRange , NoAlign) in
    ParseErr //> point (fst . fst . fst **> fellParserT indentAnal lexs) `bind` \ iseq ->
    DisambiguationErr //> point (bimap id head **> fellDisambiguatorT (disambiguateProg iseq) openvs) `bind` \ (ast , openv) ->
    DevelopmentErr //> point (fst **> fellDeveloperT (developProg ast)) `bind` \ ast' -> 
    ScopeInferErr //> point (bimap id (snd . head) **> fellScopeInferencerT (scopeInferProg realfp ast') (dmaps , tmaps) (zip fps uidmaps)) `bind` \ ((_ , (dmap , tmap)) , uidmap) ->
    TypeInferErr //> point (fst **> fellTypeInferencerT (typeInferProg ast') 0 (unions tenvs , unions kenvs) (openv : openvs)) `bind` \ (tenv , kenv) ->
    after (putStrLn "done") $
    dummyCast "load, 2" //> push (realfp , (ast' , openv , dmap , tmap , uidmap , tenv , kenv))

quit
  :: (Bifunctor p, Dipointed p)
  => UIT p e ()
quit
  = invoke ()

help
  :: (Dimonad p, UI p)
  => UIT p e ()
help
  = let
      m = maximum (fmap (length . fst) cmdTable)
      table
        = fmap (\ (cmd , hlp) -> take (m + 3) (':' : cmd ++ repeat ' ') ++ hlp) cmdTable
    in
    after (mapC putStrLn table) $
    invoke ()

promptAndEdit
  :: forall r p
  .  (UI p, Mut r p)
  => UIT p UIError ()
promptAndEdit
  = getModCxt `bind` \ mods ->
    lift (after (putStr (prompt (fmap modName mods))) $ after flush $ edit) `bind` \ line ->
    parse line

start
  :: forall r p
  .  (UI p, Mut r p)
  => p UIError ()
start
  = putStr logo ~**
    putStrLn versionInfo ~**
    bimap id fst (fellUIT promptAndEdit [("" , (Prog [] [] [] :@ Unspec , primOpMap , primEnv , primTyEnv , primUIdMap , primVarTyEnv , primConKiEnv))])
