{-# LANGUAGE
      GeneralizedNewtypeDeriving,
      TypeOperators,
      FlexibleContexts
    #-}
module ScopeInferencer (
    module ScopeInferencer.Data,
    module ScopeInferencer
  ) where


import Data.Monoid (Endo(..))
import Utils
import Control.Diapplicative
import Control.Diapplicative.Trans
import Data.MapUtils
import Address
import Stack
import Syntax.AST
import ScopeInferencer.Data


newtype ScopeInferencerT p e a
  = ScopeInferencerT { runScopeInferencerT :: ReaderT ([UIdEnv] , [UIdEnv]) (StackT (String , UIdMap) p) e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, Reader ([UIdEnv] , [UIdEnv]), StackState (String , UIdMap))

fellScopeInferencerT
  :: ScopeInferencerT p e a -> ([UIdEnv] , [UIdEnv]) -> [(String , UIdMap)] -> p e (a , [(String , UIdMap)])
fellScopeInferencerT p env maps
  = fellStackT (runReaderT (runScopeInferencerT p) env) maps


instance Transformer ScopeInferencerT where
  
  lift
    = ScopeInferencerT . lift . lift
  

askUId
  :: (Dimonad p, Reader ([UIdEnv] , [UIdEnv]) p)
  => NamSpc -> String -> p e UId
askUId spc nam
  = (if spc == DataSpc then fst else snd) **> ask `bind` \ maps ->
    dummyCast "askUId" //> lookupsM nam maps

withEnv
  :: (Reader ([UIdEnv] , [UIdEnv]) p)
  => NamSpc -> UIdEnv -> p e a -> p e a
withEnv spc map
  = local (if spc == DataSpc then bimap (map :) id else bimap id (map :))

newUIdMap
  :: (StackState (String , UIdMap) p)
  => String -> p e ()
newUIdMap fn
  = dummyCast "newUIdMap" //> push (fn , empty)

getFreeUId
  :: (StackState (String , UIdMap) p)
  => p e Int
getFreeUId
  = dummyCast "getFreeUId" //> sum . fmap (size . snd) **> getAll

getAddr
  :: (Dimonad p, StackState (String , UIdMap) p)
  => UId -> p e (String , Addr)
getAddr uid
  = dummyCast "getAddr, 1" //> getAll `bind` \ ps ->
    dummyCast "getAddr, 2" //> mapT (strength . bimap id ((locationOf **>) . lookupM uid)) ps

withIntRepCheck
  :: (Dimonad p, StackState (String , UIdMap) p)
  => p ScopeInferError (a , Endo [(String , UId)]) -> p ScopeInferError (a , UIdEnv)
withIntRepCheck p
  = p `bind` \ (res , delta) ->
      (
      fromListM (appEndo delta []) `catch` \ (MultiplyBound _ uid1 uid2) ->
      negation $
        invoke ScopeClash ***
        getAddr uid1 ***
        (snd **> getAddr uid2)
      ) `bind` \ env ->
    invoke (res , env)

withExtRepCheck
  :: (Dimonad p, Reader ([UIdEnv], [UIdEnv]) p, StackState (String , UIdMap) p)
  => p ScopeInferError (a , UIdEnv) -> p ScopeInferError (a , UIdEnv)
withExtRepCheck p
  = p `bind` \ (res , tymap) ->
    const (res , tymap) **> negation (mapT (strength . both id (askUId TypeSpc)) (keys tymap)) `catch` \ (nam , ouid) ->
    dummyCast "withExtRepCheck" //> lookupM nam tymap `bind` \ nuid ->
    negation $
      invoke ScopeClash ***
      getAddr ouid ***
      (snd **> getAddr nuid)


storeBinding
  :: (Dimonad p, StackState (String , UIdMap) p, Writer (Endo [(String , UId)]) p)
  => String -> UId -> Addr -> p e ()
storeBinding nam uid addr
  = after (tell (Endo ((nam , uid) :))) $
    dummyCast "storeBinding, 1" //> pop `bind` \ (fn , uidmap) ->
    dummyCast "storeBinding, 2" //> push (fn , insert uid (nam :@ addr) uidmap)

registerNewCon
  :: (Dimonad p, StackState (String , UIdMap) p, Writer (Endo [(String , UId)]) p)
  => ConAST String :@ Addr -> p e (ConAST UId :@ Addr)
registerNewCon (Con nam :@ addr)
  = getFreeUId `bind` \ uid ->
    after (storeBinding nam uid addr) $
    invoke (Con uid :@ addr)

registerNewVar
  :: (Dimonad p, StackState (String , UIdMap) p, Writer (Endo [(String , UId)]) p)
  => VarAST String :@ Addr -> p e (VarAST UId :@ Addr)
registerNewVar (Var nam :@ addr)
  = getFreeUId `bind` \ uid ->
    after (storeBinding nam uid addr) $
    invoke (Var uid :@ addr)


scopeInferProg
  :: (Dimonad p, StackState (String , UIdMap) p, Reader ([UIdEnv] , [UIdEnv]) p)
  => String ->
     ProgAST String (TyRHSAST String :@ Addr) (PatAST String :@ Addr) (ExprAST String :@ Addr) :@ Addr ->
     p ScopeInferError (ProgAST UId (TyRHSAST UId :@ Addr) (PatAST UId :@ Addr) (ExprAST UId :@ Addr) :@ Addr , (UIdEnv , UIdEnv))
scopeInferProg fn (Prog idecls tdecls eqs :@ addr)
  = after (newUIdMap fn) $ 
    scopeInferTyEqs tdecls `bind` \ (tdecls' , (env , tyenv)) -> 
    withEnv DataSpc env (withEnv TypeSpc tyenv (scopeInferEqs DataSpc eqs)) `bind` \ (eqs' , env') -> 
    invoke (Prog idecls tdecls' eqs' :@ addr , (union env env' , tyenv))

scopeInferTyEqs
  :: (Dimonad p, Reader ([UIdEnv] , [UIdEnv]) p, StackState (String , UIdMap) p)
  => [TyEqAST (ConAST String :@ Addr) (TyRHSAST String :@ Addr) :@ Addr] ->
     p ScopeInferError ([TyEqAST (ConAST UId :@ Addr) (TyRHSAST UId :@ Addr) :@ Addr] , (UIdEnv , UIdEnv))
scopeInferTyEqs teqs
  = let ((lhss , rhss) , addrs) = bimap separate id (separate teqs) in
    withExtRepCheck (withIntRepCheck (runWriterT (mapC registerNewCon lhss))) `bind` \ (lhss' , tymap) ->
    withEnv TypeSpc tymap (withIntRepCheck (runWriterT (mapC scopeInferTyRHS rhss))) `bind` \ (rhss' , conmap) ->
    invoke (zipWith (:@) (zipWith TyEq lhss' rhss') addrs , (conmap , tymap))

scopeInferTyRHS
  :: (Dimonad p, Reader ([UIdEnv], [UIdEnv]) p, StackState (String , UIdMap) p, Writer (Endo [(String, UId)]) p)
  => TyRHSAST String :@ Addr -> p ScopeInferError (TyRHSAST UId :@ Addr)
scopeInferTyRHS (TyRHS pats temps :@ addr)
  = withIntRepCheck (runWriterT (mapC (scopeInferPat TypeSpc) pats)) `bind` \ (pats' , tymap) ->
    withEnv TypeSpc tymap (mapC scopeInferTemp temps) `bind` \ temps' ->
    invoke (TyRHS pats' temps' :@ addr)

scopeInferTemp
  :: (Dimonad p, StackState (String , UIdMap) p, Writer (Endo [(String , UId)]) p, Reader ([UIdEnv] , [UIdEnv]) p)
  => TempAST String :@ Addr -> p ScopeInferError (TempAST UId :@ Addr)
scopeInferTemp (Temp con args :@ addr)
  = invoke (\ con' args' -> Temp con' args' :@ addr) ***
    registerNewCon con ***
    mapC (scopeInferExpr TypeSpc) args

scopeInferBlock
  :: (Dimonad p, Reader ([UIdEnv] , [UIdEnv]) p, StackState (String , UIdMap) p)
  => NamSpc -> 
     BlockAST (PatAST String :@ Addr) (ExprAST String :@ Addr) :@ Addr ->
     p ScopeInferError (BlockAST (PatAST UId :@ Addr) (ExprAST UId :@ Addr) :@ Addr , UIdEnv)
scopeInferBlock spc (Block idecls eqs :@ addr)
  = bimap (\ eqs' -> Block idecls eqs' :@ addr) id **> scopeInferEqs spc eqs

scopeInferEqs
  :: (Dimonad p, Reader ([UIdEnv] , [UIdEnv]) p, StackState (String , UIdMap) p)
  => NamSpc ->
     [EqAST (PatAST String :@ Addr) (ExprAST String :@ Addr) :@ Addr] ->
     p ScopeInferError ([EqAST (PatAST UId :@ Addr) (ExprAST UId :@ Addr) :@ Addr] , UIdEnv)
scopeInferEqs spc eqs
  = let ((lhss , rhss) , addrs) = bimap separate id (separate eqs) in
    withIntRepCheck (runWriterT (mapC (scopeInferPat spc) lhss)) `bind` \ (lhss' , env) ->
    withEnv spc env (mapC (scopeInferExpr spc) rhss) `bind` \ rhss' ->
    invoke (zipWith (:@) (zipWith Eq lhss' rhss') addrs , env)

scopeInferPat
  :: (Dimonad p, Reader ([UIdEnv] , [UIdEnv]) p, StackState (String , UIdMap) p, Writer (Endo [(String , UId)]) p)
  => NamSpc -> PatAST String :@ Addr -> p ScopeInferError (PatAST UId :@ Addr)
scopeInferPat _   (VarPat (Var nam :@ iaddr) :@ oaddr)
  = (\ var -> VarPat var :@ oaddr) **> registerNewVar (Var nam :@ iaddr)
scopeInferPat _   (WildPat :@ addr)
  = invoke (WildPat :@ addr)
scopeInferPat spc (ConPat (Con nam :@ iaddr) pats :@ oaddr)
  = invoke (\ uid pats' -> ConPat (Con uid :@ iaddr) pats' :@ oaddr) ***
    (const (UndefCon iaddr) //> askUId spc nam) ***
    mapC (scopeInferPat spc) pats

scopeInferExpr
  :: (Dimonad p, Reader ([UIdEnv] , [UIdEnv]) p, StackState (String , UIdMap) p)
  => NamSpc -> ExprAST String :@ Addr -> p ScopeInferError (ExprAST UId :@ Addr)
scopeInferExpr spc (AtomExpr atom :@ addr)
  = (\ atom' -> AtomExpr atom' :@ addr) **> scopeInferAtomExpr spc atom
scopeInferExpr spc (App fun arg :@ addr)
  = invoke (\ fun' arg' -> App fun' arg' :@ addr) ***
    scopeInferExpr spc fun ***
    scopeInferExpr spc arg
scopeInferExpr spc (Lam mod pat expr :@ addr)
  = withIntRepCheck (runWriterT (scopeInferPat spc pat)) `bind` \ (pat' , env) ->
    withEnv spc env (scopeInferExpr spc expr) `bind` \ expr' ->
    invoke (Lam mod pat' expr' :@ addr)
scopeInferExpr spc (Local block expr :@ addr)
  = scopeInferBlock spc block `bind` \ (block' , env) ->
    withEnv spc env (scopeInferExpr spc expr) `bind` \ expr' ->
    invoke (Local block' expr' :@ addr)

scopeInferAtomExpr
  :: (Dimonad p, Reader ([UIdEnv], [UIdEnv]) p)
  => NamSpc -> AtomExprAST String :@ Addr -> p ScopeInferError (AtomExprAST UId :@ Addr)
scopeInferAtomExpr spc (VarExpr (Var nam :@ iaddr) :@ oaddr)
  = const (UndefVar iaddr) //> 
    (\ uid -> VarExpr (Var uid :@ iaddr) :@ oaddr) **> askUId spc nam
scopeInferAtomExpr spc (ConExpr (Con nam :@ iaddr) :@ oaddr)
  = const (UndefCon iaddr) //> 
    (\ uid -> ConExpr (Con uid :@ iaddr) :@ oaddr) **> askUId spc nam
scopeInferAtomExpr _   (Lit lit :@ addr)
  = invoke (Lit lit :@ addr)
