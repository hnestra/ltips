module Phase 
  ( module Phase 
  ) where


data Phase
  = Lexing
  | Decommenting
  | Parsing
  | Disambiguation
  | Development
  | ScopeInference
  | TypeInference
  | Interpretation


instance Show Phase where
  
  show Lexing
    = "lexing"
  show Decommenting
    = "decommenting"
  show Parsing
    = "parsing"
  show Disambiguation
    = "disambiguation"
  show Development
    = "development"
  show ScopeInference
    = "scope inference"
  show TypeInference
    = "type inference"
  show Interpretation
    = "interpretation"
  
