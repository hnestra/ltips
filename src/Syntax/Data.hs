module Syntax.Data 
  ( module Syntax.Data
  ) where


import Data.Int
import Data.Map
import Utils
import Rel
import Syntax.AST


defaultRange
  :: Range (Inf Int)
defaultRange
  = minCol := inf

defaultRel
  :: DblRel Int
defaultRel
  = ge


type OpMap
  = Map String (Int8 , Int8)

minPrior
  :: (Integral a)
  => a
minPrior
  = 0

limPrior
  :: Integer
limPrior
  = fromIntegral (maxBound :: Int8) + 1

tempSepNam
  :: String
tempSepNam
  = "|"

arrNam
  :: String
arrNam
  = "->"

exnNam
  :: String
exnNam
  = "Exn"

intNam
  :: String
intNam
  = "Int"

floatNam
  :: String
floatNam
  = "Float"

charNam
  :: String
charNam
  = "Char"

stringNam
  :: String
stringNam
  = "String"

starNam
  :: String
starNam
  = "TYPE"

evalMode
  :: String -> EvalMode
evalMode "."
  = Normal
evalMode "?"
  = Backtrack
evalMode "!"
  = Postponed
evalMode _
  = error "evalMode: Panic! Not an EvalMode operator!"

primOpMap
  :: OpMap
primOpMap
  = fromList $
    addAssoc (124 , 125) [""] $
    addAssoc (127 , 4) [".", "?", "!", "{}"] $
    addAssoc (1 , 0) [tempSepNam] $
    addAssoc (17 , 16) [arrNam] $
    []


data RawError e
  = EOF
  | UnexpectedToken e
  | MisalignedToken e
  deriving (Eq)


instance (Show e) => Show (RawError e) where
  
  show EOF
    = "Unexpected end of file"
  show (UnexpectedToken e)
    = "Unexpected token " ++ show e
  show (MisalignedToken e)
    = "Misaligned token " ++ show e
  

instance Functor RawError where
  
  fmap f (UnexpectedToken e)
    = UnexpectedToken (f e)
  fmap f (MisalignedToken e)
    = MisalignedToken (f e)
  fmap _ _
    = EOF
  

