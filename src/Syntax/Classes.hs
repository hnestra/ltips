{-# LANGUAGE
      FunctionalDependencies,
      MultiParamTypeClasses,
      TypeOperators
    #-}
module Syntax.Classes
  ( module Syntax.Classes
  ) where


import Control.Diapplicative
import Utils
import Address
import Syntax.Data


class (Mixable p, LazyTriable p, Catenative p) => Parser t a p | p -> t a where
  
  consume
    :: (t -> Either e e) -> p (RawError (e :@ a)) (t :@ a)
  

consumeIf
  :: (Parser t a p)
  => (t -> Bool) -> p (RawError (t :@ a)) (t :@ a)
consumeIf pred
  = consume (wrap pred)


class Failure e where
  
  failAddr
    :: e -> Addr
  

instance (Locatable a) => Failure (RawError a) where
  
  failAddr (UnexpectedToken a)
    = locationOf a
  failAddr (MisalignedToken a)
    = locationOf a
  failAddr _ 
    = Unspec
  

