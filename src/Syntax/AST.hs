{-# LANGUAGE
      FlexibleInstances,
      MultiParamTypeClasses,
      TypeOperators,
      DeriveFunctor
    #-}
module Syntax.AST (
  module Syntax.AST.Classes,
  module Syntax.AST.Infix,
  module Syntax.AST
) where


import Data.Bifunctor
import Data.Utils
import Utils
import Data.SetUtils hiding (foldl, foldr)
import Syntax.AST.Classes
import Syntax.AST.Infix


data ProgAST nam tyrhs lhs rhs
  = Prog [InfixDeclAST :@ Addr] [TyEqAST (ConAST nam :@ Addr) tyrhs :@ Addr] [EqAST lhs rhs :@ Addr]
data TyEqAST lhs rhs
  = TyEq lhs rhs
  deriving (Functor)

data TyRHSAST nam
  = TyRHS [PatAST nam :@ Addr] [TempAST nam :@ Addr]
data TempAST nam
  = Temp (ConAST nam :@ Addr) [ExprAST nam :@ Addr]
data PatAST nam
  = VarPat (VarAST nam :@ Addr)
  | ConPat (ConAST nam :@ Addr) [PatAST nam :@ Addr]
  | WildPat
data ExprAST nam
  = AtomExpr (AtomExprAST nam :@ Addr)
  | App (ExprAST nam :@ Addr) (ExprAST nam :@ Addr)
  | Lam EvalMode (PatAST nam :@ Addr) (ExprAST nam :@ Addr)
  | Local (BlockAST (PatAST nam :@ Addr) (ExprAST nam :@ Addr) :@ Addr) (ExprAST nam :@ Addr)
data AtomExprAST nam
  = VarExpr (VarAST nam :@ Addr)
  | ConExpr (ConAST nam :@ Addr)
  | Lit (LitAST :@ Addr)
data VarAST nam
  = Var nam
  deriving (Show)
data ConAST nam
  = Con nam
  deriving (Show)
data LitAST
  = Char String
  | String String
  | Num String
  deriving (Show)
data EvalMode
  = Normal
  | Backtrack
  | Postponed
  deriving (Show, Eq)


instance (Show nam, Show tyrhs, Show lhs, Show rhs) => Show (ProgAST nam tyrhs lhs rhs) where

  showsPrec lev (Prog infixs types eqs)
    = joinWith " "
        [
        ("Block" ++),
        inBracket (joinWith ", " (fmap (showsPrec lev) infixs)),
        inBracket (joinWith ", " (fmap (showsPrec lev) types)),
        inBracket (joinWith ", " (fmap (showsPrec lev) eqs))
        ]
  

instance (Show lhs, Show rhs) => Show (TyEqAST lhs rhs) where
  
  showsPrec lev (TyEq tycon rhs)
    = joinWith " "
        [
        ("TyEq" ++),
        inParen (showsPrec lev tycon),
        inParen (showsPrec lev rhs)
        ]
  

instance (Show nam) => Show (TyRHSAST nam) where
  
  showsPrec lev (TyRHS args temps)
    = joinWith " "
        [
        ("TyRHS" ++),
        inBracket (joinWith ", " (fmap (showsPrec lev) args)),
        inBracket (joinWith ", " (fmap (showsPrec lev) temps))
        ]


instance (Show nam) => Show (TempAST nam) where
  
  showsPrec lev (Temp con args)
    = joinWith " "
        [
        ("Temp" ++),
        inParen (showsPrec lev con),
        inBracket (joinWith ", " (fmap (showsPrec lev) args))
        ]
  

instance (Show nam) => Show (PatAST nam) where
  
  showsPrec lev (VarPat var)
    = joinWith " "
        [
        ("VarPat" ++),
        inParen (showsPrec lev var)
        ]
  showsPrec lev (ConPat con args)
    = joinWith " "
        [
        ("ConPat" ++),
        inParen (showsPrec lev con),
        showsPrec lev args
        ]
  showsPrec _   WildPat
    = ("WildCard" ++)
  

instance (Show nam) => Show (ExprAST nam) where
  
  showsPrec lev (AtomExpr atom)
    = joinWith " "
        [
        ("AtomExpr" ++),
        inParen (showsPrec lev atom)
        ]
  showsPrec lev (App fun arg)
    = joinWith " "
        [
        ("App" ++),
        inParen (showsPrec lev fun),
        inParen (showsPrec lev arg)
        ]
  showsPrec lev (Lam typ arg rhs)
    = joinWith " "
        [
        ("Lam" ++),
        showsPrec lev typ,
        inParen (showsPrec lev arg),
        inParen (showsPrec lev rhs)
        ]
  showsPrec lev (Local block expr)
    = joinWith " "
        [
        ("Local" ++),
        inParen (showsPrec lev block),
        inParen (showsPrec lev expr)
        ]
  

instance (Show nam) => Show (AtomExprAST nam) where
  
  showsPrec lev (VarExpr var)
    = joinWith " "
        [
        ("VarExpr" ++),
        inParen (showsPrec lev var)
        ]
  showsPrec lev (ConExpr con)
    = joinWith " "
        [
        ("ConExpr" ++),
        inParen (showsPrec lev con)
        ]
  showsPrec lev (Lit lit)
    = joinWith " "
        [
        ("Lit" ++),
        inParen (showsPrec lev lit)
        ]
  

instance Bifunctor TyEqAST where
  
  bimap f g (TyEq lhs rhs)
    = TyEq (f lhs) (g rhs)
  

instance Separable TyEqAST where
  
  separate
    = foldr (\ (TyEq lhs rhs) (lhss , rhss) -> (lhs : lhss , rhs : rhss)) ([] , [])
  

vars
  :: (Ord nam) 
  => BlockAST (PatAST nam :@ Addr) (ExprAST nam :@ Addr) :@ Addr -> (Set nam , Set nam) -> (Set nam , Set nam)
vars (Block _ eqs :@ _) (bvs , fvs)
  = let ((lhss , rhss) , _) = bimap separate id (separate eqs) in
    let bvs' = foldl (flip bvarsHelper) bvs lhss in
    (bvs' , foldl (\ acc lhs -> fvarsHelper lhs (bvs' , acc)) fvs rhss)

fvars
  :: (Varying v a)
  => a -> Set v
fvars u
  = fvarsHelper u (empty , empty)

bvars
  :: (Binding v a)
  => a -> Set v
bvars u
  = bvarsHelper u empty

cons
  :: (Constructed c a)
  => a -> Set c
cons u
  = consHelper u empty


instance (Ord nam) => Varying nam (ExprAST nam :@ Addr) where
  
  fvarsHelper (AtomExpr atom :@ _) (bvs , fvs)
    = fvarsHelper atom (bvs , fvs)
  fvarsHelper (App e1 e2 :@ _)     (bvs , fvs)
    = fvarsHelper e1 (bvs , fvarsHelper e2 (bvs , fvs))
  fvarsHelper (Lam _ p e :@ _)     (bvs , fvs)
    = fvarsHelper e (bvarsHelper p bvs , fvs)
  fvarsHelper (Local b e :@ _)     (bvs , fvs)
    = fvarsHelper e (vars b (bvs , fvs))
  

instance (Ord nam) => Varying nam (AtomExprAST nam :@ Addr) where
  
  fvarsHelper (VarExpr (Var nam :@ _) :@ _) (bvs , fvs)
    | not (member nam bvs)
      = insert nam fvs
  fvarsHelper _                             (_   , fvs)
    = fvs
  

instance (Ord nam) => Binding nam (PatAST nam :@ Addr) where
  
  bvarsHelper (VarPat (Var nam :@ _)      :@ _) bvs
    = insert nam bvs
  bvarsHelper (ConPat (Con _   :@ _) args :@ _) bvs
    = foldl (flip bvarsHelper) bvs args
  bvarsHelper _                                 bvs
    = bvs
  

instance (Ord nam) => Constructed nam (TyRHSAST nam :@ Addr) where
  
  consHelper (TyRHS _ temps :@ _) cs
    = foldl (flip consHelper) cs temps
  

instance (Ord nam) => Constructed nam (TempAST nam :@ Addr) where
  
  consHelper (Temp _ exprs :@ _) cs
    = foldl (flip consHelper) cs exprs
  

instance (Ord nam) => Constructed nam (ExprAST nam :@ Addr) where
  
  consHelper (AtomExpr atom :@ _) cs
    = consHelper atom cs
  consHelper (App e1 e2     :@ _) cs
    = consHelper e2 (consHelper e1 cs)
  consHelper (Lam _ p e     :@ _) cs
    = consHelper e (consHelper p cs)
  consHelper (Local b e     :@ _) cs
    = consHelper e (consHelper b cs)
  

instance (Ord nam) => Constructed nam (AtomExprAST nam :@ Addr) where
  
  consHelper (ConExpr (Con nam :@ _) :@ _) cs
    = insert nam cs
  consHelper _                             cs
    = cs
  

instance (Ord nam) => Constructed nam (PatAST nam :@ Addr) where
  
  consHelper (ConPat (Con nam :@ _) args :@ _) cs
    = foldl (flip consHelper) (insert nam cs) args
  consHelper _                                 cs
    = cs
  

instance (Ord nam) => Constructed nam (BlockAST (PatAST nam :@ Addr) (ExprAST nam :@ Addr) :@ Addr) where
  
  consHelper (Block _ eqs :@ _) cs
    = foldl (flip consHelper) cs eqs
  

instance (Ord nam) => Constructed nam (EqAST (PatAST nam :@ Addr) (ExprAST nam :@ Addr) :@ Addr) where
  
  consHelper (Eq lhs rhs :@ _) cs
    = consHelper rhs (consHelper lhs cs)
  

