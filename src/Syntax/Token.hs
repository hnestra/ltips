module Syntax.Token 
  ( module Syntax.Token
  ) where


import Data.Char
import Data.Utils


data AtomCat
  = NumLit
  | CharLit
  | StrLit
  | FunVar
  | FunCon
  | Wild
  deriving (Show, Eq)

data OpCat
  = OpVar
  | OpCon
  | LamDot
  | JP
  deriving (Show, Eq)

data KeyCat
  = InfixKey
  | TypeKey
  | EqKey
  deriving (Show, Eq)

data DelimCat
  = OpenParen
  | CloseParen
  | OpenBracket
  | CloseBracket
  | OpenBrace
  | CloseBrace
  | BgnCom
  | EndCom
  | LineCom
  deriving (Show, Eq)

data SepCat
  = Shift
  | NewLine
  deriving (Show, Eq)

data LexCat
  = Atom AtomCat
  | Op OpCat
  | Delim DelimCat
  | Sep SepCat
  | Key KeyCat
  deriving (Show, Eq)

data Lexeme
  = Lex LexCat String
  deriving (Show, Eq)


lexcat
  :: Lexeme -> LexCat
lexcat (Lex cat _)
  = cat

source
  :: Lexeme -> String
source (Lex _ str)
  = str


isFunVar
  :: Lexeme -> Bool
isFunVar (Lex cat _)
  = cat == Atom FunVar

isFunCon
  :: Lexeme -> Bool
isFunCon (Lex cat _)
  = cat == Atom FunCon

isChar
  :: Lexeme -> Bool
isChar (Lex cat _)
  = cat == Atom CharLit

isStr
  :: Lexeme -> Bool
isStr (Lex cat _)
  = cat == Atom StrLit

isNum
  :: Lexeme -> Bool
isNum (Lex cat _)
  = cat == Atom NumLit

isWild
  :: Lexeme -> Bool
isWild (Lex cat _)
  = cat == Atom Wild

isOpVar
  :: Lexeme -> Bool
isOpVar (Lex cat _)
  = cat == Op OpVar

isOpCon
  :: Lexeme -> Bool
isOpCon (Lex cat _)
  = cat == Op OpCon

isLamDot
  :: Lexeme -> Bool
isLamDot (Lex cat _)
  = cat == Op LamDot

isOpenParen
  :: Lexeme -> Bool
isOpenParen (Lex cat _)
  = cat == Delim OpenParen

isCloseParen
  :: Lexeme -> Bool
isCloseParen (Lex cat _)
  = cat == Delim CloseParen

isOpenBracket
  :: Lexeme -> Bool
isOpenBracket (Lex cat _)
  = cat == Delim OpenBracket

isCloseBracket
  :: Lexeme -> Bool
isCloseBracket (Lex cat _)
  = cat == Delim CloseBracket

isOpenBrace
  :: Lexeme -> Bool
isOpenBrace (Lex cat _)
  = cat == Delim OpenBrace

isCloseBrace
  :: Lexeme -> Bool
isCloseBrace (Lex cat _)
  = cat == Delim CloseBrace

isBgnCom
  :: Lexeme -> Bool
isBgnCom (Lex cat _)
  = cat == Delim BgnCom

isEndCom
  :: Lexeme -> Bool
isEndCom (Lex cat _)
  = cat == Delim EndCom

isLineCom
  :: Lexeme -> Bool
isLineCom (Lex cat _)
  = cat == Delim LineCom

isShift
  :: Lexeme -> Bool
isShift (Lex cat _)
  = cat == Sep Shift

isNewLine
  :: Lexeme -> Bool
isNewLine (Lex cat _)
  = cat == Sep NewLine

isInfix
  :: Lexeme -> Bool
isInfix (Lex cat _)
  = cat == Key InfixKey

isType
  :: Lexeme -> Bool
isType (Lex cat _)
  = cat == Key TypeKey

isEq
  :: Lexeme -> Bool
isEq (Lex cat _)
  = cat == Key EqKey


isFun
  :: Lexeme -> Bool
isFun
  = isFunVar `orelse` isFunCon

isOp
  :: Lexeme -> Bool
isOp
  = isOpVar `orelse` isOpCon `orelse` isLamDot

isLit
  :: Lexeme -> Bool
isLit
  = isChar `orelse` isStr `orelse` isNum

isAtom
  :: Lexeme -> Bool
isAtom
  = isFun `orelse` isLit `orelse` isWild

isCom
  :: Lexeme -> Bool
isCom
  = isBgnCom `orelse` isEndCom `orelse` isLineCom

isParen
  :: Lexeme -> Bool
isParen
  = isOpenParen `orelse` isCloseParen

isBrace
  :: Lexeme -> Bool
isBrace
  = isOpenBrace `orelse` isCloseBrace

isBracket
  :: Lexeme -> Bool
isBracket
  = isOpenBracket `orelse` isCloseBracket

isKey
  :: Lexeme -> Bool
isKey
  = isInfix `orelse` isType `orelse` isEq

isOpen
  :: Lexeme -> Bool
isOpen
  = isOpenParen `orelse` isOpenBracket `orelse` isOpenBrace

isClose
  :: Lexeme -> Bool
isClose
  = isCloseParen `orelse` isCloseBracket `orelse` isCloseBrace

isSep
  :: Lexeme -> Bool
isSep
  = isShift `orelse` isNewLine


isInt
  :: String -> Bool
isInt str
  = let
      nondigits
        = filter (not . isDigit) str
    in
    null nondigits || head nondigits `elem` "oOxX"

isPrefix
  :: String -> Bool
isPrefix nam@(~(c : _))
  = nam /= "" && (isAlpha c || c == '_')

