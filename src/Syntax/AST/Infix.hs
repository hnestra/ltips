{-# LANGUAGE
      TypeOperators,
      DeriveFunctor
    #-}
module Syntax.AST.Infix (
  module Address,
  module Syntax.AST.Infix
) where


import Data.Int
import Data.Bifunctor
import Data.Utils
import Utils
import Address
import Syntax.Token


data BlockAST lhs rhs
  = Block [InfixDeclAST :@ Addr] [EqAST lhs rhs :@ Addr]
data InfixDeclAST
  = InfixDecl (Int8 :@ Addr) (String :@ Addr) (Int8 :@ Addr)
data EqAST lhs rhs
  = Eq lhs rhs
  deriving (Functor)

data InfixElem
  = InfixElem OpAtom
  | PrefixElem OpAtom
  | FunElem FunAtom
  | EscElem (BlockAST [InfixElem :@ Addr] [InfixElem :@ Addr])
  | Sub [InfixElem :@ Addr]
data InfixLeaf
  = OpLeaf OpAtom
  | FunLeaf FunAtom
data InfixTree
  = Leaf InfixLeaf
  | OrdBranch (OpAtom :@ Addr) (InfixTree :@ Addr) (InfixTree :@ Addr)
  | AppBranch (InfixTree :@ Addr) (InfixTree :@ Addr)
  | LocalBranch (BlockAST (InfixTree :@ Addr) (InfixTree :@ Addr) :@ Addr) (InfixTree :@ Addr)

data OpAtom
  = OpAtom OpCat String
  deriving (Eq)
data FunAtom
  = FunAtom AtomCat String
  deriving (Eq)


appSeqOf
  :: InfixTree :@ Addr -> [InfixTree :@ Addr]
appSeqOf tree
  = let
      helper (AppBranch ut vt :@ _    )             acc
        = helper ut (vt : acc)
      helper (OrdBranch (atom :@ oaddr) ut vt :@ _) acc
        = (Leaf (OpLeaf atom) :@ oaddr) : ut : vt : acc
      helper other                                  acc
        = other : acc
    in
    helper tree []

lamSeqOf
  :: InfixTree :@ Addr -> ([(InfixTree :@ Addr , OpAtom :@ Addr)] , InfixTree :@ Addr)
lamSeqOf (tree :@ addr)
  = case tree of
      OrdBranch o@(OpAtom LamDot _ :@ _) l r
        -> bimap ((l , o) :) id (lamSeqOf r)
      _
        -> ([] , tree :@ addr)

ropSeqOf
  :: OpAtom -> InfixTree :@ Addr -> [InfixTree :@ Addr]
ropSeqOf o (tree :@ addr)
  = case tree of
      OrdBranch (o' :@ _) l r
        | o == o'
          -> l : ropSeqOf o r
      _
        -> [tree :@ addr]


instance (Show lhs, Show rhs) => Show (BlockAST lhs rhs) where

  showsPrec lev (Block infixs eqs)
    = joinWith " "
        [
        ("Block" ++),
        inBracket (joinWith ", " (fmap (showsPrec lev) infixs)),
        inBracket (joinWith ", " (fmap (showsPrec lev) eqs))
        ]
  

instance (Show lhs, Show rhs) => Show (EqAST lhs rhs) where
  
  showsPrec lev (Eq lhs rhs)
    = joinWith " "
        [
        ("Eq" ++),
        inParen (showsPrec lev lhs),
        inParen (showsPrec lev rhs)
        ]
  

instance Show InfixDeclAST where
  
  showsPrec lev (InfixDecl lpr op rpr)
    = joinWith " "
        [
        ("InfixDecl" ++),
        inParen (showsPrec lev lpr),
        inParen (showsPrec lev op),
        inParen (showsPrec lev rpr)
        ]
  

instance Show InfixElem where
  
  showsPrec lev (InfixElem atom)
    = joinWith " "
        [
        ("InfixElem" ++),
        inParen (showsPrec lev atom)
        ]
  showsPrec lev (PrefixElem atom)
    = joinWith " "
        [
        ("PrefixElem" ++),
        inParen (showsPrec lev atom)
        ]
  showsPrec lev (FunElem atom)
    = joinWith " " 
        [
        ("OrdElem" ++),
        inParen (showsPrec lev atom)
        ]
  showsPrec lev (EscElem bl)
    = joinWith " "
        [
        ("EscElem" ++),
        inParen (showsPrec lev bl)
        ]
  showsPrec lev (Sub els)
    = joinWith " "
        [
        ("Sub" ++),
        inBracket (joinWith ", " (fmap (showsPrec lev) els))
        ]
  

instance Show InfixLeaf where
  
  showsPrec lev (OpLeaf atom)
    = joinWith " "
        [
        ("OpLeaf" ++),
        inParen (showsPrec lev atom)
        ]
  showsPrec lev (FunLeaf atom)
    = joinWith " " 
        [
        ("FunLeaf" ++),
        inParen (showsPrec lev atom)
        ]
  

instance Show InfixTree where
  
  showsPrec lev (Leaf leaf)
    = joinWith " "
        [
        ("Leaf" ++),
        inParen (showsPrec lev leaf)
        ]
  showsPrec lev (AppBranch ut vt)
    = joinWith " "
        [
        ("AppBranch" ++),
        inParen (showsPrec lev ut),
        inParen (showsPrec lev vt)
        ]
  showsPrec lev (LocalBranch bl vt)
    = joinWith " "
        [
        ("LocalBranch" ++),
        inParen (showsPrec lev bl),
        inParen (showsPrec lev vt)
        ]
  showsPrec lev (OrdBranch el ut vt)
    = joinWith " "
        [
        ("OrdBranch" ++),
        inParen (showsPrec lev el),
        inParen (showsPrec lev ut),
        inParen (showsPrec lev vt)
        ]
  

instance Show OpAtom where
  
  showsPrec lev (OpAtom cat nam)
    = joinWith " "
        [
        ("OpAtom" ++),
        showsPrec lev cat,
        showsPrec lev nam
        ]
  

instance Show FunAtom where
  
  showsPrec lev (FunAtom cat nam)
    = joinWith " "
        [
        ("FunAtom" ++),
        showsPrec lev cat,
        showsPrec lev nam
        ]
  

instance Bifunctor EqAST where
  
  bimap f g (Eq lhs rhs)
    = Eq (f lhs) (g rhs)
  

instance Separable EqAST where
  
  separate
    = foldr (\ (Eq lhs rhs) (lhss , rhss) -> (lhs : lhss , rhs : rhss)) ([] , [])
  

infixIndent
  :: Int
infixIndent
  = 4

addNothing
  :: [String] -> [String]
addNothing t
  = fmap (replicate infixIndent ' ' ++) t

addTopLine
  :: String -> String
addTopLine s
  = '\x2554' : replicate (infixIndent - 2) '\x2550' ++ ' ' : s

addBotLine
  :: String -> String
addBotLine s
  = '\x255A' : replicate (infixIndent - 2) '\x2550' ++ ' ' : s

addVerLine
  :: [String] -> [String]
addVerLine t
  = fmap (('\x2551' :) . (replicate (infixIndent - 1) ' ' ++)) t

buildStep
  :: String -> ([String]  , [String]) -> ([String] , [String]) -> ([String] , [String])
buildStep op (l1 , m1 : r1) (l2 , m2 : r2)
  = (
      addNothing l1 ++ addTopLine m1 : addVerLine r1
    , 
      op : addVerLine l2 ++ addBotLine m2 : addNothing r2
    )
buildStep _ _               _
  = error "buildStep: Panic! Empty strings!"


infixTreeHelper
  :: InfixTree :@ Addr -> ([String] , [String])
infixTreeHelper (Leaf leaf :@ _)
  = case leaf of
      OpLeaf (OpAtom _ nam)
        -> ([] , [nam])
      FunLeaf (FunAtom _ nam)
        -> ([] , [nam])
infixTreeHelper (OrdBranch (OpAtom _ op :@ _) arg1 arg2 :@ _l)
  = buildStep op (infixTreeHelper arg1) (infixTreeHelper arg2)
infixTreeHelper (AppBranch arg1 arg2 :@ _)
  = buildStep "" (infixTreeHelper arg1) (infixTreeHelper arg2)
infixTreeHelper (LocalBranch _ arg :@ _)
  = buildStep "" ([] , ["{ ... }"]) (infixTreeHelper arg)

infixTree
  :: InfixTree :@ Addr -> [String]
  -- Assumes that infixIndent is at least 2
infixTree tree
  = let
      (l , m : r)
        = infixTreeHelper tree
    in
    fmap tail $
      addNothing l ++
      addBotLine m :
      addNothing r

