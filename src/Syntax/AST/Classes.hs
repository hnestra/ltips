{-# LANGUAGE
      FunctionalDependencies,
      MultiParamTypeClasses
    #-}
module Syntax.AST.Classes 
  ( module Syntax.AST.Classes
  ) where


import Data.SetUtils


class Varying v a | a -> v where
  
  fvarsHelper
    :: a -> (Set v , Set v) -> Set v
  

class Binding v a | a -> v where
  
  bvarsHelper
    :: a -> Set v -> Set v
  

class Constructed c a | a -> c where
  
  consHelper
    :: a -> Set c -> Set c
  

