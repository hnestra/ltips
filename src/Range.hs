module Range 
  ( module Range
  ) where


data Range a
  = a := a
  deriving (Show)

infix 5 :=


only
  :: (Enum a)
  => a -> Range a
only n
  = n := succ n

size
  :: (Num a)
  => Range a -> a
size (s := t)
  = t - s

start
  :: Range a -> a
start (s := _)
  = s


data RangeOrdering
  = LTRange
  | InRange
  | GTRange
  deriving (Show, Eq, Ord, Enum)


rangeCompare
  :: (Ord a)
  => a -> Range a -> RangeOrdering
rangeCompare x (s := t)
  | x < s
    = LTRange
  | x < t
    = InRange
  | otherwise
    = GTRange

isIn
  :: (Ord a)
  => a -> Range a -> Bool
x `isIn` ran
  = rangeCompare x ran == InRange


meet
  :: (Ord a)
  => Range a -> Range a -> Maybe (Range a)
meet (sx := tx) (sy := ty)
  = let
      sz = max sx sy
      tz = min tx ty
    in
    if sz < tz
      then Just (sz := tz)
      else Nothing
