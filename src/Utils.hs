{-# LANGUAGE 
      TupleSections
    #-}
module Utils 
  ( module Utils 
  ) where


import Data.List
import Data.Bifunctor


dummyCast
  :: String -> a
dummyCast msg
  = error ("Dummy type cast invoked by  " ++ show msg ++ " ")

assert
  :: (a -> Bool) -> a -> a
assert p x
  | p x
    = x
  | otherwise
    = error "assert: value does not satisfy the required predicate"

wrap
  :: (a -> Bool) -> a -> Either a a
wrap p a
  = (if p a then Right else Left) a

spanRev
  :: Int -> [a] -> ([a], [a])
spanRev n xs
  = let
      span n (z : zs) acc
        | n > 0
          = span (n - 1) zs (z : acc)
      span _ xs       acc
        = (acc , xs)
    in
    span n xs []

inParen
  :: ShowS -> ShowS
inParen s
  = ("(" ++) . s . (")" ++)

inBracket
  :: ShowS -> ShowS
inBracket s
  = ("[" ++) . s . ("]" ++)

joinWith
  :: String -> [ShowS] -> ShowS
joinWith str
  = foldr (.) id . intersperse (str ++)

zipMatching
  :: [a] -> [a] -> ([(a , a)] , [a])
zipMatching (x : xs) (y : ys)
  = bimap ((x , y) :) id (zipMatching xs ys)
zipMatching []       us
  = ([] , us)
zipMatching us       []
  = ([] , us)


showAmount
  :: Int -> String -> String
showAmount n str
  = shows n (' ' : str ++ if n == 1 then "" else "s")

reorganize
  :: (a , [(b , a)]) -> ([(b , a)] , a)
reorganize (x , ps)
  = case ps of
      (t , y) : rest
        -> let
             (qs , u)
               = reorganize (y , rest)
           in
           ((t , x) : qs , u)
      _
        -> ([] , x)

addAssoc
  :: a -> [b] -> [(b , a)] -> [(b , a)]
addAssoc p
  = flip (foldr ((:) . (, p)))


{-
-- halvad meetodite nimed, peaks olema võrdse kaaluga
class CartProd f where
  
  cont
    :: f a b -> a
  
  amd
    :: f a b -> b
  
  toPair
    :: f a b -> (a , b)
  
  fromPair
    :: (a , b) -> f a b
  
  toPair p
    = (cont p , amd p)
  
  cont
    = fst . toPair
  
  amd
    = snd . toPair


separate
  :: (CartProd f)
  => [f a b] -> f [a] [b]
separate
  = fromPair . unzip . map toPair


instance CartProd (,) where
  
  cont
    = fst
  
  amd
    = snd
  
  toPair
    = id
  
  fromPair
    = id
  

instance CartProd (:@) where
  
  toPair (t :@ u)
    = (t , u)
  
  fromPair (t , u)
    = t :@ u

-}
{-
deepZipWith
  :: (a -> b -> c) -> [[a]] -> [b] -> [[c]]
deepZipWith (*) zss as
  = let
      shift
        = do
            x : xs <- get
            put xs
            return x
      deep (*) (zs : zss)
        = do
            ps <- mapM (\ z -> fmap (z *) shift) zs
            pss <- deep (*) zss
            return (ps : pss)
      deep _   _
        = return []
    in
    fst $ runState (deep (*) zss) as
-}

