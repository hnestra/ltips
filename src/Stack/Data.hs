module Stack.Data 
  ( module Stack.Data
  ) where


data StackError
  = EmptyStack
  | OutOfBounds Int (Int , Int)
  deriving (Eq)


