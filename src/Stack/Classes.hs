{-# LANGUAGE
     FunctionalDependencies,
     MultiParamTypeClasses
   #-}
module Stack.Classes 
  ( module Stack.Classes
  ) where


import Data.Bifunctor
import Stack.Data


class (Bifunctor f) => StackState s f | f -> s where
  
  push
    :: s -> f StackError ()
  
  pop
    :: f StackError s
  
  top
    :: Int -> f StackError s
  
  getAll
    :: f StackError [s]
  
  modifyAll
    :: (s -> s) -> f StackError ()
  

class (StackState s f) => DoubleStackState s f where
  
  new
    :: [s] -> f StackError ()
  
  restore
    :: f StackError ()
  

