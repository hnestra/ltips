{-# LANGUAGE
     GeneralizedNewtypeDeriving,
     UndecidableInstances,
     MultiParamTypeClasses,
     FlexibleInstances
   #-}
module Stack (
    module Stack.Data,
    module Stack.Classes,
    module Stack
  ) where


import Control.Diapplicative
import Control.Diapplicative.Trans
import Pool
import Stack.Data
import Stack.Classes


newtype StackT s f e a
  = StackT { runStackT :: StateT [s] f e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, Reader r, Writer w, Pool c)

fellStackT
  :: StackT s f e a -> [s] -> f e (a , [s])
fellStackT
  = runStateT . runStackT

mapStackT
  :: (([s] -> f e (a , [s])) -> ([s'] -> f' e' (a' , [s']))) -> StackT s f e a -> StackT s' f' e' a'
mapStackT f
  = StackT . StateT . f . runStateT . runStackT


newtype DblStkT s f e a
  = DblStkT { runDblStkT :: StateT [[s]] f e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, Reader r, Writer w)

fellDblStkT
  :: DblStkT s f e a -> [[s]] -> f e (a, [[s]])
fellDblStkT
  = runStateT . runDblStkT

mapDblStkT
  :: (([[s]] -> f e (a , [[s]])) -> ([[s']] -> f' e' (a' , [[s']]))) -> DblStkT s f e a -> DblStkT s' f' e' a'
mapDblStkT f
  = DblStkT . StateT . f . runStateT . runDblStkT


instance Transformer (StackT s) where
  
  lift
    = StackT . lift
  

instance Transformer (DblStkT s) where
  
  lift
    = DblStkT . lift
  

instance (Bifunctor f, Dipointed f) => StackState s (StackT s f) where
  
  push s
    = StackT $ modify (s :)
  
  pop
    = StackT $ StateT $ \ stk@(~(s : ss)) ->
      if null stk
        then raise EmptyStack
        else invoke (s , ss)
  
  top n
    = StackT $ StateT $ \ stk ->
      let len = length stk in
      if 0 <= n && n < len
        then invoke (stk !! n , stk)
        else raise (OutOfBounds n (0 , len - 1))
  
  getAll
    = StackT $ get
  
  modifyAll f
    = StackT $ modify (fmap f)
  

instance (StackState s f) => StackState s (ReaderT r f) where
  
  push
    = lift . push
  
  pop
    = lift pop
  
  top
    = lift . top
  
  getAll
    = lift getAll
  
  modifyAll
    = lift . modifyAll
  

instance (StackState s f, Monoid w) => StackState s (WriterT w f) where
  
  push
    = lift . push
  
  pop
    = lift pop
  
  top
    = lift . top
  
  getAll
    = lift getAll
  
  modifyAll
    = lift . modifyAll
  

instance (Bifunctor f, Dipointed f) => StackState s (DblStkT s f) where
  
  push s
    = DblStkT $ StateT $ \ stk@(~(l : ls)) ->
      if null stk
        then raise EmptyStack
        else invoke (() , (s : l) : ls)
  
  pop
    = DblStkT $ StateT $ \ stk@(~(l@(~(s : ss)) : ls)) ->
      if null stk || null l
        then raise EmptyStack
        else invoke (s , ss : ls)
  
  top n
    = DblStkT $ StateT $ \ stk@(~(l : _)) ->
      if null stk
        then raise EmptyStack
        else let len = length l in
             if 0 <= n && n < len
               then invoke (l !! n , stk)
               else raise (OutOfBounds n (0 , len - 1))
  
  getAll
    = DblStkT $ StateT $ \ stk@(~(l : _)) ->
      if null stk
        then raise EmptyStack
        else invoke (l , stk)
  
  modifyAll f
    = DblStkT $ StateT $ \ stk@(~(l : ls)) ->
      if null stk
        then raise EmptyStack
        else invoke (() , fmap f l : ls)
  

instance (Bifunctor f, Dipointed f) => DoubleStackState s (DblStkT s f) where
  
  new l
    = DblStkT $ StateT $ \ stk ->
      invoke (() , l : stk)
  
  restore
    = DblStkT $ StateT $ \ stk@(~(_ : ls)) ->
      if null stk
        then raise EmptyStack
        else invoke (() , ls)
  
