{-# LANGUAGE 
      GeneralizedNewtypeDeriving,
      FlexibleContexts, 
      TypeOperators
    #-}
module Developer
  ( DeveloperT(..)
  , fellDeveloperT
  , developProg
  , developExpr
  , developTyExpr
  ) where


import Control.Diapplicative
import Control.Diapplicative.Trans
import Utils
import Syntax
import Developer.Data


newtype DeveloperT p e a
  = DeveloperT { runDeveloperT :: WriterT Addr p e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, Writer Addr)

fellDeveloperT
  :: DeveloperT p e a -> p e (a , Addr)
fellDeveloperT p
  = runWriterT (runDeveloperT p)


instance Transformer DeveloperT where
  
  lift
    = DeveloperT . lift
  

checkCon
  :: PatAST String :@ Addr -> Either DevelopmentError (ConAST String :@ Addr)
checkCon (pat :@ addr)
  = case pat of
      ConPat con _
        -> Right $ con
      VarPat _
        -> Left $ VarAsCon addr
      WildPat
        -> Left $ WildAsCon addr

patOfLeaf
  :: (Dipointed p)
  => InfixLeaf :@ Addr -> p DevelopmentError (PatAST String)
patOfLeaf (OpLeaf (OpAtom cat nam) :@ addr)
  = case cat of
      OpVar
        -> invoke $ VarPat (Var nam :@ addr)
      OpCon
        -> invoke $ ConPat (Con nam :@ addr) []
      LamDot
        -> raise $ LamPat addr
      _
        -> dummyCast "patOfLeaf"
patOfLeaf (FunLeaf (FunAtom cat nam) :@ addr)
  = case cat of
      FunVar
        -> invoke $ VarPat (Var nam :@ addr)
      FunCon
        -> invoke $ ConPat (Con nam :@ addr) []
      Wild
        -> invoke $ WildPat
      _
        -> raise $ LitPat addr

exprOfLeaf
  :: (Dipointed p)
  => InfixLeaf :@ Addr -> p DevelopmentError (ExprAST String)
exprOfLeaf (OpLeaf (OpAtom cat nam) :@ addr)
  = case cat of
      OpVar
        -> invoke $ AtomExpr (VarExpr (Var nam :@ addr) :@ addr)
      OpCon
        -> invoke $ AtomExpr (ConExpr (Con nam :@ addr) :@ addr)
      _
        -> dummyCast "exprOfLeaf"
exprOfLeaf (FunLeaf (FunAtom cat nam) :@ addr)
  = case cat of
      FunVar
        -> invoke $ AtomExpr (VarExpr (Var nam :@ addr) :@ addr)
      FunCon
        -> invoke $ AtomExpr (ConExpr (Con nam :@ addr) :@ addr)
      NumLit
        -> invoke $ AtomExpr (Lit (Num nam :@ addr) :@ addr)
      StrLit
        -> invoke $ AtomExpr (Lit (String nam :@ addr) :@ addr)
      CharLit
        -> invoke $ AtomExpr (Lit (Char nam :@ addr) :@ addr)
      _
        -> raise $ WildExpr addr

typeOfLeaf
  :: (Dipointed p)
  => InfixLeaf :@ Addr -> p DevelopmentError (ExprAST String)
typeOfLeaf (OpLeaf (OpAtom cat nam) :@ addr)
  = case cat of
      OpVar
        -> invoke $ AtomExpr (ConExpr (Con nam :@ addr) :@ addr)
      OpCon
        -> invoke $ AtomExpr (ConExpr (Con nam :@ addr) :@ addr)
      _
        -> dummyCast "typeOfLeaf"
typeOfLeaf (FunLeaf (FunAtom cat nam) :@ addr)
  = case cat of
      FunVar
        -> invoke $ AtomExpr (VarExpr (Var nam :@ addr) :@ addr)
      FunCon
        -> invoke $ AtomExpr (ConExpr (Con nam :@ addr) :@ addr)
      Wild
        -> raise $ WildExpr addr
      _
        -> raise $ LitType addr


developProg
  :: (Catenative p, Mixable p, Writer Addr p)
  => ProgAST String (InfixTree :@ Addr) (InfixTree :@ Addr) (InfixTree :@ Addr) :@ Addr ->
     p DevelopmentError (ProgAST String (TyRHSAST String :@ Addr) (PatAST String :@ Addr) (ExprAST String :@ Addr) :@ Addr)
developProg (Prog idecls tdecls eqs :@ addr)
  = invoke (\ ts es -> Prog idecls ts es :@ addr) ***
    mapC developTyEq tdecls ***
    mapC developEq eqs

developTyEq
  :: (Catenative p, Mixable p, Writer Addr p)
  => TyEqAST lhs (InfixTree :@ Addr) :@ Addr -> 
     p DevelopmentError (TyEqAST lhs (TyRHSAST String :@ Addr) :@ Addr)
developTyEq (TyEq con rhs :@ addr)
  = (\ r -> TyEq con r :@ addr) **>
    developTyRHS rhs

developTyRHS
  :: (Catenative p, Mixable p, Writer Addr p)
  => InfixTree :@ Addr -> p DevelopmentError (TyRHSAST String :@ Addr)
developTyRHS rhs@(_ :@ addr)
  = let
      (args , body)
        = lamSeqOf rhs
    in
    invoke (\ as ts -> TyRHS as ts :@ addr) ***
    mapC developTyPat args ***
    mapC developTemp (ropSeqOf (OpAtom OpVar tempSepNam) body)

developTyPat
  :: (Catenative p, Writer Addr p)
  => (InfixTree :@ Addr, OpAtom :@ Addr) -> 
     p DevelopmentError (PatAST String :@ Addr)
developTyPat (l :@ laddr , OpAtom _ dot :@ oaddr)
  = let
      mode
        = evalMode dot
    in
    case mode of
      Normal
        -> listenAfterTelling laddr $
           case l of
             Leaf (FunLeaf (FunAtom FunVar nam))
               -> invoke (VarPat (Var nam :@ laddr))
             Leaf (FunLeaf (FunAtom Wild _))
               -> invoke WildPat
             _
               -> raise (IllegalTyPat laddr)
      _
        -> raise (AbnormalLamDot mode oaddr)

developTemp
  :: (Catenative p, Mixable p, Writer Addr p)
  => InfixTree :@ Addr -> p DevelopmentError (TempAST String :@ Addr)
developTemp expr@(_ :@ addr)
  = case appSeqOf expr of
      fun : args
        -> case fun of
             Leaf leaf :@ oaddr
               -> listenAfterTelling addr $
                    invoke Temp ***
                    ((listenAfterTelling oaddr $ patOfLeaf (leaf :@ oaddr)) `pbind` checkCon) ***
                    mapC developTyExpr args
             _
               -> developTemp fun
      _ -> error "developTemp: Panic: Empty application!"

developTyExpr
  :: (Catenative p, Writer Addr p)
  => InfixTree :@ Addr -> p DevelopmentError (ExprAST String :@ Addr)
developTyExpr (Leaf atom :@ addr)
  = listenAfterTelling addr $
    typeOfLeaf (atom :@ addr)
developTyExpr (OrdBranch (atom@(OpAtom cat _) :@ oaddr) ut vt :@ addr)
  = listenAfterTelling addr $ 
    case cat of
      LamDot
        -> raise (LamType addr)
      _
        -> invoke App ***
             (
             listenAddr $
               invoke (flip App) ***
               developTyExpr ut ***
               (listenAfterTelling oaddr $ typeOfLeaf (OpLeaf atom :@ oaddr))
             ) ***
           developTyExpr vt
developTyExpr (AppBranch ut vt :@ addr)
  = listenAfterTelling addr $
      invoke App ***
      developTyExpr ut ***
      developTyExpr vt
developTyExpr (LocalBranch _ _ :@ addr)
  = raise (LocalType addr)

developBlock
  :: (Catenative p, Mixable p, Writer Addr p)
  => BlockAST (InfixTree :@ Addr) (InfixTree :@ Addr) :@ Addr ->
     p DevelopmentError (BlockAST (PatAST String :@ Addr) (ExprAST String :@ Addr) :@ Addr)
developBlock (Block idecls eqs :@ addr)
  = (\ e -> Block idecls e :@ addr) **>
    mapC developEq eqs

developEq
  :: (Catenative p, Mixable p, Writer Addr p)
  => EqAST (InfixTree :@ Addr) (InfixTree :@ Addr) :@ Addr ->
     p DevelopmentError (EqAST (PatAST String :@ Addr) (ExprAST String :@ Addr) :@ Addr)
developEq (Eq lhs rhs :@ addr)
  = invoke (\ l r -> Eq l r :@ addr) ***
    developPat lhs ***
    developExpr rhs

developPat
  :: (Catenative p, Mixable p, Writer Addr p)
  => InfixTree :@ Addr -> p DevelopmentError (PatAST String :@ Addr)
developPat (Leaf atom       :@ addr)
  = listenAfterTelling addr $
    patOfLeaf (atom :@ addr)
developPat (LocalBranch (_ :@ addr) _ :@ _)
  = raise $ BlockPat addr
developPat tree@(_ :@ addr)
  = case appSeqOf tree of
      fun : args
        -> case fun of
             Leaf leaf :@ oaddr
               -> listenAfterTelling addr $
                    invoke ConPat ***
                    ((listenAfterTelling oaddr $ patOfLeaf (leaf :@ oaddr)) `pbind` checkCon) ***
                    mapC developPat args
             _
               -> developPat fun
      _ -> error "developPat: Panic! Empty application!"

developExpr
  :: (Catenative p, Mixable p, Writer Addr p)
  => InfixTree :@ Addr -> p DevelopmentError (ExprAST String :@ Addr)
developExpr (Leaf atom :@ addr)
  = listenAfterTelling addr $
    exprOfLeaf (atom :@ addr)
developExpr (OrdBranch (atom@(OpAtom cat nam) :@ oaddr) ut vt :@ addr)
  = listenAfterTelling addr $ 
    case cat of
      LamDot
        -> invoke (Lam (evalMode nam)) ***
           developPat ut ***
           developExpr vt
      _
        -> invoke App ***
             (
             listenAddr $
               invoke (flip App) ***
               developExpr ut ***
               (listenAfterTelling oaddr $ exprOfLeaf (OpLeaf atom :@ oaddr))
             ) ***
           developExpr vt
developExpr (AppBranch ut vt :@ addr)
  = listenAfterTelling addr $
      invoke App ***
      developExpr ut ***
      developExpr vt
developExpr (LocalBranch bl vt :@ addr)
  = listenAfterTelling addr $
      invoke Local ***
      developBlock bl ***
      developExpr vt
