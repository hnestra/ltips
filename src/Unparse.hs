{-# LANGUAGE
      MultiParamTypeClasses,
      FunctionalDependencies,
      GeneralizedNewtypeDeriving
  #-}
module Unparse 
  ( InfixAST(..)
  , Unparse(..)
  , unparse
  , UnparseT(..)
  , fellUnparseT
  ) where


import Control.Diapplicative
import Control.Diapplicative.Trans
import Utils
import Data.MapUtils
import Lexer.Utils
import Syntax.Data


data InfixAST n
  = Primitive n
  | Compound n (InfixAST n) (InfixAST n)

class (Dimonad p) => Unparse t p | p -> t where
  
  askOpMaps
    :: p e [OpMap]
  destruct
    :: t -> p e (InfixAST String)
  

priorLB
  :: (Integral a)
  => a
priorLB
  = minPrior - 1

formatPrimitive
  :: String -> ShowS
formatPrimitive n
  | all isOpSym n
    = inParen (n ++)
  | otherwise
    = (n ++)
    

formatCompound
  :: Bool -> String -> ShowS -> ShowS -> ShowS
formatCompound isProtected n larg rarg
  | isProtected
    = locres
  | otherwise
    = inParen locres
  where
    connective
      = if null n then " " else " " ++ n ++ " "
    locres
      = larg . (connective ++) . rarg

lookupPriorities
  :: (Unparse t f)
  => String -> f e (Integer , Integer)
lookupPriorities n
  = dummyCast ("lookupPriorities, " ++ n) //> 
    bimap toInteger toInteger **> 
    (askOpMaps `bind` lookupsM n)

unparses
  :: (Unparse t f)
  => (Integer , Integer) -> InfixAST String -> f e ShowS
unparses (lpr , rpr) t
  = case t of
      Primitive n
        -> invoke (formatPrimitive n)
      Compound n lt rt
        -> lookupPriorities n `bind` \ (lpr' , rpr') ->
           let isProtected = lpr < lpr' && rpr < rpr' in
           let lprs = if isProtected then (lpr , lpr') else (priorLB , lpr') in
           let rprs = if isProtected then (rpr' , rpr) else (rpr' , priorLB) in
           invoke (formatCompound isProtected n) ***
           unparses lprs lt ***
           unparses rprs rt

unparse
  :: (Unparse t f)
  => t -> f e String
unparse
  = destruct @@@ unparses (priorLB , priorLB) @@@ invoke . ($ "")


newtype UnparseT t p e a
  = UnparseT { runUnparseT :: ReaderT [OpMap] p e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, Reader [OpMap])

fellUnparseT
  :: UnparseT t p e a -> [OpMap] -> p e a
fellUnparseT p opmaps
  = runReaderT (runUnparseT p) opmaps


instance Transformer (UnparseT t) where
  
  lift
    = UnparseT . lift
  

