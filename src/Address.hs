{-# LANGUAGE
     FlexibleContexts,
     FlexibleInstances,
     TypeOperators
   #-}
module Address 
  ( module Address
  ) where


import Data.Bifunctor
import Data.Utils


data Pos 
  = Int :. Int
  deriving (Eq, Ord)
data Addr
  = Unspec
  | Pos :- Pos
  deriving (Eq, Ord)


infix 7 :.
infix 6 :-


rowNo, colNo
  :: Pos -> Int
rowNo (r :. _)
  = r
colNo (_ :. c)
  = c


posPlus
  :: Pos -> Pos -> Pos
posPlus (roworig :. colorig) (row :. col)
  = case compare row 0 of
      GT
        -> (row + roworig) :. col
      EQ
        -> roworig :. (col + colorig)
      _
        -> error "posPlus for negative rowshift not implemented"

posMinus
  :: Pos -> Pos -> Pos
posMinus (roworig :. colorig) (row :. col)
  = case compare row roworig of
      GT
        -> (row - roworig) :. col
      EQ
        -> 0 :. (col - colorig)
      LT
        -> error "posMinus for negative rowshift not implemented"

addrSize
  :: Addr -> Pos
addrSize (row :. col :- endpos)
  = posMinus (row :. (col - 1)) endpos
addrSize _
  = error "addrSize for Unspec not implemented"

addrPlus
  :: Pos -> Addr -> Addr
addrPlus orig (bgn :- end)
  = posPlus orig bgn :- posPlus orig end
addrPlus _    _
  = error "addrPlus for Unspec not implemented"

addrMinus
  :: Pos -> Addr -> Addr
addrMinus orig (bgn :- end)
  = posMinus orig bgn :- posMinus orig end
addrMinus _    _
  = error "addrMinus for Unspec not implemented"

endpos
  :: Addr -> Pos
endpos (_ :- end)
  = end
endpos _
  = error "endpos for Unspec not implemented"


instance Show Pos where
  
  showsPrec lev (x :. y) acc
    = showsPrec lev x $
      (":" ++) $
      showsPrec lev y $
      acc
  

instance Show Addr where
  
  showsPrec lev (p1 :- p2) acc
    = showsPrec lev p1 $
      ("--" ++) $
      showsPrec lev p2 $
      acc
  showsPrec _   _          acc
    = "Unspec" ++ acc
  

instance Semigroup Addr where
  
  (bgn1 :- end1) <> (bgn2 :- end2)
    = min bgn1 bgn2 :- max end1 end2
  addr      <> Unspec
    = addr
  _         <> addr
    = addr
  

instance Monoid Addr where
  
  mempty
    = Unspec
  

infixl 1 :@

data t :@ u
  = t :@ u
  deriving (Eq, Ord)


instance Separable (:@) where

  separate
    = foldr (\ (t :@ addr) (ts , addrs) -> (t : ts , addr : addrs)) ([] , [])
  

instance (Show t, Show u, Eq u, Monoid u) => Show (t :@ u) where
  
  showsPrec lev (elem :@ addr)
    = showsPrec lev elem .
      if addr == mempty
        then id 
        else (" at " ++) . showsPrec lev addr
  

instance Functor ((:@) t) where
  
  fmap f (elem :@ addr)
    = elem :@ f addr
  

instance Bifunctor (:@) where
  
  bimap f g (t :@ a)
    = f t :@ g a
  

formatError
  :: (Show t, Show u, Eq u, Monoid u)
  => t -> u -> String -> String
formatError pass addr msg
  = "Error during " ++ shows (pass :@ addr) (": " ++ msg)


class Locatable a where

  locationOf
    :: a -> Addr
  

instance Locatable (a :@ Addr) where
  
  locationOf (_ :@ addr)
    = addr
  

instance (Locatable (a :@ b)) => Locatable (a :@ (b , c)) where
  
  locationOf
    = locationOf . bimap id fst
  
