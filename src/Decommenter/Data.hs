module Decommenter.Data 
  ( module Decommenter.Data
  ) where


import Address
import Phase


data DecommentError
  = UnmatchedBgnCom Addr Addr
  | UnmatchedEndCom Addr
  deriving (Eq)


instance Show DecommentError where
  
  show (UnmatchedBgnCom bgnaddr endaddr)
    = formatError Decommenting endaddr $
      "Block comment delimiter at " ++ show bgnaddr ++ " unmatched"
  show (UnmatchedEndCom addr)
    = formatError Decommenting addr $
      "Block comment delimiter unmatched"
  

