module Data.ListUtils (
  module Data.List,
  module Data.ListUtils
) where


import Data.List


merge
  :: (Ord a)
  => [a] -> [a] -> [a]
-- Assumes that both input lists are sorted
merge us@(x : xs) vs@(y : ys)
  | x <= y
    = x : merge xs vs
  | otherwise
    = y : merge us ys
merge us           []
  = us
merge _            vs
  = vs

