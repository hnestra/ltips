{-# LANGUAGE 
      MultiParamTypeClasses,
      FunctionalDependencies,
      FlexibleInstances
  #-}
module Data.RefUtils 
  ( module Data.RefUtils
  ) where


import Data.IORef
import Data.STRef
import Control.Monad.ST
import Control.Diapplicative


class (Dimonad f) => Mut r f | f -> r where
  
  newRef
    :: a -> f e (r a)
  readRef
    :: r a -> f e a
  writeRef
    :: a -> r a -> f e ()
  modifyRef
    :: (a -> a) -> r a -> f e ()
  
  modifyRef f r
    = readRef r `bind` \ a ->
      writeRef (f a) r
  
  writeRef a
    = modifyRef (const a)
  

instance Mut IORef (Symmetric IO) where
  
  newRef x
    = normal (newIORef x)
  readRef r
    = normal (readIORef r)
  writeRef x r
    = normal (writeIORef r x)
  modifyRef f r
    = normal (modifyIORef r f)
  
  
instance Mut (STRef s) (Symmetric (ST s)) where
  
  newRef x
    = normal (newSTRef x)
  readRef r
    = normal (readSTRef r)
  writeRef x r
    = normal (writeSTRef r x)
  modifyRef f r
    = normal (modifySTRef' r f)
  

