{-# LANGUAGE
      MultiParamTypeClasses
   #-}
module Data.Utils 
  (
  either,
  module Data.Utils
  ) where


import Data.Bifunctor


class Boolean b where
  
  true
    :: b
  
  false
    :: b
  
  andalso
    :: b -> b -> b
  
  orelse
    :: b -> b -> b
  
  no
    :: b -> b
  

instance Boolean Bool where
  
  true = True
  
  false = False
  
  andalso = (&&)
  
  orelse = (||)
  
  no = not
  

instance (Boolean b) => Boolean (a -> b) where
  
  true _
    = true
  
  false _
    = false
  
  (f `andalso` g) x
    = f x `andalso` g x
  
  (f `orelse` g) x
    = f x `orelse` g x
  
  no f x
    = no (f x)
  

both
  :: (a -> b) -> (a -> c) -> a -> (b , c)
both f g x
  = (f x , g x)


class (Bifunctor f) => Associative f where
  
  assocl
    :: f a (f b c) -> f (f a b) c
  
  assocr
    :: f (f a b) c -> f a (f b c)
  

instance Associative (,) where
  
  assocl (x , (y , z))
    = ((x , y) , z)
  
  assocr ((x , y) , z)
    = (x , (y , z))
  

instance Associative Either where
  
  assocl
    = either (Left . Left) (bimap Right id)
  
  assocr
    = either (bimap id Left) (Right . Right)
  

class (Bifunctor f) => Commutative f where
  
  commute
    :: f a b -> f b a
  

instance Commutative (,) where
  
  commute (x , y)
    = (y , x)
  

instance Commutative Either where
  
  commute
    = either Right Left
  

class (Bifunctor f, Bifunctor g) => Distributive f g where
  
  distrl
    :: f (g a b) c -> g (f a c) (f b c)
  
  distrr
    :: f a (g b c) -> g (f a b) (f a c)
  
  undistrl
    :: g (f a c) (f b c) -> f (g a b) c
  
  undistrr
    :: g (f a b) (f a c) -> f a (g b c)
  

instance Distributive (,) Either where
  
  distrl (Left a , z)
    = Left (a , z)
  distrl (Right b , z)
    = Right (b , z)
  
  distrr (x , Left a)
    = Left (x , a)
  distrr (x , Right b)
    = Right (x , b)
  
  undistrl
    = either (both (Left . fst) snd) (both (Right . fst) snd)
  
  undistrr
    = either (both fst (Left . snd)) (both fst (Right . snd))
  

infixr 7 **>
infixr 6 //>

(//>)
  :: (Bifunctor f)
  => (e1 -> e2) -> f e1 a -> f e2 a
(//>)
  = first

(**>)
  :: (Bifunctor f)
  => (a1 -> a2) -> f e a1 -> f e a2
(**>)
  = second


swasl
  :: (Associative f, Commutative f)
  => f a (f b c) -> f b (f a c)
swasl
  = assocr . bimap commute id . assocl

swasr
  :: (Associative f, Commutative f)
  => f (f a b) c -> f (f a c) b
swasr
  = assocl . bimap id commute . assocr


class (Bifunctor f) => Separable f where
  
  separate
    :: [f a b] -> ([a] , [b])
  

instance Separable (,) where
  
  separate
    = unzip
  
