module Data.MapUtils 
  (
  module Data.Map,
  MapError(..),
  lookupM,
  lookupsM,
  insertM,
  fromListM,
  invert,
  mapWithKeyC,
  restrictKeys,
  withoutKeys
  ) where


import Prelude hiding (lookup)
import Data.List hiding (lookup, insert)
import Data.Set (Set)
import Data.Map hiding (null, foldr, partition, take, restrictKeys, withoutKeys)
import qualified Data.Map as Map

import Control.Diapplicative


data MapError key val
  = KeyNotFound
  | KeyBoundTo val
  | MultiplyBound key val val
  | Other key [Map key val]
  deriving (Show, Eq)


instance Functor (MapError key) where
  
  fmap f (MultiplyBound key val1 val2)
    = MultiplyBound key (f val1) (f val2)
  fmap f (KeyBoundTo val)
    = KeyBoundTo (f val)
  fmap _ _
    = KeyNotFound 
  

sortM
  :: (Ord key, Dimonad p)
  => [(key , val)] -> [(key , val)] -> p (MapError key val) [(key , val)]
sortM ((key , val) : rest) acc
  = let
      (us , vs)
        = partition ((< key) . fst) rest
    in
    sortM vs acc `bind` \ acc' ->
    if not (null acc') && key == fst (head acc')
      then raise (MultiplyBound key val (snd (head acc')))
      else sortM us ((key , val) : acc')
sortM _                    acc
  = invoke acc
    
fromListM
  :: (Ord key, Dimonad p)
  => [(key , val)] -> p (MapError key val) (Map key val)
fromListM ps
  = fromAscList **> sortM ps []

lookupM
  :: (Ord key, Dipointed p)
  => key -> Map key val -> p (MapError key val) val
lookupM key map
  = case lookup key map of
      Just val
        -> invoke val
      _
        -> raise KeyNotFound

lookupsM
  :: (Ord key, Triable p)
  => key -> [Map key val] -> p (MapError key val) val
{-
lookupsM key (m : ms)
  = raise const ///
    lookupM key m ///
    lookupsM key ms
lookupsM _   _
  = raise KeyNotFound
-}
{-
lookupsM key list
  = foldr (///) (raise KeyNotFound) .
    fmap (bimap const id . lookupM key) $
    list
-}
lookupsM key list 
  = const KeyNotFound //> mapT (lookupM key) list

insertM
  :: (Ord key, Dipointed p)
  => key -> val -> Map key val -> p (MapError key val) (Map key val)
insertM key val map
  = case lookup key map of
      Just val
        -> raise (KeyBoundTo val)
      _
        -> invoke (insert key val map)

invert
  :: (Ord b)
  => Map a b -> Map b a
invert
  = fromList . fmap commute . toList

mapWithKeyC
  :: (Ord k, Catenative p)
  => (k -> a -> p e b) -> Map k a -> p e (Map k b)
mapWithKeyC f map
  = let
      list
        = toList map
      (keys , _)
        = unzip list
    in
    invoke (fromList . zip keys) ***
    mapC (uncurry f) list

restrictKeys
  :: (Ord k)
  => Set k -> Map k a -> Map k a
restrictKeys
  = flip Map.restrictKeys

withoutKeys
  :: (Ord k)
  => Set k -> Map k a -> Map k a
withoutKeys
  = flip Map.withoutKeys
