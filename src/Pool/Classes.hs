{-# LANGUAGE
      FunctionalDependencies,
      MultiParamTypeClasses,
      FlexibleInstances,
      UndecidableInstances
  #-}
module Pool.Classes 
  ( module Pool.Classes
  ) where


import Control.Diapplicative.Trans
import Data.Bifunctor


class (Bifunctor f) => Pool s f | f -> s where
  
  next
    :: f e s
  

instance (Pool c f) => Pool c (ReaderT r f) where
  
  next
    = lift next
  

instance (Pool c f, Monoid w) => Pool c (WriterT w f) where
  
  next
    = lift next
  

instance (Pool c f) => Pool c (StateT s f) where
  
  next
    = lift next
  
