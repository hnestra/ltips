module UI.FilePath 
  ( module UI.FilePath
  ) where


import Control.Monad
import Data.Maybe
import System.FilePath


langName
  :: String
langName
  = "λ-tips"

logoFile
  :: FilePath
logoFile
  = "logo" <.> "txt"

stdExts
  :: [String]
stdExts
  = ["tps", "ltips"]

trunk
  :: FilePath -> FilePath
trunk fp
  = fromJust (foldr mplus (Just fp) (fmap (flip stripExtension fp) stdExts))

modName
  :: FilePath -> String
modName ""
  = langName
modName fp
  = trunk (takeFileName fp)

equiv
  :: FilePath -> FilePath -> Bool
equiv fp1 fp2
  = let
      fp2'
        = trunk fp2
    in
    fp1 == fp2 || fp1 == takeFileName fp2 ||
    fp1 == fp2' || fp1 == takeFileName fp2'

variants
  :: FilePath -> [FilePath]
variants fp
  = fp : fmap (fp <.>) stdExts

