module UI.Editor
  ( edit
  ) where


import Prelude hiding (getLine, readLine, putStr, putStrLn, print)
import UI.Classes


edit
  :: (UI p)
  => p e String
edit
  = getLine

