module UI.Data
  ( module UI.Data
  ) where


import Data.Char
import Data.Maybe
import Data.List
import Utils
import Lexer.Data
import Decommenter.Data
import Parser.Data
import Disambiguator.Data
import Developer.Data
import ScopeInferencer.Data
import TypeInferencer.Data
import Interpreter.Data


highlight
  :: String -> String -> String
highlight color str
  = "\ESC[1;" ++ color ++ str ++ "\ESC[0m"

ansiblack
  :: String
ansiblack
  = "30m"

ansiblue
  :: String
ansiblue
  = "34m"

promptEnd
  :: String
promptEnd
  = "> "

prompt
  :: [String] -> String
prompt ws
  = highlight ansiblack (intercalate " " (reverse ws)) ++ promptEnd

cmdTable
  :: [(String , String)]
cmdTable
  = ("<expr>" , "Evaluate given expression") :
    sort
    [
      ("quit" , "Quit the interactive environment"),
      ("help" , "Print the list of commands available"),
      ("load <files>" , "Load definitions from given files, in given order"),
      ("unload <file>" , "Withdraw definitions from given file and all successor files"),
      ("unload" , "Withdraw definitions from the topmost file"),
      ("reload <file>" , "Reload definitions from given file and all successor files"),
      ("reload" , "Reload definitions from the topmost file"),
      ("info <names>" , "Information about given names"),
      ("view" , "List all names currently visible"),
      ("type <expr>" , "Infer the type of given expression"),
      ("kind <expr>" , "Infer the kind of given type expression"),
      ("draw <expr/pat>" , "Draw given expression or pattern as AST")
    ]


type Name
  = String

type Expr
  = String

data Cmd
  = Quit
  | Help
  | Load [FilePath]
  | Unload (Maybe FilePath)
  | Reload (Maybe FilePath)
  | Info [String]
  | View
  | Draw Expr
  | Type Expr
  | Kind Expr
  | Exec Expr
  | ErrMsg UIError

data UIError
  = UnknownCmd String
  | AmbiguousCmd String [String]
  | WrongUse String WrongArgNo
  | NotImplemented
  | FileNotFound FilePath
  | PermissionDenied FilePath
  | AlreadyInUse FilePath
  | InappropriateType FilePath
  | ResourceVanished FilePath
  | NotLoaded (Maybe FilePath)
  | LexErr LexError
  | DecommentErr DecommentError
  | ParseErr ParseError
  | DisambiguationErr DisambiguationError
  | DevelopmentErr DevelopmentError
  | ScopeInferErr ScopeInferError
  | TypeInferErr TypeInferError
  | InterpretErr InterpretationError

data WrongArgNo
  = Arg
  | MultipleArg
  | NoArg

instance Show UIError where
  
  show (UnknownCmd cmd)
    = "Command " ++ show cmd ++ " unknown"
  show (AmbiguousCmd cmd cmds)
    = "Command " ++ show cmd ++ " ambiguous: It can refer to " ++
      intercalate ", " cmds
  show (WrongUse cmd argno)
    = "Command " ++ show cmd ++ " " ++
      case argno of
        Arg
          -> "does not assume an argument"
        MultipleArg
          -> "does not work with multiple arguments"
        NoArg
          -> "assumes an argument"
  show NotImplemented
    = "Operation not implemented"
  show (FileNotFound fp)
    = "File " ++ fp ++ " not found"
  show (PermissionDenied fp)
    = "Opening file " ++ fp ++ " failed due to missing permission"
  show (AlreadyInUse fp)
    = "File " ++ fp ++ " is locked by another process"
  show (InappropriateType fp)
    = "File " ++ fp ++ " is not of normal type"
  show (ResourceVanished fp)
    = "File " ++ fp ++ " disappeared"
  show (NotLoaded fpo)
    = case fpo of
        Just fp
          -> "File " ++ fp ++ " not loaded"
        _
          -> "No file loaded"
  show (LexErr e)
    = show e
  show (DecommentErr e)
    = show e
  show (ParseErr e)
    = show e
  show (DisambiguationErr e)
    = show e
  show (DevelopmentErr e)
    = show e
  show (ScopeInferErr e)
    = show e
  show (TypeInferErr e)
    = show e
  show (InterpretErr e)
    = show e
  

select
  :: String -> String -> Cmd
select cmd arg
  | null cmds
    = ErrMsg (UnknownCmd cmd)
  | length cmds > 1
    = ErrMsg (AmbiguousCmd cmd cmds)
  | realcmd == "quit"
    = if null arg
        then Quit
        else ErrMsg (WrongUse realcmd Arg)
  | realcmd == "help"
    = if null arg
        then Help
        else ErrMsg (WrongUse realcmd Arg)
  | realcmd == "load"
    = Load (words arg)
  | realcmd == "unload"
    = let args = words arg in
      case args of
        _ : _ : _
          -> ErrMsg (WrongUse realcmd MultipleArg)
        _
          -> Unload (listToMaybe args)
  | realcmd == "reload"
    = let args = words arg in
      case args of
        _ : _ : _
          -> ErrMsg (WrongUse realcmd MultipleArg)
        _
          -> Reload (listToMaybe args)
  | realcmd == "info"
    = Info (words arg)
  | realcmd == "view"
    = if null arg
        then View
        else ErrMsg (WrongUse realcmd Arg)
  | realcmd == "type"
    = if null arg
        then ErrMsg (WrongUse realcmd NoArg)
        else Type arg
  | realcmd == "kind"
    = if null arg
        then ErrMsg (WrongUse realcmd NoArg)
        else Kind arg
  | realcmd == "draw"
    = if null arg
        then ErrMsg (WrongUse realcmd NoArg)
        else Draw arg
  | otherwise
    = dummyCast "select"
  where
    cmds
      = map head $
        group $
        filter (isPrefixOf cmd) $
        map (takeWhile (not . isSpace) . fst) $
        cmdTable
    realcmd
      = head cmds


logo
  :: String
logo
  = unlines
    [ " \\\\          /|    ()              "
    , "  \\\\         ||         ___     __ "
    , "  / \\   __  _||_   ||  | _ \\   / _|"
    , " / /\\\\       ||__  ||  ||_)|  _\\\\ "
    , "/_/  \\\\      \\__/  ||  | __/ |__/ "
    , "                       ||         "
    , "                       ||         "
    ]

versionInfo
  :: String
versionInfo
  = "Version 0.2, January 2025"

