{-# LANGUAGE
      FlexibleInstances
    #-}
module UI.Classes
  ( module UI.Classes
  ) where


import Prelude hiding (readFile, getLine, putStr, putStrLn, print)
import qualified System.IO as IO
import System.IO.Error
import Control.Diapplicative
import Control.Diapplicative.Trans
import UI.Data


class UI p where
  
  getLine
    :: p e String
  
  readFile
    :: FilePath -> p UIError String
  
  putStr
    :: String -> p e ()
  
  flush
    :: p e ()
  

putStrLn
  :: (UI p)
  => String -> p e ()
putStrLn s
  = putStr (s ++ "\n")

print
  :: (Show a, UI p)
  => a -> p e ()
print
  = putStrLn . show

tryAndPrintErrMsg
  :: (Dimonad p, UI p, Show e)
  => p e a -> p e (Maybe a)
tryAndPrintErrMsg p
  = Just **> p `catch` \ e ->
    after (print e) $
    invoke Nothing


instance (Bifunctor p, UI p, Transformer t) => UI (t p) where
  
  getLine
    = lift getLine
  
  readFile fp
    = lift (readFile fp)
  
  putStr s
    = lift (putStr s)
  
  flush
    = lift flush
  

instance UI (Symmetric IO) where
  
  getLine
    = normal (IO.getLine)
  
  readFile fp
    = let
        handler e
          | isDoesNotExistError e
            = return (Left (FileNotFound fp))
          | isPermissionError e
            = return (Left (PermissionDenied fp))
          | isAlreadyInUseError e
            = return (Left (AlreadyInUse fp))
          | ioeGetErrorString e == "inappropriate type"
            = return (Left (InappropriateType fp))
          | otherwise
            = ioError e
      in
      Symmetric (fmap Right (IO.readFile fp) `catchIOError` handler)
  
  putStr s
    = normal (IO.putStr s)
  
  flush
    = normal (IO.hFlush IO.stdout)
  
