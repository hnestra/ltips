{-# LANGUAGE 
      GeneralizedNewtypeDeriving,
      TypeOperators 
    #-}
module Lexer
  (
    LexerT(..),
    fellLexerT,
    LexError(..),
    readLexs,
    lexAll
  ) where


import Data.Maybe
import Control.Diapplicative
import qualified Data.Set as Set
import qualified Data.Map as Map
import Utils
import Address
import Syntax.Token
import Lexer.Utils
import Lexer.Data


newtype LexerT p e a
  = LexerT { runLexerT :: p e a }
  deriving (Functor, Bifunctor, Dipointed, Triable, Catenative)

fellLexerT
  :: LexerT p e a -> p e a
fellLexerT
  = runLexerT


type Trans a
  = Maybe Char -> Dec a

data Flag
  = Consume
  | Ignore
  | Backtrack Int

data Dec a
  = Cont Flag (Trans a)
  | NewLex (String -> a)
  | Stop
  | Error (Addr -> LexError)


formsLineCom
  :: String -> Bool
formsLineCom str
  = let
      (us , vs)
        = splitAt 2 str
    in
    us == "--" && all ('-' ==) vs

formsBgnCom
  :: String -> Bool
formsBgnCom (c : cs)
  = c == '!' && formsLineCom cs
formsBgnCom _
  = error "formsBgnCom: Panic! Empty operator!"

withKeyCheck
  :: (String -> Lexeme) -> String -> Lexeme
withKeyCheck h str
  = case Map.lookup str keyMap of
      Just cat
        -> Lex (Key cat) str
      _
        | formsBgnCom str
          -> Lex (Delim BgnCom) str
        | formsBgnCom (reverse str)
          -> Lex (Delim EndCom) str
        | formsLineCom str
          -> Lex (Delim LineCom) str
        | otherwise
          -> h str

withLamDotCheck
  :: (String -> Lexeme) -> String -> Lexeme
withLamDotCheck h str
  | isFullStop (head str) && str == take 1 str
    = Lex (Op LamDot) str
  | otherwise
    = h str

withWildCheck
  :: (String -> Lexeme) -> String -> Lexeme
withWildCheck h str
  | str == "_"
    = Lex (Atom Wild) str
  | otherwise
    = h str


lexAny
  :: Maybe Char -> Dec Lexeme
lexAny (Just c)
  | c == '\''
    = Cont Consume lexCharLit
  | c == '"'
    = Cont Consume lexStrLit
  | c == '0'
    = Cont Consume lexIntLit
  | isDigit c
    = Cont Consume lexNumLit
  | isUpper c
    = Cont Consume lexFunCon
  | isLower c || c == '_'
    = Cont Consume lexFunVar
  | isHalfStop c
    = Cont Consume lexOpCon
  | isOpSym c
    = Cont Consume lexOpVar
  | isBracketSym c
    = Cont Consume (lexDelim (fromJust (Map.lookup [c] bracketMap)))
  | c == ' '
    = Cont Consume lexShift
  | c == '\n'
    = Cont Consume lexNewLine
  | otherwise
    = Error (SpecChar c)
lexAny _
  = Stop

lexDigitalCodeWith
  :: (Maybe Char -> Dec a) -> Radix -> Trans a
lexDigitalCodeWith h radix (Just c)
  | c `isDigitOnBase` radix
    = Cont Consume (lexDigitalCodeWith h radix)
lexDigitalCodeWith h _     _
  = Cont (Backtrack 0) h

lexLongEscSeqWith
  :: Trans a -> Set.Set String -> Maybe Char -> Dec a
lexLongEscSeqWith h codes (Just c)
  | "" `Set.member` newcodes
    = Cont Consume h
  where
    newcodes
      = Set.map tail (Set.filter ((c ==) . head) codes)
lexLongEscSeqWith h _     _
  = Cont (Backtrack 0) h

lexEscSeqWith
  :: Trans a -> Set.Set String -> Trans a
lexEscSeqWith h codes (Just c)
  | Set.null newcodes
    = Error UnknownEscCode
  | "" `Set.member` newcodes
    = let
        reduced
          = Set.delete "" newcodes
      in
      if Set.null reduced
        then Cont Consume h
        else Cont Consume (lexLongEscSeqWith h reduced)
  | otherwise
    = Cont Consume (lexEscSeqWith h newcodes)
  where
    newcodes
      = Set.map tail (Set.filter ((c ==) . head) codes)
lexEscSeqWith h _     _
  = Cont (Backtrack 0) h
{-
lexIgnoredSpace
  :: Trans Lexeme
lexIgnoredSpace (Just c)
  | isNormalSpace c
    = Cont Ignore lexIgnoredSpace
lexIgnoredSpace other
  = lexStrLit other
-}
ignoreSpace
  :: Trans Lexeme
ignoreSpace (Just c)
  | isSpace c
    = Cont Ignore ignoreSpace
ignoreSpace _
  = Cont (Backtrack 0) lexStrLit

ignoreAny
  :: (Ord a, Num a)
  => a -> Trans Lexeme
ignoreAny n _
  | n > 0
    = Cont Ignore (ignoreAny (n - 1))
ignoreAny _ _
  = Cont Ignore ignoreSpace

lexPossibleLineBreak
  :: Trans Lexeme
lexPossibleLineBreak (Just c)
  | c == '\n'
    = Cont (Backtrack 1) (ignoreAny 2)
lexPossibleLineBreak _
  = Cont (Backtrack 0) (lexEscCharWith lexStrLit codeSet)

lexEscCharWith
  :: (Maybe Char -> Dec a) -> Set.Set String -> Maybe Char -> Dec a
lexEscCharWith h _     (Just c)
  | isDigit c
    = Cont Consume (lexDigitalCodeWith h Dec)
  | c `elem` "xX"
    = Cont Consume (lexDigitalCodeWith h Hex)
  | c `elem` "oO"
    = Cont Consume (lexDigitalCodeWith h Oct)
lexEscCharWith h codes other
    = lexEscSeqWith h codes other

lexTermCharWith
  :: (String -> a) -> p -> Dec a
lexTermCharWith shape _
  = NewLex shape

lexOnlyChar
  :: Maybe Char -> Dec Lexeme
lexOnlyChar (Just c)
  | c == '\''
    = Cont Consume (lexTermCharWith (Lex (Atom CharLit)))
lexOnlyChar _
  = Error NonTermCharLit

lexCharLit
  :: Trans Lexeme
lexCharLit (Just c)
  | c == '\\'
   = Cont Consume (lexEscCharWith lexOnlyChar codeSet)
  | c == '\''
    = Error EmptyCharLit
  | isNormal c
    = Cont Consume lexOnlyChar
  | otherwise
    = Cont (Backtrack 0) lexAny
lexCharLit other
  = lexOnlyChar other

lexStrLit
  :: Trans Lexeme
lexStrLit (Just c)
  | c == '"'
    = Cont Consume (lexTermCharWith (Lex (Atom StrLit)))
  | c == '\\'
    = Cont Consume lexPossibleLineBreak
  | isNormal c
    = Cont Consume lexStrLit
  | otherwise
    = Cont (Backtrack 0) lexAny
lexStrLit _
  = Error NonTermStrLit

lexDigitSeq
  :: Radix -> Trans Lexeme
lexDigitSeq radix (Just c)
  | isDigitOnBase c radix
    = Cont Consume (lexDigitSeq radix)
lexDigitSeq _     _
  = NewLex (Lex (Atom NumLit))

lexPossibleSign
  :: Maybe Char -> Dec Lexeme
lexPossibleSign (Just c)
  | isDigit c
    = Cont Consume (lexDigitSeq Dec)
lexPossibleSign _
  = Cont (Backtrack 2) (lexDigitSeq Dec)

lexPossibleExp
  :: Maybe Char -> Dec Lexeme
lexPossibleExp (Just c)
  | isDigit c
    = Cont Consume (lexDigitSeq Dec)
  | c `elem` "+-"
    = Cont Consume lexPossibleSign
lexPossibleExp _
  = Cont (Backtrack 1) (lexDigitSeq Dec)

lexFloatLit
  :: Trans Lexeme
lexFloatLit (Just c)
  | isDigit c
    = Cont Consume lexFloatLit
  | c `elem` "eE"
    = Cont Consume lexPossibleExp
lexFloatLit _
  = NewLex (Lex (Atom NumLit))

lexPossibleFloat
  :: Maybe Char -> Dec Lexeme
lexPossibleFloat (Just c)
  | isDigit c
    = Cont Consume lexFloatLit
lexPossibleFloat _
  = Cont (Backtrack 1) (lexDigitSeq Dec)

lexNumLit
  :: Trans Lexeme
lexNumLit (Just c)
  | isDigit c
    = Cont Consume lexNumLit
  | c == '.'
    = Cont Consume lexPossibleFloat
  | c `elem` "eE"
    = Cont Consume lexPossibleExp
lexNumLit _
  = NewLex (Lex (Atom NumLit))

lexPossibleRadix
  :: Radix -> Maybe Char -> Dec Lexeme
lexPossibleRadix radix (Just c)
  | isDigitOnBase c radix
    = Cont Consume (lexDigitSeq radix)
lexPossibleRadix _    _
  = Cont (Backtrack 1) (lexDigitSeq Dec)

lexIntLit
  :: Maybe Char -> Dec Lexeme
lexIntLit (Just c)
  | isDigit c
    = Cont Consume (lexNumLit)
  | c `elem` "xX"
    = Cont Consume (lexPossibleRadix Hex)
  | c `elem` "oO"
    = Cont Consume (lexPossibleRadix Oct)
lexIntLit _
  = NewLex (Lex (Atom NumLit))

lexFunCon
  :: Trans Lexeme
lexFunCon (Just c)
  | isAlpha c || isDigit c || c == '\'' || c == '_'
    = Cont Consume lexFunCon
lexFunCon _
  = NewLex (Lex (Atom FunCon))

lexFunVar
  :: Trans Lexeme
lexFunVar (Just c)
  | isAlpha c || isDigit c || c == '\'' || c == '_'
    = Cont Consume lexFunVar
lexFunVar _
  = NewLex (withWildCheck (withKeyCheck (Lex (Atom FunVar))))

lexOpCon
  :: Trans Lexeme
lexOpCon (Just c)
  | isOpSym c
    = Cont Consume lexOpCon
lexOpCon _
  = NewLex (withKeyCheck (Lex (Op OpCon)))

lexOpVar
  :: Trans Lexeme
lexOpVar (Just c)
  | isOpSym c
    = Cont Consume lexOpVar
lexOpVar _
  = NewLex (withLamDotCheck (withKeyCheck (Lex (Op OpVar))))

lexDelim
  :: DelimCat -> p -> Dec Lexeme
lexDelim cat _
  = NewLex (Lex (Delim cat))

lexShift
  :: Trans Lexeme
lexShift (Just c)
  | c == ' '
    = Cont Consume lexShift
lexShift _
  = NewLex (Lex (Sep Shift))

lexNewLine
  :: p -> Dec Lexeme
lexNewLine _
  = NewLex (Lex (Sep NewLine))


readLexs
  :: (Dipointed p)
  => String -> Addr -> Trans Lexeme -> String -> [p LexError (Lexeme :@ Addr)]
readLexs acc (ipos :- tpos) trans input@(~(c : cs))
  = let
      irow :. _
        = ipos
      trow :. tcol
        = tpos
      ipos'
        = if acc == "\n" then (irow + 1) :. 0 else ipos
      tpos'
        = if c == '\n' then (trow + 1) :. 1 else trow :. (tcol + 1)
    in
    case trans (listToMaybe input) of
      Cont Consume trans'
        -> readLexs (c : acc) (ipos :- tpos') trans' cs
      Cont Ignore trans'
        -> readLexs acc (ipos :- tpos') trans' cs
      Cont (Backtrack n) trans'
        -> let
             (back , acc')
               = spanRev n acc
           in
           readLexs acc' (ipos :- trow :. (tcol - n)) trans' (back ++ input)
      NewLex lexfun
        -> invoke (lexfun (reverse acc) :@ ipos' :- trow :. (tcol - 1)) :
           if null input 
           then [] 
           else readLexs "" (tpos :- tpos) lexAny input
      Stop
        -> []
      Error e
        -> [raise (e (ipos :- tpos))]
readLexs _   _              _     _
  = error "readLexs: Panic! Unspecified address!"

lexAll
  :: (Catenative p)
  => String -> p LexError [Lexeme :@ Addr]
lexAll
  = foldr (\ x xa -> invoke (:) *** x *** xa) (invoke []) .
    readLexs "" (1 :. 1 :- 1 :. 1) lexAny

