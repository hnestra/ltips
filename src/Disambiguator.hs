{-# LANGUAGE
      FlexibleContexts,
      TypeOperators,
      GeneralizedNewtypeDeriving
    #-}
module Disambiguator 
  (
    module Disambiguator.Data,
    module Disambiguator.Infix,
    module Disambiguator
  ) where


import Data.Int
import Data.Maybe
import Utils
import Control.Diapplicative
import Control.Diapplicative.Trans
import Data.MapUtils
import Stack
import Address
import Syntax
import Disambiguator.Data
import Disambiguator.Infix


newtype DisambiguatorT p e a
  = DisambiguatorT { runDisambiguatorT :: StackT OpMap p e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, StackState OpMap)

fellDisambiguatorT
  :: DisambiguatorT p e a -> [OpMap] -> p e (a , [OpMap])
fellDisambiguatorT p imap
  = runStateT (runStackT (runDisambiguatorT p)) imap


instance Transformer DisambiguatorT where
  
  lift
    = DisambiguatorT . lift
  

opMapOf
  :: (Dimonad p)
  => [InfixDeclAST :@ a] -> p (MapError String Addr) OpMap
opMapOf
  = bimap (fmap snd) (fmap fst) .
    fromListM . 
    fmap (\ (InfixDecl (lpr :@ _) (op :@ addr) (rpr :@ _) :@ _) -> (op , ((lpr , rpr) , addr)))

getPriorities
  :: (Dimonad p, StackState OpMap p)
  => String -> p (MapError String (Int8 , Int8)) (Int8 , Int8)
getPriorities op
  = dummyCast "getPriorities" //> getAll `bind` \ stk ->
    lookupsM op stk


disambiguateProg
  :: (Dimonad p, StackState OpMap p)
  => ProgAST String [InfixElem :@ Addr] [InfixElem :@ Addr] [InfixElem :@ Addr] :@ Addr -> 
     p DisambiguationError (ProgAST String (InfixTree :@ Addr) (InfixTree :@ Addr) (InfixTree :@ Addr) :@ Addr)
disambiguateProg (Prog idecls tdecls eqs :@ addr)
  = invoke (\ is ts es -> Prog is ts es :@ addr) ***
    disambiguateOpMap idecls ***
    mapC disambiguateTyEq tdecls ***
    mapC disambiguateEq eqs

disambiguateBlock
  :: (Dimonad p, StackState OpMap p)
  => BlockAST [InfixElem :@ Addr] [InfixElem :@ Addr] :@ Addr -> p DisambiguationError (BlockAST (InfixTree :@ Addr) (InfixTree :@ Addr) :@ Addr)
disambiguateBlock (Block idecls eqs :@ addr)
  = invoke (\ is es -> Block is es :@ addr) ***
    disambiguateOpMap idecls ***
    mapC disambiguateEq eqs

disambiguateOpMap
  :: (Dimonad p, StackState OpMap p)
  => [InfixDeclAST :@ Addr] -> p DisambiguationError [InfixDeclAST :@ Addr]
disambiguateOpMap is
  = (\ (MultiplyBound _ addr1 addr2) -> MultipleInfixDecl addr1 addr2) //>
    opMapOf is `bind` \ imap ->
    dummyCast "disambiguateOpMap" //> const is **> push imap

disambiguateTyEq
  :: (Dimonad p, StackState OpMap p)
  => TyEqAST (ConAST String :@ Addr) [InfixElem :@ Addr] :@ Addr -> 
     p DisambiguationError (TyEqAST (ConAST String :@ Addr) (InfixTree :@ Addr) :@ Addr)
disambiguateTyEq (TyEq con rhs :@ addr)
  = invoke (\ r -> TyEq con r :@ addr) ***
    disambiguateInfixTree rhs

disambiguateEq
  :: (Dimonad p, StackState OpMap p)
  => EqAST [InfixElem :@ Addr] [InfixElem :@ Addr] :@ Addr -> p DisambiguationError (EqAST (InfixTree :@ Addr) (InfixTree :@ Addr) :@ Addr)
disambiguateEq (Eq lhs rhs :@ addr)
  = invoke (\ l r -> Eq l r :@ addr) ***
    disambiguateInfixTree lhs ***
    disambiguateInfixTree rhs

disambiguateInfixTree
  :: (Dimonad p, StackState OpMap p)
  => [InfixElem :@ Addr] -> p DisambiguationError (InfixTree :@ Addr)
disambiguateInfixTree els
  = fromJust . findOrdArg . fst . fst . fst **> 
    fellInfixT shuntingyard [els] [[]]

shuntingyard
  :: (Dimonad p, StackState OpMap p)
  => InfixT p DisambiguationError OutStkElem
shuntingyard
  = after (condStar (applyInStk $ top 0) processElem) $
    after (collapse (minPrior - 1) Unspec) $
      dummyCast "shuntingyard" //> (applyOutStk $ pop)

processElem
  :: (Dimonad p, StackState OpMap p)
  => InfixT p DisambiguationError ()
processElem
  = dummyCast "processElem, 1" //> (applyInStk $ pop) `bind` \ elem@(dat :@ addr) ->
    case dat of
      InfixElem atom@(OpAtom _ nam)
        -> (\ KeyNotFound -> UndeclaredOp addr) //> lift (getPriorities nam) `bind` \ (lpr , _) ->
           collapse lpr addr `bind` \ _ ->
           dummyCast "processElem, 2" //> (applyOutStk $ push (Node (atom :@ addr)))
      _
        -> opt (applyOutStk $ top 0) `bind` \ prevo@(~(Just prev)) ->
           if isNothing prevo || isJust (findNode prev)
             then case dat of
                    Sub sub
                      -> processSub sub addr
                    EscElem block
                      -> lift (disambiguateBlock (block :@ addr)) `bind` \ (bl :@ _) ->
                         dummyCast "processElem, 3" //> (applyOutStk $ push (EscArg (bl :@ addr)))
                    PrefixElem atom
                      -> dummyCast "processElem, 4" //> (applyOutStk $ push (OrdArg (Leaf (OpLeaf atom) :@ addr)))
                    FunElem atom
                      -> dummyCast "processElem, 5" //> (applyOutStk $ push (OrdArg (Leaf (FunLeaf atom) :@ addr)))
             else dummyCast "processElem, 6" //>
                    (
                    applyInStk (push elem) `bind` \ _ ->
                    applyInStk (push (InfixElem (OpAtom JP (if isJust (findEscArg prev) then "{}" else "")) :@ Unspec))
                    )

processSub
  :: (Dimonad p, StackState OpMap p)
  => [InfixElem :@ Addr] -> Addr -> InfixT p DisambiguationError ()
processSub input addr
  = dummyCast "processSub, 1" //> (applyOutStk $ new []) `bind` \ _ ->
    dummyCast "processSub, 2" //> (applyInStk $ new input) `bind` \ _ ->
    shuntingyard `bind` \ res ->
    dummyCast "processSub, 3" //> (applyInStk $ restore) `bind` \ _ ->
    dummyCast "processSub, 4" //> (applyOutStk $ restore) `bind` \ _ ->
    dummyCast "processSub, 5" //> (applyOutStk $ push (withAddr addr res))
      
-- Assumes stack being non-empty and containing alternating args and ops
collapse
  :: (Dimonad p, StackState OpMap p)
  => Int8 -> Addr -> InfixT p DisambiguationError ()
collapse prior addr
  = opt (applyOutStk $ top 1) `bind` \ opo ->
    case opo of
      Just (Node node@(OpAtom _ nam :@ oaddr))
        -> dummyCast "collapse, 1" //> lift (getPriorities nam) `bind` \ (_ , rpr) ->
           case compare prior rpr of
             LT
               -> listenAddr (dummyCast "collapse, 2" //>
                    (
                    popAndTell `bind` \ arg2 ->
                    popAndTell `bind` \ _ ->
                    popAndTell `bind` \ arg1 ->
                    unless (isNothing (findEscArg arg1)) (const () **> lift pop) `bind` \ _ ->
                    invoke (buildTree node arg1 arg2)
                    )) `bind` \ res ->
                  dummyCast "collapse, 3" //> (applyOutStk $ push (OrdArg res)) `bind` \ _ ->
                  collapse prior addr
             GT
               -> invoke ()
             _
               -> raise (PriorityClash oaddr addr)
      _
        -> invoke ()

