module Rel
  (
  module InfNum,
  module Range,
  minCol,
  indent,
  dedent,
  DblRel,
  pivot,
  eq,
  ge,
  gt,
  rlx
  ) where


import Data.Maybe
import Utils
import InfNum
import Range


minCol
  :: (Integral a)
  => Inf a
minCol
  = Fin 1


-- Assumes weak unimodality of -lo and hi, with given single extremums
data Rel a
  = Rel
      {
      lo :: Inf a -> Inf a,
      loMin :: Inf a,
      hi :: Inf a -> Inf a,
      hiMax :: Inf a
      }

data DblRel a
  = Dbl 
      {
      slf :: Rel a,
      inv :: Rel a
      }

pivot
  :: (Enum a, Ord a)
  => a -> Range a -> a
pivot y ran@(s := t)
  = case rangeCompare y ran of
      LTRange
        -> s
      InRange
        -> y
      _
        -> pred t

applyRel
  :: (Integral a)
  => Rel a -> Range (Inf a) -> Range (Inf a)
applyRel rel ran
  = lo rel (pivot (loMin rel) ran) := hi rel (pivot (hiMax rel) ran)

indent
  :: (Integral a)
  => DblRel a -> Range (Inf a) -> Range (Inf a)
indent rel ran
  = applyRel (inv rel) ran

-- Assumes that the inner range is correct w.r.t. the outer
-- i.e. a subrange of the result of indent rel oran
dedent
  :: (Integral a)
  => DblRel a -> Range (Inf a) -> Range (Inf a) -> Range (Inf a)
dedent rel oran iran
  = fromJust $ meet oran (applyRel (slf rel) iran)

mono
  :: (Integral a)
  => (Inf a -> Inf a) -> (Inf a -> Inf a) -> Rel a
mono f g
  = Rel f minCol g inf

-- Assumes finite k
add
  :: (Integral a)
  => Inf a -> Inf a -> DblRel a
add k l
  = Dbl
      {
      slf = Rel (if l == inf then const minCol else max minCol . subtract (l - 1)) (minCol + k) (assert (> minCol) . subtract (k - 1)) inf,
      inv = mono (k +) (l +)
      }

eq, ge, gt
  :: (Integral a)
  => DblRel a

eq
  = add 0 1
ge
  = add 0 inf
gt
  = add 1 inf

-- Assumes finite k
rlx
  :: (Integral a)
  => Inf a -> Inf a -> DblRel a
rlx k l
  = Dbl
      {
      slf = Rel (($!) (const minCol) . assert (< l) . assert (>= k)) k (($!) (const inf) . assert (< l) . assert (>= k)) l,
      inv = mono (const k) (const l)
      }
