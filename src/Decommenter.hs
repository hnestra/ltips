{-# LANGUAGE 
      GeneralizedNewtypeDeriving, 
      FlexibleContexts,
      FlexibleInstances, 
      MultiParamTypeClasses, 
      TypeOperators
    #-}
module Decommenter
  ( DecommenterT(..)
  , fellDecommenterT
  , decommentAll
  ) where


import Data.Maybe
import Control.Diapplicative
import Control.Diapplicative.Trans
import Utils
import Address
import Syntax
import Decommenter.Data


newtype DecommenterT p e a
  = DecommenterT { runDecommenterT :: StateT [Lexeme :@ Addr] p e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, Transformer, State [Lexeme :@ Addr])

fellDecommenterT
  :: DecommenterT p e a -> [Lexeme :@ Addr] -> p e (a , [Lexeme :@ Addr])
fellDecommenterT p input
  = runStateT (runDecommenterT p) input


instance (Dimonad p) => Parser Lexeme Addr (DecommenterT p) where
  
  consume pred
    = get `bind` \ lexs@(~((lex :@ addr) : rest)) ->
      if null lexs
        then raise EOF
        else case pred lex of
               Left e
                 -> raise (UnexpectedToken (e :@ addr))
               _
                 -> put rest `bind` \ _ ->
                    invoke (lex :@ addr)
  

instance (Dimonad p, Parser Lexeme Addr p)
  => Parser Lexeme (Addr , Addr) (StateT Pos p) where
  
  consume prop
    = get `bind` \ transpos ->
      let hyp addr = addrPlus transpos (0 :. 1 :- addrSize addr) in
      let augment (lex :@ addr) = lex :@ (addr , hyp addr) in
      fmap augment //> lift (consume prop) `bind` \ (lex :@ addr) ->
      let transaddr@(_ :- end) = if isNewLine lex then addrPlus transpos (1 :. 0 :- 1 :. 0) else hyp addr in
      put end `bind` \ _ ->
      invoke (lex :@ (addr , transaddr))
  

decommentAll
  :: (Dimonad p, Parser Lexeme Addr p)
  => p DecommentError [Lexeme :@ (Addr , Addr)]
decommentAll
  = fst **> runStateT (decommentContent **~ ifSucceeds isEndCom UnmatchedEndCom) (1 :. 0)

decommentContent
  :: (Parser Lexeme (Addr , Addr) p, State Pos p)
  => p DecommentError [Lexeme :@ (Addr , Addr)]
decommentContent
  = catMaybes **> condStar (consumeIf (no isEndCom)) decommentUnit

decommentUnit
  :: (Parser Lexeme (Addr , Addr) p, State Pos p)
  => p DecommentError (Maybe (Lexeme :@ (Addr , Addr)))
-- Assumes the input being non-empty
decommentUnit
  = either id id //>
      (
      raise (Left $ dummyCast "decommentUnit 1") |||
      consumeIf (no (isCom `orelse` isSep)) &&& 
        Just **> (dummyCast "decommentUnit 2" //> consumeIf true) |||
      consumeIf isSep &&&
        const Nothing **> (dummyCast "decommentUnit 3" //> consumeIf true) |||
      consumeIf isBgnCom &&& 
        const Nothing **> cancel decommentBlock |||
      consumeIf isLineCom &&& 
        const Nothing **> cancel decommentLine |||
      Right //> dummyCast "decommentUnit 4" **> ifSucceeds isEndCom UnmatchedEndCom
      )

decommentBlock
  :: (Parser Lexeme (Addr , Addr) p, State Pos p)
  => p DecommentError [Lexeme :@ (Addr , Addr)]
decommentBlock
  = fusel $
      invoke (\ (_ :@ (addr , _)) conts -> bimap (UnmatchedBgnCom addr . failAddr) (const conts)) ***
      (dummyCast "decommentBlock" //> consumeIf isBgnCom) ***
      decommentContent ***
      mixmap Right (consumeIf isEndCom)

decommentLine
  :: (Parser Lexeme (Addr , Addr) p, State Pos p)
  => p DecommentError [Lexeme :@ (Addr , Addr)]
decommentLine
  = after (dummyCast "decommentLine" //> consumeIf isLineCom) $
      catMaybes **> condStar (consumeIf (no isNewLine)) decommentUnit

