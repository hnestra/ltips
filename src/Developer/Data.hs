module Developer.Data 
  ( module Developer.Data
  ) where


import Data.Char
import Address
import Phase
import Syntax.AST


data DevelopmentError
  = LamPat Addr
  | BlockPat Addr
  | LitPat Addr
  | VarAsCon Addr
  | WildAsCon Addr
  | WildExpr Addr
  | IllegalTyPat Addr
  | AbnormalLamDot EvalMode Addr
  | LocalType Addr
  | LamType Addr
  | LitType Addr
  deriving (Eq)


instance Show DevelopmentError where
  
  show (LamPat addr)
    = formatError Development addr $
      "Lambda dot in pattern"
  show (BlockPat addr)
    = formatError Development addr $
      "Block of local declarations in pattern"
  show (LitPat addr)
    = formatError Development addr $
      "Literal in pattern"
  show (VarAsCon addr)
    = formatError Development addr $
      "Constructor expected but variable found"
  show (WildAsCon addr)
    = formatError Development addr $
      "Constructor expected but wildcard found"
  show (WildExpr addr)
    = formatError Development addr $
      "Expression expected but wildcard found"
  show (IllegalTyPat addr)
    = formatError Development addr $
      "Illegal type parameter"
  show (AbnormalLamDot mode addr)
    = formatError Development addr $
      "Lambda of " ++ map toLower (show mode) ++ " mode in type definition"
  show (LocalType addr)
    = formatError Development addr $
      "Local declaration in type"
  show (LamType addr)
    = formatError Development addr $
      "Lambda expression in type"
  show (LitType addr)
    = formatError Development addr $
      "Literal in type"

