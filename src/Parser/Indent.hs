{-# LANGUAGE 
      FlexibleInstances,
      GeneralizedNewtypeDeriving,
      UndecidableInstances,
      FlexibleContexts,
      MultiParamTypeClasses
    #-}
module Parser.Indent 
  (
    module Rel,
    module Parser.Indent.Classes,
    IndentT(..),
    fellIndentT
  ) where


import Control.Diapplicative
import Control.Diapplicative.Trans
import Utils
import Address
import Rel
import Syntax
import Parser.Indent.Classes


getRange
  :: (State AlignAttr f)
  => f e (Range (Inf Int))
getRange
  = fst **> get

modifyRange
  :: (State AlignAttr f)
  => (Range (Inf Int) -> Range (Inf Int)) -> f e ()
modifyRange f
  = modify (bimap f id)

putRange
  :: (State AlignAttr f)
  => Range (Inf Int) -> f e ()
putRange s
  = modifyRange (const s)

getFlag
  :: (State AlignAttr f)
  => f e AlignFlag
getFlag
  = snd **> get

modifyFlag
  :: (State AlignAttr f)
  => (AlignFlag -> AlignFlag) -> f e ()
modifyFlag f
  = modify (bimap id f)

putFlag
  :: (State AlignAttr f)
  => AlignFlag -> f e ()
putFlag s
  = modifyFlag (const s)

askTokenRel
  :: (Reader (DblRel Int) f)
  => f e (DblRel Int)
askTokenRel
  = ask

localTokenRel
  :: (Reader (DblRel Int) f)
  => (DblRel Int -> DblRel Int) -> f e a -> f e a
localTokenRel
  = local


instance (Dimonad p) => Indenter (ReaderT (DblRel Int) (StateT AlignAttr p)) where
  
  indentAt rel p
    = getRange `bind` \ ran ->
      modifyRange (indent rel) `bind` \ _ ->
      p `bind` \ x ->
      modifyRange (dedent rel ran) `bind` \ _ ->
      invoke x
  {-
  aligned p
    = getFlag `bind` \ flag1 ->
      putFlag Align `bind` \ _ ->
      p `bind` \ x ->
      getFlag `bind` \ flag2 ->
      putFlag (if flag1 == Align && flag2 == Align then Align else NoAlign) `bind` \ _ ->
      invoke x
  -}
  -- Equivalent to the commented definition
  aligned p
    = getFlag `bind` \ flag ->
      if flag == Align
        then p
        else putFlag Align `bind` \ _ ->
             p `bind` \ x ->
             putFlag NoAlign `bind` \ _ ->
             invoke x
  
  tokensAt rel
    = localTokenRel (const rel)
  

newtype IndentT p e a
  = IndentT { runIndentT :: ReaderT (DblRel Int) (StateT AlignAttr p) e a }
  deriving (Functor, Bifunctor, Triable, LazyTriable, Catenative, Mixable, Dimonad, Reader (DblRel Int), State AlignAttr, Writer w, Indenter)

fellIndentT
  :: IndentT p e a -> DblRel Int -> AlignAttr -> p e (a , AlignAttr)
fellIndentT p r s
  = runStateT (runReaderT (runIndentT p) r) s


instance Transformer IndentT where
  
  lift = IndentT . lift . lift


currentTokenRel
  :: (Catenative p, Reader (DblRel Int) p, State AlignAttr p)
  => p e (DblRel Int)
currentTokenRel
  = invoke (\ flag rel -> if flag == Align then eq else rel) ***
    getFlag ***
    askTokenRel

saveIndent
  :: (Catenative p, Mixable p, State AlignAttr p)
  => Int -> p e ()
saveIndent col
  = invoke (\ _ _ -> ()) ***
    (dummyCast "saveIndent" //> mixmap (wrap (Fin col `isIn`) . either id id) getRange) ***
    putRange (only (Fin col))


instance (Dimonad p, Parser Lexeme (Addr , Addr) p)
  => Parser Lexeme Addr (IndentT p) where
  
  consume pred
    = currentTokenRel `bind` \ rel ->
      putFlag NoAlign `bind` \ _ ->
      indentAt rel $
        fmap (\ (lex :@ (addr , _)) -> lex :@ addr) //> lift (consume pred) `bind` \ (lex :@ (addr , _ :. col :- _)) ->
        let Right e = pred lex in
        const (MisalignedToken (e :@ addr)) //> saveIndent col `bind` \ _ ->
        invoke (lex :@ addr)
  
