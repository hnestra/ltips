{-# LANGUAGE 
      FlexibleContexts 
    #-}
module Parser.Indent.Classes
  ( module Parser.Indent.Classes
  ) where


import Control.Diapplicative.Trans
import Rel


data AlignFlag
  = NoAlign
  | Align
  deriving (Eq)

type AlignAttr
  = (Range (Inf Int) , AlignFlag)


class (Reader (DblRel Int) p, State AlignAttr p) => Indenter p where
  
  indentAt
    :: DblRel Int -> p e a -> p e a
  
  aligned
    :: p e a -> p e a
  
  tokensAt
    :: DblRel Int -> p e a -> p e a
  

