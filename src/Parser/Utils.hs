module Parser.Utils 
  ( module Parser.Utils
  ) where


import Text.Read (readEither)
import Data.Utils
import Syntax


findLegalPrior
  :: String -> Maybe Integer
findLegalPrior str
  = case readEither str of
      Right n
        | minPrior <= n && n < limPrior
          -> Just n
      _
        -> Nothing

isInfixSeqStop
  :: Lexeme -> Bool
isInfixSeqStop
  = isOp `orelse` isEq `orelse` isCloseParen `orelse` isCloseBrace

