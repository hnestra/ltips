module Parser.Data 
  ( module Parser.Data
  ) where


import Address
import Phase
import Syntax.Data


data ParseError
  = MisalignedDeclaration Addr
  | EndOfSynUnit Addr
  | MissingSynUnit Addr
  | NotInt Addr
  | NotOp Addr
  | NotEq Addr
  | ExtraToken Addr
  | UnexpectedKey Addr
  | ExtraEq Addr Addr
  | UnmatchedOpenDelim Addr Addr
  | UnmatchedCloseDelim Addr
  | MeaninglessDelim Addr
  | PrefixLamDot Addr
  | TyConExpected Addr
  deriving (Eq)


instance Show ParseError where
  
  show (MisalignedDeclaration addr)
    = formatError Parsing addr $
      "Misaligned declaration"
  show (UnexpectedKey addr)
    = formatError Parsing addr $
      "Unexpected keyword occurrence"
  show (ExtraEq oaddr addr)
    = formatError Parsing addr $
      "Repetition of equality sign occurring in this declaration at " ++ show oaddr
  show (NotInt addr)
    = formatError Parsing addr $
      "Expecting an integer not less than " ++ show minPrior ++ " and less than " ++ show limPrior
  show (NotOp addr)
    = formatError Parsing addr $
      "Expecting an infix operator that is not a lambda dot or equality sign"
  show (NotEq addr)
    = formatError Parsing addr $
      "Expecting an equality sign"
  show (ExtraToken addr)
    = formatError Parsing addr $
      "Extra token in infix declaration"
  show (EndOfSynUnit addr)
    = formatError Parsing addr $
      "Previous syntactic unit ended unexpectedly"
  show (MissingSynUnit addr)
    = formatError Parsing addr $
      "Missing syntactic unit"
  show (UnmatchedOpenDelim oaddr addr)
    = formatError Parsing addr $
      "Opening delimiter at " ++ show oaddr ++ " unmatched within its syntactic unit"
  show (UnmatchedCloseDelim addr)
    = formatError Parsing addr $
      "Unmatched closing delimiter"
  show (MeaninglessDelim addr)
    = formatError Parsing addr $
      "Meaningless delimiter"
  show (PrefixLamDot addr)
    = formatError Parsing addr $
      "Lambda dot in prefix form"
  show (TyConExpected addr)
    = formatError Parsing addr $
      "Type constructor expected"
  

