{-# LANGUAGE
      FlexibleInstances,
      FlexibleContexts,
      TupleSections,
      FunctionalDependencies,
      MultiParamTypeClasses
    #-}
module TypeInferencer.Classes
  ( module TypeInferencer.Classes
  ) where


import Control.Diapplicative
import Data.List as List
import Data.Set as Set
import Data.Map as Map
import Syntax.AST
import Unparse
import TypeInferencer.TyAST
import TypeInferencer.Data


unparseTyEnv
  :: (Show nam, Unparse Type p)
  => TyEnv nam -> p e String
unparseTyEnv env
  = (\ list -> "fromList [" ++ intercalate "," list ++ "]") **> 
    mapC (bimap id (\ (x , y) -> "(" ++ x ++ "," ++ y ++ ")") . strength . bimap show unparse) (Map.toList env)


class (Unparse Type p) => TypeInferencer nam p | p -> nam where
  
  nextUId
    :: p e TyVarId
  
  lookupNam
    :: nam -> p e Type
  
  withGlobal
    :: TyEnv nam -> p e a -> p e a
  
  getLevel
    :: p e Level
  

genModeTy
  :: TypeInferencer nam p
  => EvalMode -> p e Type
genModeTy mode
  = (if mode == Backtrack then exnTy else id) . varTy **> nextUId

freshInst
  :: (TypeInferencer nam p)
  => Type -> p e (Map TyVarId TyVarId)
freshInst ty
  = Map.fromAscList **>
    mapC (strength . (, nextUId)) (Set.toAscList (fv ty))

instantiate
  :: (TypeInferencer nam p)
  => Type -> p e Type
instantiate ty
  = freshInst ty `bind` \ instmap ->
    invoke (applySubst (renaming instmap) ty)


class MultiArc n a | a -> n where
  
  ends
    :: a -> ([n] , [n])
  

