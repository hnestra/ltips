module TypeInferencer.Unify 
  ( module TypeInferencer.Unify
  ) where


import Control.Diapplicative
import TypeInferencer.TyAST
import TypeInferencer.Data


unify
  :: (Ord var, Dimonad p)
  => TyAST TyConId var -> TyAST TyConId var -> p (UnificationError var) (Subst TyConId var)
unify (AtomTy (VarTy (TyVar x))) (AtomTy (VarTy (TyVar y)))
  | x == y
    = invoke mempty
unify (AtomTy (VarTy (TyVar x))) u
  | occurs x u
    = raise (Occurs x u)
  | otherwise
    = invoke (only (x , u))
unify t                          u@(AtomTy (VarTy (TyVar _)))
  = unify u t
unify (AtomTy (ConTy (TyCon c))) (AtomTy (ConTy (TyCon d)))
  | c == d
    = invoke mempty
unify (AppTy t1 t2)              (AppTy u1 u2)
  = unify t1 u1 `bind` \ s1 ->
    unify (applySubst s1 t2) (applySubst s1 u2) `bind` \ s2 ->
    invoke (s1 `mappend` s2)
unify t                  u
  = raise (StructureMismatch t u)
{-
unifySeq
  :: (Ord var, Dimonad p)
  => [TyAST TyConId var] -> [TyAST TyConId var] -> p (UnificationError var) (Subst TyConId var)
unifySeq ts us
  = case zipMatching ts us of
      (ps , rest)
        | null rest
          -> foldlM (\ sigma (t , u) -> (sigma `mappend`) **> unify (applySubst sigma t) (applySubst sigma u)) mempty ps
        | otherwise
          -> raise (NumberMismatch rest)
  
unifyMap
  :: (Ord key, Ord var, Dimonad p)
  => Map key (TyAST TyConId var) -> Map key (TyAST TyConId var) -> p (UnificationError var) (Subst TyConId var)
unifyMap env1 env2
  = unifySeq (fmap snd (toAscList env1)) (fmap snd (toAscList env2))
-}
