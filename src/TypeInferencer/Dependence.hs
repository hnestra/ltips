{-# LANGUAGE
      FlexibleContexts,
      MultiParamTypeClasses,
      FlexibleInstances,
      TypeOperators,
      TupleSections
    #-}
module TypeInferencer.Dependence
  ( topsort
  ) where


import Data.Bifunctor
import Data.Maybe
import Data.Utils
import Data.Graph
import Data.SetUtils as Set
import Data.MapUtils as Map hiding (mapMaybe)
import Address
import Syntax.AST
import TypeInferencer.Classes


defNoMap
  :: (Ord nam)
  => [(Int , [nam])] -> Map nam Int
defNoMap ps
  = Map.fromList $ 
    fmap commute $
    ps >>= \ (i , nams) -> fmap (i ,) nams

dependenceGraphOf
  :: (Ord nam)
  => [(Int , ([nam] , [nam]))] -> [(Int , [Int])]
dependenceGraphOf ts
  = let bvmap = defNoMap (fmap (bimap id fst) ts) in
    let ifvs = fmap (bimap id snd) ts in
    fmap (bimap id (mapMaybe (\ fv -> Map.lookup fv bvmap))) ifvs

topsort
  :: (Ord nam, MultiArc nam a)
  => [a] -> [[a]]
topsort eqs
  = fmap flattenSCC $
    stronglyConnComp $
    fmap (\ (x , (y , z)) -> (x , y , z)) $
    zip eqs $
    dependenceGraphOf $
    zip [1 ..] $
    fmap ends eqs


instance (Ord nam) => MultiArc nam (EqAST (PatAST nam :@ Addr) (ExprAST nam :@ Addr) :@ Addr) where
  
  ends (Eq lhs rhs :@ _)
    = (Set.toList (bvars lhs) , Set.toList (fvars rhs))
  

instance (Ord nam) => MultiArc nam (TyEqAST (ConAST nam :@ Addr) (TyRHSAST nam :@ Addr) :@ Addr) where
  
  ends (TyEq (Con nam :@ _) rhs :@ _)
    = ([nam] , Set.toList (cons rhs))
  
{-
nodifyComp
  :: [EqAST (PatAST String :@ Addr) (ExprAST String :@ Addr)] ->
     EqAST (PatAST String :@ Addr) (ExprAST String :@ Addr)
nodifyComp eqs
  = let (lhss , rhss) = separate eqs in
    Eq (tuplifyPat lhss) (tuplifyExpr rhss)
-}
