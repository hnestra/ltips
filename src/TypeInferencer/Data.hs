{-# LANGUAGE
      FlexibleContexts,
      FlexibleInstances,
      MultiParamTypeClasses
    #-}
module TypeInferencer.Data (
    module Syntax.Data,
    Level(..),
    UnificationError(..),
    TypeInferError(..)
  ) where


import Control.Diapplicative
import Control.Diapplicative.Trans
import Utils
import Address
import Phase
import Syntax.Token
import Syntax.AST
import Syntax.Data
import Unparse
import TypeInferencer.TyAST


instance (Show var, Dimonad p) => Unparse (TyAST TyConId var) (UnparseT (TyAST TyConId var) p) where
  
  askOpMaps
    = ask
  
  destruct (AtomTy atom)
    = case atom of
        VarTy (TyVar v)
          -> invoke (Primitive ('v' : show v))
        ConTy (TyCon c)
          -> invoke (Primitive c)
  destruct (AppTy fun arg)
    = case fun of
        AppTy (AtomTy (ConTy (TyCon c))) t
          | not (isPrefix c)
            -> invoke (Compound c) ***
               destruct t ***
               destruct arg
        _ -> invoke (Compound "") ***
             destruct fun ***
             destruct arg
  

{-
format
  :: TyConId -> (Integer , Integer) -> Integer -> Integer -> ShowS -> ShowS -> ShowS
format con (lpr , rpr) tpr apr x y
  = (if tpr < lpr then inParen else id) x .
    (' ' :) . (con ++) . (if null con then id else (' ' :)) .
    (if apr < rpr then inParen else id) y

lookupOp
  :: (Dimonad p, Reader [OpMap] p) => TyConId -> p e (Integer , Integer)
lookupOp op
  = ask `bind` \ opMap ->
    dummyCast ("lookupOp " ++ op) //> bimap toInteger toInteger **> lookupsM op opMap

openPrior
  :: (Dimonad p, Reader [OpMap] p)
  => TyAST TyConId var -> p e (Integer , Integer)
openPrior (AtomTy _)
  = invoke (both id id limPrior)
openPrior (AppTy (AppTy (AtomTy (ConTy (TyCon con))) t1) t2)
  | not (isPrefix con)
    = lookupOp con
openPrior _
  = lookupOp ""

unparsesTy
  :: (Dimonad p, Reader [OpMap] p, Show var)
  => TyAST TyConId var -> p e ShowS
unparsesTy (AtomTy atom)
  = unparsesAtomTy atom
unparsesTy (AppTy fun arg)
  = case fun of
      AppTy (AtomTy (ConTy (TyCon con))) t
        | not (isPrefix con)
          -> invoke (format con) ***
             lookupOp con ***
             (snd **> openPrior t) ***
             (fst **> openPrior arg) ***
             unparsesTy t ***
             unparsesTy arg
      _
        -> invoke (format "") ***
           lookupOp "" ***
           (snd **> openPrior fun) ***
           (fst **> openPrior arg) ***
           unparsesTy fun ***
           unparsesTy arg

unparsesAtomTy
  :: (Dipointed p, Show var)
  => AtomTyAST TyConId var -> p e ShowS
unparsesAtomTy (ConTy (TyCon c)) 
  = invoke (if isPrefix c then (c ++) else inParen (c ++))
unparsesAtomTy (VarTy (TyVar v))
  = invoke (('v' :) . shows v)
-}

data Level
  = TypeLev
  | KindLev
  deriving (Eq)


instance Show Level where
  
  show TypeLev
    = "type"
  show KindLev
    = "kind"
  

data UnificationError var
  = Occurs var (TyAST TyConId var)
  | StructureMismatch (TyAST TyConId var) (TyAST TyConId var)
  | NumberMismatch [TyAST TyConId var]
  deriving (Eq)

{-
instance (Show var) => Show (UnificationError var) where
  
  show (Occurs var ty)
    = "Unification of " ++ show var ++ " with " ++ ty ++ " would give an infinite type"
  show (StructureMismatch ty1 ty2)
    = "Unification of " ++ ty1 ++ " with " ++ ty2 ++ " fails due to structure mismatch"
  show (NumberMismatch tys)
    = "Numbers of terms mismatch revealed by remaining types " ++
      concat (intersperse ", " tys)
-}  

data TypeInferError
  = ArityMismatch Int Int Addr
  | IncorrectArg Int String String Addr
  | IncorrectDef Level String String Addr
  | IncorrectApply Level String String Addr
  | NonFunApply Level String Addr
  | FunInArg Level String String Addr
  | IncorrectEvalMode EvalMode Addr
  | IncorrectLambdaRHS String String Addr
  | TypeArgOfHigherKind String Addr
  deriving (Eq)


instance Show TypeInferError where
  
  show (ArityMismatch actAr expAr addr)
    = formatError TypeInference addr $
      "Constructor is given " ++ showAmount actAr "argument" ++ " but it expects " ++ show expAr
  show (IncorrectArg n actTy expTy addr)
    = formatError TypeInference addr $
      "Argument no " ++ show n ++ " of type " ++ actTy ++ " given to constructor that expects type " ++ expTy
  show (IncorrectDef lev patTy exprTy addr)
    = formatError TypeInference addr $
      "The rhs has " ++ show lev ++ " " ++ exprTy ++ " while the lhs assumes " ++ show lev ++ " " ++ patTy
  show (IncorrectApply lev expTy actTy addr)
    = formatError TypeInference addr $
      "Applied expression expects argument of " ++ show lev ++ " " ++ expTy ++ " but the actual argument has " ++ show lev ++ " " ++ actTy
  show (NonFunApply lev ty addr)
    = formatError TypeInference addr $ 
      "Applied expression is of a non-functional " ++ show lev ++ " " ++ ty
  show (FunInArg lev funty argty addr)
    = formatError TypeInference addr $ 
      "The " ++ show lev ++ " " ++ funty ++ " of applied expression is included in the " ++ show lev ++ " " ++ argty ++ " of the argument"
  show (IncorrectLambdaRHS actTy expTy addr)
    = formatError TypeInference addr $
      "Expression of type " ++ actTy ++ " found while type " ++ expTy ++ " was expected"
  show (IncorrectEvalMode mode addr)
    = formatError TypeInference addr $
      show mode ++ " mode of function evaluation not supported"
  show (TypeArgOfHigherKind actTy addr)
    = formatError TypeInference addr $
      "Type argument of higher kind " ++ actTy
  
