module TypeInferencer.TyAST 
  ( module TypeInferencer.TyAST
  ) where


import Prelude hiding (lookup)
import Control.Monad
import Data.Maybe
import Data.Set hiding (foldl, foldr)
import Data.Map hiding (foldl, foldr)
import Syntax.Data


data TyAST con var
  = AtomTy (AtomTyAST con var)
  | AppTy (TyAST con var) (TyAST con var)
  deriving (Eq)
data AtomTyAST con var
  = VarTy (VarTyAST var)
  | ConTy (ConTyAST con)
  deriving (Eq)
data VarTyAST var
  = TyVar var
  deriving (Show, Eq)
data ConTyAST con
  = TyCon con
  deriving (Show, Eq)

type TyVarId
  = Int
type TyConId
  = String
type Type
  = TyAST TyConId TyVarId
type TyEnv nam
  = Map nam Type


instance Functor (TyAST con) where
  
  fmap f (AtomTy atom)
    = AtomTy (fmap f atom)
  fmap f (AppTy fun arg)
    = AppTy (fmap f fun) (fmap f arg)
  

instance Functor (AtomTyAST con) where
  
  fmap _ (ConTy c)
    = ConTy c
  fmap f (VarTy (TyVar v))
    = VarTy (TyVar (f v))
  

instance Applicative (TyAST con) where
  
  pure x
    = AtomTy (VarTy (TyVar x))
  
  (<*>)
    = ap
  

instance Monad (TyAST con) where
  
  return
    = pure
  
  AtomTy (VarTy (TyVar x)) >>= f
    = f x
  AtomTy (ConTy c) >>= _
    = AtomTy (ConTy c)
  AppTy fun arg >>= f
    = AppTy (fun >>= f) (arg >>= f)
  

newtype Subst con var
  = Subst (Map var (TyAST con var))


applySubst
  :: (Ord var)
  => Subst con var -> TyAST con var -> TyAST con var
applySubst (Subst s) t
  = t >>= \ x ->
    fromMaybe (AtomTy (VarTy (TyVar x))) (lookup x s)


instance (Ord var) => Semigroup (Subst con var) where
  
  Subst s1 <> Subst s2
    = Subst $ Data.Map.union (fmap (applySubst (Subst s2)) s1) s2
  

instance (Ord var) => Monoid (Subst con var) where
  
  mempty
    = Subst $ Data.Map.empty
  

only
  :: (var , TyAST con var) -> Subst con var
only (x , t)
  = Subst $ Data.Map.singleton x t

renaming
  :: Map var var -> Subst con var
renaming
  = Subst . fmap (AtomTy . VarTy . TyVar)

occurs
  :: (Eq var)
  => var -> TyAST con var -> Bool
occurs x (AtomTy (VarTy (TyVar y)))
  = x == y
occurs _ (AtomTy _                )
  = False
occurs x (AppTy t1 t2)
  = occurs x t1 || occurs x t2

fv
  :: (Ord var)
  => TyAST con var -> Set var
fv ty
  = let
      fvlist (AtomTy (ConTy _        ))
        = id
      fvlist (AtomTy (VarTy (TyVar v)))
        = (v :)
      fvlist (AppTy ty1 ty2)
        = fvlist ty1 . fvlist ty2
    in
    Data.Set.fromList (fvlist ty [])


manyAppTy
  :: con -> [TyAST con var] -> TyAST con var
manyAppTy con
  = foldl AppTy (AtomTy (ConTy (TyCon con)))

varTy
  :: var -> TyAST con var
varTy nam
  = AtomTy (VarTy (TyVar nam))

conTy
  :: con -> TyAST con var
conTy nam
  = AtomTy (ConTy (TyCon nam))

funTy
  :: TyAST TyConId var -> TyAST TyConId var -> TyAST TyConId var
funTy t1 t2
  = manyAppTy arrNam [t1, t2]

exnTy
  :: TyAST TyConId var -> TyAST TyConId var
exnTy t
  = manyAppTy exnNam [t]

intTy
  :: TyAST TyConId var
intTy
  = manyAppTy intNam []

floatTy
  :: TyAST TyConId var
floatTy
  = manyAppTy floatNam []

charTy
  :: TyAST TyConId var
charTy
  = manyAppTy charNam []

stringTy
  :: TyAST TyConId var
stringTy
  = manyAppTy stringNam []

typeKind
  :: TyAST TyConId var
typeKind
  = manyAppTy starNam []

curriedTy
  :: (Foldable t)
  => t Type -> Type
curriedTy ts
  = foldr1 funTy ts


asMultiParam
  :: TyAST TyConId var -> ([TyAST TyConId var] , TyAST TyConId var)
asMultiParam (AppTy (AppTy (AtomTy (ConTy (TyCon nam))) argTy) resTy)
  | nam == arrNam
    = case asMultiParam resTy of
        (argTys , resTy)
          -> (argTy : argTys , resTy)
asMultiParam nonFunTy
  = ([] , nonFunTy)

arity
  :: TyAST TyConId var -> Int
arity
  = length . fst . asMultiParam

argTyOf
  :: TyAST TyConId var -> Maybe (TyAST TyConId var)
argTyOf (AppTy (AppTy (AtomTy (ConTy (TyCon nam))) argTy) _)
  | nam == arrNam
    = Just argTy
argTyOf _
  = Nothing
