{-# LANGUAGE
      TupleSections
    #-}
module Control.Diapplicative 
  (
    module Data.Bifunctor,
    module Data.Utils,
    module Control.Diapplicative.Classes,
    module Control.Diapplicative.Symmetric,
    module Control.Diapplicative
  ) where


import Data.Bifunctor
import Data.Utils
import Utils
import Control.Diapplicative.Classes
import Control.Diapplicative.Symmetric


sep
  :: (Either e a -> Either e' a') -> (Either (Either e a) a -> Either (Either e' a') a')
sep g
  = either (Left . g) (bimap Left id . g . Right)

strength
  :: (Bifunctor f)
  => (a , f e a') -> f e (a , a')
strength (v , p)
  = (v ,) **> p

opt
  :: (Triable f)
  => f e a -> f e' (Maybe a)
opt p
  = raise (dummyCast "opt") ///
    Just **> p /// 
    invoke Nothing

star
  :: (Triable f, Catenative f)
  => f e a -> f e [a]
star p
  = raise (dummyCast "star") ///
    plus p ///
    invoke []

plus
  :: (Triable f, Catenative f)
  => f e a -> f e [a]
plus p
  = invoke (:) *** p *** star p

alternate
  :: (Triable f, Catenative f)
  => f e [a] -> f e a -> f e [a]
alternate p q
  = invoke (flip (foldr (\ (xs , y) as -> xs ++ y : as))) ***
    star (invoke (,) *** p *** q) ***
    p

mapC
  :: (Catenative f)
  => (a -> f e a') -> [a] -> f e [a']
mapC f
  = foldr (***) (invoke []) . fmap (bimap id (:) . f)

mapT
  :: (Triable f)
  => (e -> f e' a) -> [e] -> f [e'] a
mapT f
  = foldr (///) (raise []) . fmap (bimap (:) id . f)

zipWithC
  :: (Catenative f)
  => (a -> a' -> f e a'') -> [a] -> [a'] -> f e [a'']
zipWithC f xs ys
  = foldr (***) (invoke []) . fmap (bimap id (:)) $ zipWith f xs ys

zipWithT
  :: (Triable f)
  => (e -> e' -> f e'' a) -> [e] -> [e'] -> f [e''] a
zipWithT f ds es
  = foldr (///) (raise []) . fmap (bimap (:) id) $ zipWith f ds es

sequenceC
  :: (Catenative f)
  => [f e a] -> f e [a]
sequenceC
  = mapC id

sequenceT
  :: (Triable f)
  => [f e a] -> f [e] a
sequenceT
  = mapT id

successes
  :: (Dimonad f)
  => [f e a] -> f e [a]
successes
  = let
      op p rest
        = invoke (:) *** p *** rest ~// rest
      e = invoke []
    in
    foldr op e

foldrM
  :: (Dimonad f)
  => (a -> b -> f e b) -> b -> [a] -> f e b
foldrM op e (x : xs)
  = foldrM op e xs `bind` op x
foldrM _  e _
  = invoke e

foldlM
  :: (Dimonad f)
  => (b -> a -> f e b) -> b -> [a] -> f e b
foldlM op e (x : xs)
  = op e x `bind` \ acc -> 
    foldlM op acc xs
foldlM _  e _
  = invoke e

ignore
  :: (LazyTriable f, Catenative f)
  => f e a -> f e ()
ignore p
  = invoke (dummyCast "ignore 1") *** (Right //> p) *** raise (Left (dummyCast "ignore 2")) ¦¦¦
    invoke ()

negation
  :: (Mixable f)
  => f e a -> f a e
negation
  = mixmap commute


infix 3 &&&
infix 8 >>>


(&&&)
  :: (LazyTriable f, Catenative f)
  => f e a -> f e' b -> f (Either e e') b
p &&& q
  = invoke (flip const) *** (Left //> ignore p) *** (Right //> q)

branch
  :: (LazyTriable f, Catenative f)
  => f e a -> (f e' b , f e' b) -> f e' b
branch p (q , r)
  = either id id //>
    (
      raise (Left (dummyCast ">>>")) |||
      p &&& q |||
      Right //> r
    )

(>>>)
  :: (LazyTriable f, Catenative f)
  => f e a -> f e' a' -> f e' (Either () a')
p >>> q
  = branch p (Right **> q , invoke (Left ()))


condOpt
  :: (LazyTriable f, Catenative f)
  => f e a -> f e' a' -> f e' (Maybe a')
condOpt p q
  = either (const Nothing) Just **> (p >>> q)

condStar
  :: (LazyTriable f, Catenative f)
  => f e a -> f e' a' -> f e' [a']
condStar p q
  = either (const []) id **> (p >>> condPlus p q)

condPlus
  :: (LazyTriable f, Catenative f)
  => f e a -> f e' a' -> f e' [a']
condPlus p q
  = invoke (:) *** q *** condStar p q

condRaise
  :: (Mixable f)
  => f e a -> (a -> e') -> f e' ()
condRaise p h
  = bimap h (const ()) (negation p)

pbind
  :: (Mixable f)
  => f e a -> (a -> Either e a') -> f e a'
f `pbind` k
  = mixmap (`bind` k) f

pcatch
  :: (Mixable f)
  => f e a -> (e -> Either e' a) -> f e' a
f `pcatch` h
  = mixmap (`catch` h) f


{-
instance (Bifunctor p) => Functor (p e) where
  
  fmap
    = second
  

instance (Catenative p) => Applicative (p e) where
  
  pure
    = invoke
  
  (<*>)
    = (***)
  

instance (Dimonad p) => Monad (p e) where
  
  (>>=)
    = bind
  
-}
