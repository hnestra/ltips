{-# LANGUAGE
      TypeOperators
    #-}
module Control.Diapplicative.Classes
  ( module Control.Diapplicative.Classes
  ) where


import Utils
import Data.Bifunctor
import Data.Utils


type (~>) a b
  = Either (a -> b) b


infixl 7 ***
infixl 6 ///, ¦¦¦
infixl 2 |||
infixl 1 `bind`
infixl 0 `catch`
infixr 0 ~>


class Dipointed f where
  
  raise
    :: e -> f e a
  raise
    = point . Left
  
  invoke
    :: a -> f e a
  invoke
    = point . Right
  
  point
    :: Either e a -> f e a
  point
    = either raise invoke
  

class (Bifunctor f) => Mixable f where
  
  mixmap
    :: (Either e a -> Either e' b) -> f e a -> f e' b
  mixmap f
    = fusel . bimap id f . fuser . bimap (Right . Left) Right
  
  fusel
    :: f e (Either e a) -> f e a
  fusel
    = mixmap (either Left id)
  
  fuser
    :: f (Either e a) a -> f e a
  fuser
    = mixmap (either id Right)
  
  turnr
    :: f (Either e e') a -> f e (Either e' a)
  turnr
    = mixmap assocr
  
  turnl
    :: f e (Either e' a) -> f (Either e e') a
  turnl
    = mixmap assocl
  

class (Bifunctor f, Dipointed f) => Triable f where
  
  (///)
    :: f (e -> e') a -> f e a -> f e' a
  p /// q
    = first2 id p q
  
  first2
    :: (e -> e' -> e'') -> f e a -> f e' a -> f e'' a
  first2 op p q
    = raise op /// p /// q
  

class (Triable f) => LazyTriable f where
  
  (¦¦¦)
    :: f (e ~> e') a -> f e a -> f e' a
  p ¦¦¦ q
    = either id id //> (p ||| Left //> q)
  
  (|||)
    :: f (Either (e -> e') e'') a -> f (Either e e'') a -> f (Either e' e'') a
  p ||| q
    = (bimap (//>) Right //> p) ¦¦¦ q
  

class (Bifunctor f, Dipointed f) => Catenative f where
  
  (***)
    :: f e (a1 -> a2) -> f e a1 -> f e a2
  p *** q
    = second2 id p q
  
  second2
    :: (a -> a' -> a'') -> f e a -> f e a' -> f e a''
  second2 op p q
    = invoke op *** p *** q
  

class (Mixable f, LazyTriable f, Catenative f) => (Dimonad f) where
  
  bind
    :: f e a -> (a -> f e a') -> f e a'
  
  catch
    :: f e a -> (e -> f e' a) -> f e' a
  
  handle
    :: f e a -> (Either e a -> f e' a') -> f e' a'
  
  bind m k
    = handle m (either raise k)
  
  catch m x
    = handle m (either x invoke)
  
  handle m h
    = (bimap (Right . Left) Right m `catch` point) `bind` h
  

first3
  :: (Triable f)
  => (e -> e' -> e'' -> e''') -> f e a -> f e' a -> f e'' a -> f e''' a
first3 op p q r
  = raise op /// p /// q /// r

second3
  :: (Catenative f)
  => (a -> a' -> a'' -> a''') -> f e a -> f e a' -> f e a'' -> f e a'''
second3 op p q r
  = invoke op *** p *** q *** r


infixl 7 **~, ~**
infixl 6 //~, ~//
infixr 5 */>
infixr 1 @@@

(*/>)
  :: (Mixable f)
  => (Either e a -> Either e' b) -> f e a -> f e' b
(*/>)
  = mixmap

(//~)
  :: (Triable f)
  => f e a -> f e' a -> f e a
(//~)
  = first2 const

(~//)
  :: (Triable f)
  => f e a -> f e' a -> f e' a
(~//)
  = first2 (flip const)

(**~)
  :: (Catenative f)
  => f e a -> f e a' -> f e a
(**~)
  = second2 const

(~**)
  :: (Catenative f)
  => f e a -> f e a' -> f e a'
(~**)
  = second2 (flip const)

unless
  :: (Dipointed f)
  => Bool -> f e () -> f e ()
unless b p
  = if b then invoke () else p

after
  :: (Catenative f)
  => f e a -> f e a' -> f e a'
after p q
  = invoke (\ _ -> id) ***
    p ***
    q

(@@@)
  :: (Dimonad f)
  => (a -> f e b) -> (b -> f e c) -> a -> f e c
(f @@@ g) x
  = f x `bind` g

biap
  :: (Dimonad f)
  => f e (a -> b) -> f e a -> f e b
biap f x
  = f `bind` (**> x)

pointMaybe
  :: (Dipointed p)
  => Maybe a -> p e a
pointMaybe
  = maybe (raise (dummyCast "pointMaybe")) invoke


instance Dipointed Either where
  
  raise
    = Left
  
  invoke
    = Right
  

instance Mixable Either where
  
  mixmap
    = id
  

instance Triable Either where
  
  f /// x
    = f `catch` \ h ->
      x `catch` \ z ->
      raise (h z)
  

instance LazyTriable Either where
  
  Right a ¦¦¦ _
    = Right a
  Left x ¦¦¦ z
    = either (//> z) Left x
  
  x ||| y
    = assocl (assocr x /// assocr y)
  

instance Catenative Either where
  
  (***)
    = biap
  

instance Dimonad Either where
  
  m `bind` k
    = either Left k m
  
  m `catch` h
    = either h Right m
  

