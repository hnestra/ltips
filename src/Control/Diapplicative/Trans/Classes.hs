{-# LANGUAGE
      MultiParamTypeClasses,
      FunctionalDependencies
    #-}
module Control.Diapplicative.Trans.Classes 
  ( module Control.Diapplicative.Trans.Classes
  ) where


import Data.Bifunctor
import Data.Utils


class Transformer t where
  
  lift
    :: (Bifunctor f)
    => f e a -> t f e a
  

class Bifunctor f => Reader r f | f -> r where
  
  ask
    :: f e r
  
  local
    :: (r -> r) -> f e a -> f e a
  

class Bifunctor f => Writer w f | f -> w where
  
  tell
    :: w -> f e ()
  
  listen
    :: f e a -> f e (a , w)
  listen
    = replace (both id snd)
  
  pass
    :: f e (a , w -> w) -> f e a
  pass
    = replace (\ ((a , f) , w) -> (a , f w))
  
  replace
    :: ((a , w) -> (a' , w)) -> f e a -> f e a'
  replace g
    = pass . bimap id (bimap id const . g) . listen
  

class Bifunctor f => State s f | f -> s where
  
  get
    :: f e s
  
  put
    :: s -> f e ()
  put s
    = modify (const s)
  
  modify
    :: (s -> s) -> f e ()
  
  cancel
    :: f e a -> f e a
  
