{-# LANGUAGE
      DeriveFunctor
  #-}
module Control.Diapplicative.Symmetric 
  ( module Control.Diapplicative.Symmetric
  ) where


import Data.Bifunctor
import Control.Diapplicative.Classes


newtype Symmetric m e a
  = Symmetric { runSymmetric :: m (Either e a) }
  deriving (Functor)

mapSymmetric
  :: (m (Either e a) -> m (Either e' a')) -> Symmetric m e a -> Symmetric m e' a'
mapSymmetric f
  = Symmetric . f . runSymmetric


instance (Functor m) => Bifunctor (Symmetric m) where
  
  bimap f g
    = mapSymmetric (fmap (bimap f g))
  

instance (Monad m) => Dipointed (Symmetric m) where
  
  point
    = Symmetric . return
  

instance (Functor m) => Mixable (Symmetric m) where
  
  mixmap g
    = mapSymmetric (fmap g)
  

instance (Monad m) => Triable (Symmetric m) where
  
  f /// x
    = f `catch` \ h ->
      x `catch` \ z ->
      raise (h z)
  

instance (Monad m) => LazyTriable (Symmetric m) where
  
  x ||| y
    = turnl (turnr x /// turnr y)
  

instance (Monad m) => Catenative (Symmetric m) where
  
  (***)
    = biap
  

instance (Monad m) => Dimonad (Symmetric m) where
  
  handle m f
    = Symmetric (runSymmetric m >>= runSymmetric . f)
  

normal
  :: (Functor m)
  => m a -> Symmetric m e a
normal
  = Symmetric . fmap Right
