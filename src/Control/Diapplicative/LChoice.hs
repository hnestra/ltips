{-# LANGUAGE
      UndecidableInstances,
      FlexibleInstances,
      FunctionalDependencies,
      MultiParamTypeClasses,
      TypeOperators
   #-}
module Control.Diapplicative.LChoice 
  ( module Control.Diapplicative.LChoice
  ) where


import Control.Diapplicative


data PNil = PNil
data x :*: l = x :*: l

infixr 5 :*:


class LChoice l e t | l e -> t, l t -> e where
  
  lchoiceMap
    :: l -> e -> t
  

instance LChoice (e -> Bool) e (e ~> e) where
  
  lchoiceMap p
    = \ e -> case id of { id -> if p e then Left id else Right (id e) }
  

instance LChoice PNil e e where
  
  lchoiceMap PNil
    = id
  

instance (LChoice l e c) => LChoice ((e -> Bool) :*: l) e (e ~> c) where
  
  lchoiceMap (p :*: ps)
    = \ e -> case lchoiceMap ps of { rec -> if p e then Left rec else Right (rec e) }
  

lchoice
  :: (LazyTriable f)
  => (e -> Bool) -> f e a -> f e a -> f e a
lchoice p x y
  = (lchoiceMap p //> x) ¦¦¦ y

