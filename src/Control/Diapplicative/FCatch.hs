{-# LANGUAGE
      FlexibleInstances,
      FunctionalDependencies,
      MultiParamTypeClasses,
      TypeOperators
    #-}
module Control.Diapplicative.FCatch 
  ( module Control.Diapplicative.FCatch
  ) where


import Control.Diapplicative


infixl 0 `fcatch`


class FCatch d e t | d e -> t, t -> e where
  
  fcatchMap
    :: d -> t
  
  fcatch
    :: (LazyTriable f)
    => f d a -> (d -> f e a) -> f e a
  

instance FCatch () e (e ~> e) where
  
  fcatchMap _
    = Left id
  
  fcatch f h
    = (fcatchMap //> f) ¦¦¦ h ()
  

instance FCatch Bool e (e ~> e ~> e) where
  
  fcatchMap e
    = if e then Left Right else (Right . Left) id
  
  fcatch f h
    = (fcatchMap //> f) ¦¦¦ h True ¦¦¦ h False
  

instance FCatch Ordering e (e ~> e ~> e ~> e) where
  
  fcatchMap e
    = case e of
        GT -> Left (Right . Right)
        EQ -> (Right . Left) Right
        LT -> (Right . Right . Left) id
  
  fcatch f h
    = (fcatchMap //> f) ¦¦¦
      h GT ¦¦¦
      h EQ ¦¦¦
      h LT
  
