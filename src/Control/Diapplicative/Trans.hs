{-# LANGUAGE
      FlexibleInstances,
      MultiParamTypeClasses,
      UndecidableInstances,
      TupleSections,
      DeriveFunctor
    #-}
module Control.Diapplicative.Trans 
  (
    module Control.Diapplicative.Trans.Classes,
    module Control.Diapplicative.Trans
  ) where


import Control.Diapplicative
import Control.Diapplicative.Trans.Classes


newtype ExceptT d f e a
  = ExceptT { runExceptT :: f e (Either d a) }
  deriving (Functor)
newtype ReaderT r f e a 
  = ReaderT { runReaderT :: r -> f e a }
  deriving (Functor)
newtype WriterT w f e a 
  = WriterT { runWriterT :: f e (a , w) }
  deriving (Functor)
newtype StateT s f e a 
  = StateT { runStateT :: s -> f e (a , s) }
  deriving (Functor)


mapExceptT
  :: (f e (Either d a) -> f' e' (Either d' a')) -> ExceptT d f e a -> ExceptT d' f' e' a'
mapExceptT f
  = ExceptT . f . runExceptT

mapReaderT
  :: ((r -> f e a) -> r' -> f' e' a') -> ReaderT r f e a -> ReaderT r' f' e' a'
mapReaderT f
  = ReaderT . f . runReaderT

mapWriterT
  :: (f e (a , w) -> f' e' (a' , w')) -> WriterT w f e a -> WriterT w' f' e' a'
mapWriterT f
  = WriterT . f . runWriterT

mapStateT
  :: ((s -> f e (a , s)) -> s' -> f' e' (a' , s')) -> StateT s f e a -> StateT s' f' e' a'
mapStateT f
  = StateT . f . runStateT


instance (Bifunctor f) => Bifunctor (ExceptT d f) where
  
  bimap f g
    = mapExceptT (bimap f (bimap id g))
  

instance (Bifunctor f) => Bifunctor (ReaderT r f) where
  
  bimap f g
    = mapReaderT (bimap f g .)
  

instance (Bifunctor f) => Bifunctor (WriterT w f) where
  
  bimap f g
    = mapWriterT (bimap f (bimap g id))
  

instance (Bifunctor f) => Bifunctor (StateT s f) where
  
  bimap f g
    = mapStateT (bimap f (bimap g id) .)
  

instance (Bifunctor f, Dipointed f, Transformer t) => Dipointed (t f) where
  
  point
    = lift . point
  

instance (Mixable f) => Mixable (ExceptT d f) where
  
  mixmap f
    = mapExceptT $
      mixmap $
      either (bimap id Right . f . Left) (either (Right . Left) (bimap id Right . f . Right))
  

instance (Mixable f) => Mixable (ReaderT r f) where
  
  mixmap f
    = mapReaderT (mixmap f .)
  

instance (Mixable f, Monoid w) => Mixable (WriterT w f) where
  
  mixmap f
    = mapWriterT $
      mixmap $
      either (bimap id (, mempty) . f . Left) (\ (a , w) -> (, w) **> f (Right a))
  

instance (Mixable f) => Mixable (StateT s f) where
  
  mixmap f
    = mapStateT $ \ g s ->
      mixmap (either (bimap id (, s) . f . Left) (\ (a , s') -> (, s') **> f (Right a))) $
      g s
      

instance (Triable f) => Triable (ExceptT d f) where
  
  a /// b
    = ExceptT $
      runExceptT a /// runExceptT b
  

instance (Triable f) => Triable (ReaderT r f) where
  
  a /// b
    = ReaderT $ \ r ->
      runReaderT a r /// runReaderT b r
  

instance (Triable f, Monoid w) => Triable (WriterT w f) where
  
  a /// b
    = WriterT $
      runWriterT a /// runWriterT b
  

instance (Triable f) => Triable (StateT s f) where

  a /// b
    = StateT $ \ s ->
      runStateT a s /// runStateT b s
  

instance (LazyTriable f) => LazyTriable (ExceptT d f) where
  
  a ¦¦¦ b
    = ExceptT $
      runExceptT a ¦¦¦ runExceptT b
  
  a ||| b
    = ExceptT $
      runExceptT a ||| runExceptT b
  

instance (LazyTriable f) => LazyTriable (ReaderT r f) where
  
  a ¦¦¦ b
    = ReaderT $ \ r ->
      runReaderT a r ¦¦¦ runReaderT b r
  
  a ||| b
    = ReaderT $ \ r ->
      runReaderT a r ||| runReaderT b r
  

instance (LazyTriable f, Monoid w) => LazyTriable (WriterT w f) where
  
  a ¦¦¦ b
    = WriterT $
      runWriterT a ¦¦¦ runWriterT b
  
  a ||| b
    = WriterT $
      runWriterT a ||| runWriterT b
  

instance (LazyTriable f) => LazyTriable (StateT s f) where
  
  a ¦¦¦ b
    = StateT $ \ s ->
      runStateT a s ¦¦¦ runStateT b s
  
  a ||| b
    = StateT $ \ s ->
      runStateT a s ||| runStateT b s
  

instance (Dimonad f) => Catenative (ExceptT d f) where
  
  (***)
    = biap
  

instance (Catenative f) => Catenative (ReaderT r f) where
  
  f *** x
    = ReaderT $ \ r ->
      runReaderT f r *** runReaderT x r
  

instance (Catenative f, Monoid w) => Catenative (WriterT w f) where
  
  f *** x
    = WriterT $ 
      invoke (\ (f , w1) (x , w2) -> (f x , w1 `mappend` w2)) *** 
      runWriterT f *** 
      runWriterT x
  

instance (Dimonad f) => Catenative (StateT s f) where
  
  (***)
    = biap
  

instance (Dimonad f) => Dimonad (ExceptT d f) where
  
  x `bind` k
    = ExceptT (runExceptT x `bind` either (invoke . Left) (runExceptT . k))
  
  x `catch` h
    = ExceptT (runExceptT x `catch` runExceptT . h)
  

instance (Dimonad f) => Dimonad (ReaderT r f) where
  
  x `bind` k
    = ReaderT $ \ r ->
      runReaderT x r `bind` \ a ->
      runReaderT (k a) r
  
  x `catch` h
    = ReaderT $ \ r ->
      runReaderT x r `catch` \ e ->
      runReaderT (h e) r
  

instance (Dimonad f, Monoid w) => Dimonad (WriterT w f) where
  
  x `bind` k
    = WriterT $
      runWriterT x `bind` \ (a , w) ->
      second (w `mappend`) **> runWriterT (k a)
  
  x `catch` h
    = WriterT (runWriterT x `catch` runWriterT . h)
  

instance (Dimonad f) => Dimonad (StateT s f) where
  
  x `bind` k
    = StateT $ \ s ->
      runStateT x s `bind` \ (a , s') ->
      runStateT (k a) s'
  
  x `catch` h
    = StateT $ \ s ->
      runStateT x s `catch` \ e ->
      runStateT (h e) s
  

instance Transformer (ExceptT d) where
  
  lift x
    = ExceptT $ 
      Right **> x
  

instance Transformer (ReaderT r) where
  
  lift x
    = ReaderT $ \ _ ->
      x
  

instance (Monoid w) => Transformer (WriterT w) where
  
  lift x
    = WriterT $
      (, mempty) **> x
  

instance Transformer (StateT s) where
  
  lift x
    = StateT $ \ s ->
      (, s) **> x
  

instance (Reader r f) => Reader r (ExceptT d f) where
  
  ask
    = lift ask

  local f
    = mapExceptT $
      local f


instance (Bifunctor f, Dipointed f) => Reader r (ReaderT r f) where
  
  ask
    = ReaderT invoke
  
  local f
    = mapReaderT (. f)
  

instance (Reader r f, Monoid w) => Reader r (WriterT w f) where
  
  ask
    = lift ask
  
  local f
    = mapWriterT (local f)
  

instance (Reader r f) => Reader r (StateT s f) where
  
  ask
    = lift ask
  
  local f
    = mapStateT (local f .)
    

instance (Writer w f) => Writer w (ExceptT d f) where

  tell
    = lift . tell
  
  replace g
    = mapExceptT $
      replace (undistrl . bimap id g . distrl)
  

instance (Writer w f) => Writer w (ReaderT r f) where
  
  tell
    = lift . tell
  
  replace g
    = mapReaderT (replace g .)
  

instance (Bifunctor f, Dipointed f) => Writer w (WriterT w f) where
  
  tell
    = WriterT . invoke . both (const ()) id
  
  replace g
    = mapWriterT (bimap id g)
  

instance (Writer w f) => Writer w (StateT s f) where
  
  tell
    = lift . tell
  
  replace g
    = mapStateT (replace (swasr . bimap g id . swasr) .)
  
  

instance (State s f) => State s (ExceptT d f) where
  
  get
    = lift get
  
  modify
    = lift . modify
  
  cancel
    = mapExceptT cancel
  

instance (State s f) =>  State s (ReaderT r f) where
  
  get
    = lift get
  
  modify
    = lift . modify
  
  cancel
    = mapReaderT (cancel .)
  
  
instance (State s f, Monoid w) => State s (WriterT w f) where
  
  get
    = lift get
  
  modify
    = lift . modify
  
  cancel
    = mapWriterT cancel
  

instance (Bifunctor f, Dipointed f) => State s (StateT s f) where
  
  get
    = StateT (invoke . both id id)
  
  modify f
    = StateT (invoke . both (const ()) f)
  
  cancel
    = mapStateT (\ f s -> bimap id (const s) **> f s)
  
