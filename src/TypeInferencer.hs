{-# LANGUAGE
      TypeOperators,
      FlexibleContexts,
      GeneralizedNewtypeDeriving,
      UndecidableInstances,
      MultiParamTypeClasses,
      FlexibleInstances
    #-}
module TypeInferencer
  (
  module TypeInferencer.TyAST,
  module TypeInferencer.Data,
  module TypeInferencer.Classes,
  module TypeInferencer.Unify,
  module TypeInferencer.Dependence,
  module TypeInferencer
  ) where


import Control.Diapplicative
import Control.Diapplicative.Trans
import Utils
import Data.MapUtils as Map hiding (foldr, foldl)
import Pool
import Stack
import Syntax
import Unparse
import TypeInferencer.TyAST
import TypeInferencer.Data
import TypeInferencer.Classes
import TypeInferencer.Unify
import TypeInferencer.Dependence


newtype TypeInferencerT nam p e a
  = TypeInferencerT { runTypeInferencerT :: PoolT TyVarId (ReaderT (TyEnv nam , TyEnv nam) (UnparseT Type p)) e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, Reader (TyEnv nam , TyEnv nam), Pool TyVarId)

fellTypeInferencerT
  :: TypeInferencerT nam p e a -> TyVarId -> (TyEnv nam , TyEnv nam) -> [OpMap] -> p e (a , TyVarId)
fellTypeInferencerT p var (tenv , kenv) oenv
  = fellUnparseT (runReaderT (fellPoolT (runTypeInferencerT p) var) (tenv , kenv)) oenv


instance Transformer (TypeInferencerT nam) where
  
  lift
    = TypeInferencerT . lift . lift . lift
  

askEnv
  :: (Bifunctor p, Dipointed p)
  => TypeInferencerT nam p e (TyEnv nam)
askEnv
  = fst **> ask

askTyEnv
  :: (Bifunctor p, Dipointed p)
  => TypeInferencerT nam p e (TyEnv nam)
askTyEnv
  = snd **> ask

getLocalEnv
  :: (TypeInferencer nam p)
  => StackT (TyEnv nam) p e [TyEnv nam]
getLocalEnv
  = dummyCast "getLocalEnv" //> getAll


newtype KindInferencerT nam p e a
  = KindInferencerT { runKindInferencerT :: TypeInferencerT nam p e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, Reader (TyEnv nam , TyEnv nam), Pool TyVarId)


instance Transformer (KindInferencerT nam) where
  
  lift
    = KindInferencerT . lift
  

instance (Dimonad p) => Unparse Type (TypeInferencerT nam p) where
  
  askOpMaps
    = TypeInferencerT . lift . lift $ askOpMaps
  destruct
    = TypeInferencerT . lift . lift . destruct
  

instance (Dimonad p) => Unparse Type (KindInferencerT nam p) where
  
  askOpMaps
    = KindInferencerT askOpMaps
  destruct
    = KindInferencerT . destruct
  

instance (Unparse t p, Monoid w) => Unparse t (WriterT w p) where
  
  askOpMaps
    = lift askOpMaps
  destruct
    = lift . destruct
  

instance (Unparse t p) => Unparse t (StackT s p) where
  
  askOpMaps
    = lift askOpMaps
  destruct
    = lift . destruct
  

instance (Dimonad p, Ord nam) => TypeInferencer nam (TypeInferencerT nam p) where
  
  nextUId
    = TypeInferencerT next
  
  lookupNam nam
    = raise (dummyCast "lookupNam, 1") ///
      (askEnv `bind` lookupM nam `bind` instantiate)
  
  withGlobal env
    = local (bimap (union env) id)
  
  getLevel
    = invoke TypeLev
  

instance (Dimonad p, Ord nam) => TypeInferencer nam (KindInferencerT nam p) where
  
  nextUId
    = KindInferencerT nextUId
  
  lookupNam nam
    = raise (dummyCast "lookupNam, 2") ///
      (KindInferencerT askTyEnv `bind` lookupM nam `bind` instantiate)
  
  withGlobal env
    = local (bimap id (union env))
  
  getLevel
    = invoke KindLev
  

instance (TypeInferencer nam p, Ord nam) => TypeInferencer nam (StackT (TyEnv nam) p) where
  
  nextUId
    = lift nextUId
  
  lookupNam nam
    = raise (dummyCast "lookupNam, 3") ///
      (getLocalEnv `bind` lookupsM nam) ///
      lift (lookupNam nam)
  
  withGlobal env
    = mapStackT (withGlobal env .)
  
  getLevel
    = lift getLevel
  

instance (TypeInferencer nam p, Monoid w) => TypeInferencer nam (WriterT w p) where
  
  nextUId
    = lift nextUId
  
  lookupNam nam
    = lift (lookupNam nam)
  
  withGlobal env
    = mapWriterT (withGlobal env)
  
  getLevel
    = lift getLevel
  

withLocal
  :: (TypeInferencer nam p, StackState (TyEnv nam) p)
  => TyEnv nam -> p e a -> p e a
withLocal env p
  = after (dummyCast "withLocal, 1" //> push env) $
      invoke const ***
      p ***
      (dummyCast "withLocal, 2" //> pop)

storeBindings
  :: (Writer (TyEnv nam) p)
  => TyEnv nam -> p TypeInferError ()
storeBindings
  = tell

storeSubst
  :: (TypeInferencer nam p, Writer (Subst TyConId TyVarId) p, StackState (TyEnv nam) p)
  => Subst TyConId TyVarId -> p TypeInferError ()
storeSubst sigma
  = after (dummyCast "requireSubst" //> modifyAll (fmap (applySubst sigma))) $
    tell sigma

listenSubst
  :: (Writer (Subst TyConId TyVarId) p)
  => p TypeInferError a -> p TypeInferError (a , Subst TyConId TyVarId)
listenSubst
  = listen

checkEqSeq
  :: (TypeInferencer nam p, Writer (Subst TyConId TyVarId) p, StackState (TyEnv nam) p)
  => [((Type , Type) , Addr)] -> p TypeInferError ()
checkEqSeq (((ty1 , ty2) , addr) : rest)
  = getLevel `bind` \ lev ->
    unparse ty1 `bind` \ t1 ->
    unparse ty2 `bind` \ t2 ->
    const (IncorrectDef lev t1 t2 addr) //> unify ty1 ty2 `bind` \ sigma ->
    storeSubst sigma `bind` \ _ ->
    checkEqSeq (fmap (bimap (bimap (applySubst sigma) (applySubst sigma)) id) rest)
checkEqSeq _
  = invoke ()

translate
  :: (TypeInferencer TyConId p)
  => ExprAST TyConId :@ Addr -> p e Type
translate (AtomExpr atom :@ _)
  = case atom of
      VarExpr (Var nam :@ _) :@ _
        -> lookupNam nam
      ConExpr (Con nam :@ _) :@ _
        -> invoke (conTy nam)
      _
        -> dummyCast "translate, 1"
translate (App e1 e2 :@ _)
  = invoke AppTy ***
    translate e1 ***
    translate e2
translate _
  = dummyCast "translate, 2"


convertTyEq
  :: (TypeInferencer TyConId p, StackState (TyEnv TyConId) p, Writer (TyEnv TyConId) p)
  => TyEqAST (ConAST TyConId :@ Addr) (TyRHSAST TyConId :@ Addr) :@ Addr -> p TypeInferError ()
convertTyEq (TyEq (Con nam :@ _) (TyRHS pats temps :@ _) :@ _)
  = convertTyEqHelper (conTy nam) pats temps

convertTyEqHelper
  :: (TypeInferencer TyConId p, StackState (TyEnv TyConId) p, Writer (TyEnv TyConId) p)
  => Type -> [PatAST TyConId :@ Addr] -> [TempAST TyConId :@ Addr] -> p TypeInferError ()
convertTyEqHelper resTy pats temps
  = case pats of
      pat : rest
        -> runWriterT (convertPat pat) `bind` \ (ty , env) ->
           withLocal env (convertTyEqHelper (AppTy resTy ty) rest temps)
      _
        -> const () **> mapC (convertTemp resTy) temps

convertPat
  :: (TypeInferencer nam p, Writer (TyEnv nam) p, Ord nam)
  => PatAST nam :@ Addr -> p TypeInferError Type
convertPat
  = typeInferPat

convertTemp
  :: (TypeInferencer TyConId p, Writer (TyEnv TyConId) p)
  => Type -> TempAST TyConId :@ Addr -> p TypeInferError ()
convertTemp resTy (Temp (Con nam :@ _) exprs :@ _)
  = convertExprs resTy exprs `bind` \ ty ->
    storeBindings (singleton nam ty)

convertExprs
  :: (TypeInferencer TyConId p)
  => Type -> [ExprAST TyConId :@ Addr] -> p e Type
convertExprs resTy exprs
  = foldr funTy resTy **> mapC translate exprs


kindInferGroups
  :: (Ord nam, Dimonad p)
  => [[TyEqAST (ConAST nam :@ Addr) (TyRHSAST nam :@ Addr) :@ Addr]] -> 
     KindInferencerT nam p TypeInferError (Map nam Type)
kindInferGroups
  = let
      op eqs rest
        = fst **> fst **> fellStackT (runWriterT (kindInferTyEqs eqs)) [] `bind` \ env -> 
          union env **> withGlobal env rest
      e = invoke empty
    in
    foldr op e

kindInferTyEqs
  :: (Ord nam, TypeInferencer nam p, Writer (Subst TyConId TyVarId) p, StackState (TyEnv nam) p)
  => [TyEqAST (ConAST nam :@ Addr) (TyRHSAST nam :@ Addr) :@ Addr] -> p TypeInferError (Map nam Type)
kindInferTyEqs eqs
  = let ((cons , rhss) , eaddrs) = bimap separate id (separate eqs) in
    runWriterT (mapC kindInferCon cons) `bind` \ (lhstys , lhsenv) ->
    listenSubst (withLocal lhsenv (mapC kindInferTyRHS rhss)) `bind` \ (rhstys , sigma) ->
    listenSubst (checkEqSeq (zip (zip (fmap (applySubst sigma) lhstys) rhstys) eaddrs)) `bind` \ (_ , tau) ->
    invoke (fmap (applySubst (sigma `mappend` tau)) lhsenv)

kindInferCon
  :: (TypeInferencer nam p, Writer (TyEnv nam) p)
  => ConAST nam :@ Addr -> p TypeInferError Type
kindInferCon (Con nam :@ _)
  = varTy **> nextUId `bind` \ ty ->
    after (storeBindings (singleton nam ty)) $
    invoke ty

kindInferTyRHS
  :: (Ord nam, TypeInferencer nam p, Writer (Subst TyConId TyVarId) p, StackState (TyEnv nam) p)
  => TyRHSAST nam :@ Addr -> p TypeInferError Type
kindInferTyRHS (TyRHS args tempseq :@ addr)
  = case args of
      a : as
        -> runWriterT (typeInferPat a) `bind` \ (argty , argenv) ->
           listenSubst (withLocal argenv (kindInferTyRHS (TyRHS as tempseq :@ addr))) `bind` \ (resty , sigma) ->
           invoke (funTy (applySubst sigma argty) resty)
      _
        -> const typeKind **> mapC kindInferTemp tempseq

kindInferTemp
  :: (Ord nam, TypeInferencer nam p, Writer (Subst TyConId TyVarId) p, StackState (TyEnv nam) p)
  => TempAST nam :@ Addr -> p TypeInferError Type
kindInferTemp (Temp _ args :@ _)
  = let
      op expr@(_ :@ addr) res
        = typeInferExpr expr `bind` \ ty ->
          unparse ty `bind` \ t ->
          const (TypeArgOfHigherKind t addr) //> unify typeKind ty `bind` \ sigma ->
          after (storeSubst sigma) $
          res
      e = invoke typeKind
    in
    foldr op e args


typeInferProg
  :: (Dimonad p)
  => ProgAST TyConId (TyRHSAST TyConId :@ Addr) (PatAST TyConId :@ Addr) (ExprAST TyConId :@ Addr) :@ Addr ->
     TypeInferencerT TyConId p TypeInferError (Map TyConId Type, Map TyConId Type)
typeInferProg (Prog _ teqs eqs :@ _)
  = runKindInferencerT (kindInferGroups (topsort teqs)) `bind` \ kenv ->
    fst **> fellStackT (runWriterT (mapC convertTyEq teqs)) [] `bind` \ (_ , env) ->
    union env **> withGlobal env (typeInferGroups (topsort eqs)) `bind` \ tenv ->
    invoke (tenv , kenv)

typeInferGroups
  :: (Ord nam, Dimonad p)
  => [[EqAST (PatAST nam :@ Addr) (ExprAST nam :@ Addr) :@ Addr]] -> 
     TypeInferencerT nam p TypeInferError (TyEnv nam)
typeInferGroups
  = let
      op eqs rest
        = fst **> fst **> fellStackT (runWriterT (typeInferEqs eqs)) [] `bind` \ env -> 
          union env **> withGlobal env rest
      e = invoke empty
    in
    foldr op e

typeInferEqs
  :: (TypeInferencer nam p, Writer (Subst TyConId TyVarId) p, StackState (TyEnv nam) p, Ord nam)
  => [EqAST (PatAST nam :@ Addr) (ExprAST nam :@ Addr) :@ Addr] -> p TypeInferError (TyEnv nam)
typeInferEqs eqs
  = let ((pats , exprs) , eaddrs) = bimap separate id (separate eqs) in
    runWriterT (typeInferPatSeq pats) `bind` \ (lhstys , lhsenv) ->
    listenSubst (withLocal lhsenv (typeInferExprSeq exprs)) `bind` \ (rhstys , sigma) ->
    listenSubst (checkEqSeq (zip (zip (fmap (applySubst sigma) lhstys) rhstys) eaddrs)) `bind` \ (_ , tau) ->
    invoke (fmap (applySubst (sigma `mappend` tau)) lhsenv)

typeInferPatSeq
  :: (TypeInferencer nam p, Writer (TyEnv nam) p, Ord nam)
  => [PatAST nam :@ Addr] -> p TypeInferError [Type]
typeInferPatSeq seq
  = mapC typeInferPat seq

typeInferPat
  :: (TypeInferencer nam p, Writer (TyEnv nam) p, Ord nam)
  => PatAST nam :@ Addr -> p TypeInferError Type
typeInferPat (VarPat (Var nam :@ _) :@ _)
  = varTy **> nextUId `bind` \ ty ->
    storeBindings (singleton nam ty) `bind` \ _ ->
    invoke ty
typeInferPat (ConPat (Con nam :@ _) patseq :@ oaddr)
  = lookupNam nam `bind` \ conTy ->
    let actArity = length patseq in
    let expArity = arity conTy in
    if actArity == expArity
      then let
             op accTy (n , actArgTy)
               = case accTy of
                   AppTy (AppTy _ expArgTy) expResTy
                     -> unparse actArgTy `bind` \ act ->
                        unparse expArgTy `bind` \ exp ->
                        const (IncorrectArg n act exp oaddr) //> unify expArgTy actArgTy `bind` \ sigma ->
                        tell sigma `bind` \ _ ->
                        invoke (applySubst sigma expResTy)
                   _ -> error "typeInferPat: Panic! Non-binary application!"
             e = conTy
           in
           runWriterT (typeInferPatSeq patseq) `bind` \ (tys , env) ->
           runWriterT (foldlM op e (zip [1 ..] tys)) `bind` \ (ty , tau) ->
           storeBindings (fmap (applySubst tau) env) `bind` \ _ ->
           invoke ty
      else raise (ArityMismatch actArity expArity oaddr)
typeInferPat (WildPat :@ _)
  = varTy **> nextUId

typeInferExprSeq
  :: (TypeInferencer nam p, Writer (Subst TyConId TyVarId) p, StackState (TyEnv nam) p, Ord nam)
  => [ExprAST nam :@ Addr] -> p TypeInferError [Type]
typeInferExprSeq
  = let
      op expr res
        = invoke (\ ty (tys , sigma) -> applySubst sigma ty : tys) ***
          typeInferExpr expr ***
          listenSubst res
      e = invoke []
    in
    foldr op e

typeInferExpr
  :: (TypeInferencer nam p, Writer (Subst TyConId TyVarId) p, StackState (TyEnv nam) p, Ord nam)
  => ExprAST nam :@ Addr -> p TypeInferError Type
typeInferExpr (AtomExpr atom :@ _)
  = typeInferAtomExpr atom
typeInferExpr (App expr1 expr2 :@ addr)
  = getLevel `bind` \ lev ->
    typeInferExpr expr1 `bind` \ ty1 ->
    listenSubst (typeInferExpr expr2) `bind` \ (ty2 , sigma) ->
    varTy **> nextUId `bind` \ ty ->
    let ty1' = applySubst sigma ty1 in
    unparse ty1' `bind` \ t1' ->
    unparse ty2 `bind` \ t2 ->
    let
      diagnose (StructureMismatch _ _)
        = NonFunApply lev t1' addr
      diagnose (Occurs _ _)
        = FunInArg lev t1' t2 addr
      diagnose _
        = error "typeInferExpr: Panic! Unexpected exception!"
      handler e
        = (const (diagnose e) //> pointMaybe (argTyOf ty1')) `bind` \ ty2' ->
          unparse ty2' `bind` \ t2' ->
          raise (IncorrectApply lev t2' t2 addr)
    in
    (unify (funTy ty2 ty) ty1' `catch` handler) `bind` \ tau ->
    storeSubst tau `bind` \ _ ->
    invoke (applySubst tau ty)
typeInferExpr (Lam mode pat expr :@ addr)
  | mode == Postponed
    = raise (IncorrectEvalMode mode addr)
  | otherwise
    = runWriterT (typeInferPat pat) `bind` \ (ty1 , env1) ->
      listenSubst (withLocal env1 (typeInferExpr expr)) `bind` \ (ty2 , sigma) ->
      genModeTy mode `bind` \ ty2' ->
      unparse ty2 `bind` \ t2 ->
      unparse ty2' `bind` \ t2' ->
      const (IncorrectLambdaRHS t2 t2' (locationOf expr)) //> unify ty2 ty2' `bind` \ tau ->
      storeSubst tau `bind` \ _ ->
      invoke (funTy (applySubst (sigma `mappend` tau) ty1) (applySubst tau ty2))
typeInferExpr (Local (Block _ eqs :@ _) expr :@ _)
  = typeInferEqs eqs `bind` \ env ->
    withLocal env (typeInferExpr expr)

typeInferAtomExpr
  :: (TypeInferencer nam p)
  => AtomExprAST nam :@ Addr -> p e Type
typeInferAtomExpr (VarExpr (Var nam :@ _) :@ _)
  = lookupNam nam
typeInferAtomExpr (ConExpr (Con nam :@ _) :@ _)
  = lookupNam nam
typeInferAtomExpr (Lit lit :@ _)
  = typeInferLit lit

typeInferLit
  :: (TypeInferencer nam p)
  => LitAST :@ Addr -> p e Type
typeInferLit (Char _ :@ _)
  = invoke charTy
typeInferLit (String _ :@ _)
  = invoke stringTy
typeInferLit (Num nam :@ _)
  = if isInt nam
      then invoke intTy
      else invoke floatTy

