module Lexer.Data 
  ( module Lexer.Data
  ) where


import qualified Data.MapUtils as Map
import qualified Data.SetUtils as Set
import Address
import Phase
import Syntax.Token


bracketMap
  :: Map.Map String DelimCat
bracketMap
  = Map.fromList $
    [
      ("(" , OpenParen),
      ("[" , OpenBracket),
      ("{" , OpenBrace),
      (")" , CloseParen),
      ("]" , CloseBracket),
      ("}" , CloseBrace)
    ]

keyMap
  :: Map.Map String KeyCat
keyMap  
  = Map.fromList $
    [
      ("infix" , InfixKey),
      ("type" , TypeKey),
      ("=" , EqKey)
    ]

codeSet
  :: Set.Set String
codeSet
  = Set.fromList $
    [
      "NUL",
      "SOH",
      "STX",
      "ETX",
      "EOT",
      "ENQ",
      "ACK",
      "a", "BEL",
      "b", "BS",
      "t", "HT",
      "n", "LF",
      "v", "VT",
      "f", "FF",
      "r", "CR",
      "SO",
      "SI",
      "DLE",
      "DC1",
      "DC2",
      "DC3",
      "DC4",
      "NAK",
      "SYN",
      "ETB",
      "CAN",
      "EM",
      "SUB",
      "ESC",
      "FS",
      "GS",
      "RS",
      "US",
      "SP",
      "\"",
      "'",
      "\\",
      "DEL"
    ]


data Radix
  = Dec
  | Hex
  | Oct
  deriving (Eq)


data LexError
  = SpecChar Char Addr
  | UnknownEscCode Addr
  | NonTermStrLit Addr
  | NonTermCharLit Addr
  | EmptyCharLit Addr
  deriving (Eq)

instance Show LexError where

  show (SpecChar c addr)
    = formatError Lexing addr $
      "Special character " ++ show c ++ " not allowed"
  show (UnknownEscCode addr)
    = formatError Lexing addr $
      "Escape code unknown"
  show (NonTermStrLit addr)
    = formatError Lexing addr $
      "String literal not terminated"
  show (NonTermCharLit addr)
    = formatError Lexing addr $
      "Character literal not terminated"
  show (EmptyCharLit addr)
    = formatError Lexing addr $
      "Character literal empty"
  

