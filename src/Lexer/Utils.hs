module Lexer.Utils 
  (
  module Data.Char,
  module Lexer.Utils
  ) where


import Data.Char hiding (GeneralCategory(..), generalCategory)
import Lexer.Data


isNormalSpace
  :: Char -> Bool
isNormalSpace c
  = c `elem` " \n"

isHalfStop
  :: Char -> Bool
isHalfStop c
  = c `elem` ":;,"

isFullStop
  :: Char -> Bool
isFullStop c
  = c `elem` ".?!"

isArithSym
  :: Char -> Bool
isArithSym c
  = c `elem` "+-*×/%÷^"

isCompSym
  :: Char -> Bool
isCompSym c
  = c `elem` "=<>"

isOtherSym
  :: Char -> Bool
isOtherSym c
  = c `elem` "@&\\|¦~#¤€£$¢©®¬¶§"

isBracketSym
  :: Char -> Bool
isBracketSym c
  = c `elem` "()[]{}"

isQuote
  :: Char -> Bool
isQuote c
  = c `elem` "'`´\""

isOpSym
  :: Char -> Bool
isOpSym c
  = isHalfStop c || isFullStop c || isArithSym c || isCompSym c || isOtherSym c

isNormal
  :: Char -> Bool
isNormal c
  = isNormalSpace c || 
    c == '_' || isDigit c || isAlpha c || isOpSym c || isBracketSym c || isQuote c

isDigitOnBase
  :: Char -> Radix -> Bool
isDigitOnBase c radix
  = case radix of
      Dec
        -> isDigit c
      Hex
        -> isHexDigit c
      Oct
        -> isOctDigit c

