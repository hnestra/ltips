{-# LANGUAGE
      FlexibleContexts,
      TypeOperators
    #-}
module Syntax
  (
    module Syntax.Token,
    module Syntax.AST,
    module Syntax.Data,
    module Syntax.Classes,
    module Syntax
  ) where


import Control.Diapplicative
import Control.Diapplicative.Trans
import Address
import Syntax.Token
import Syntax.AST
import Syntax.Data
import Syntax.Classes


listenAddr
  :: (Writer w f)
  => f e t -> f e (t :@ w)
listenAddr p
  = (\ (r , addr) -> r :@ addr) **> listen p

listenAfterTelling
  :: (Catenative p, Writer w p)
  => w -> p e a -> p e (a :@ w)
listenAfterTelling addr
  = listenAddr . after (tell addr)

ifSucceeds
  :: (Parser t a f, Locatable (t :@ a))
  => (t -> Bool) -> (Addr -> e) -> f e ()
ifSucceeds p r
  = condRaise (consumeIf p) (r . locationOf)

ifSucceedsSeq
  :: (Writer a f, Parser t a f, Locatable ([t :@ a] :@ a))
  => [t -> Bool] -> (Addr -> e) -> f e ()
ifSucceedsSeq ps r
  = condRaise (listenAddr (mapC consumeIf ps)) (r . locationOf)

ifFails
  :: (Parser t a f, Locatable (t :@ a))
  => (t -> Bool) -> (Addr -> e) -> f e ()
ifFails p r
  = condRaise (negation $ consumeIf p) (r . failAddr)

