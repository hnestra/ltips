{-# LANGUAGE
      TypeOperators
    #-}
module ScopeInferencer.Data 
  ( module ScopeInferencer.Data
  ) where


import Data.MapUtils
import Address
import Phase


data NamSpc
  = DataSpc
  | TypeSpc
  deriving (Eq)


data ScopeInferError
  = UndefVar Addr
  | UndefCon Addr
  | ScopeClash (String , Addr) Addr
  deriving (Eq)


instance Show ScopeInferError where
  
  show (UndefVar addr)
    = formatError ScopeInference addr $
      "Undefined variable"
  show (UndefCon addr)
    = formatError ScopeInference addr $
      "Unknown constructor"
  show (ScopeClash (fn , oaddr) addr)
    = formatError ScopeInference addr $
      "Scope clash with " ++ 
      case oaddr of
        Unspec
          -> "built-in name"
        _
          -> "name introduced at " ++ show oaddr ++ " in " ++ fn
  

type UId
  = Int
type UIdEnv
  = Map String UId
type UIdMap
  = Map UId (String :@ Addr)

