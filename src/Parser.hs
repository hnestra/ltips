{-# LANGUAGE 
      FlexibleContexts, 
      GeneralizedNewtypeDeriving, 
      MultiParamTypeClasses, 
      FlexibleInstances, 
      TypeOperators
    #-}
module Parser 
  (
    module Parser.Data,
    module Parser.Indent,
    module Parser
  ) where


import Data.Int
import Control.Diapplicative
import Control.Diapplicative.Trans
import Utils (dummyCast)
import Syntax
import Parser.Data
import Parser.Indent
import Parser.Utils


newtype ParserT p e a
  = ParserT { runParserT :: WriterT Addr (StateT [Lexeme :@ (Addr , Addr)] p) e a }
  deriving (Functor, Bifunctor, Mixable, Triable, LazyTriable, Catenative, Dimonad, Writer Addr, State [Lexeme :@ (Addr , Addr)])

fellParserT
  :: ParserT p e a -> [Lexeme :@ (Addr , Addr)] -> p e ((a , Addr) , [Lexeme :@ (Addr , Addr)])
fellParserT p input
  = runStateT (runWriterT (runParserT p)) input


instance Transformer ParserT where
  
  lift
    = ParserT . lift . lift
  

instance (Dimonad p) => Parser Lexeme (Addr , Addr) (ParserT p) where
  
  consume pred
    = get `bind` \ ts@(~(t@(lex :@ addr@(orig , _)) : rest)) ->
      if null ts
        then raise EOF
        else case pred lex of
               Left e
                 -> raise (UnexpectedToken (e :@ addr))
               _
                 -> put rest `bind` \ _ ->
                    tell orig `bind` \ _ ->
                    invoke t
  

inDelims
  :: (Writer Addr p, Parser Lexeme Addr p)
  => p e (a' :@ Addr) -> p (RawError (lex :@ Addr)) (a' :@ Addr) -> p ParseError (a :@ Addr) -> p ParseError (a :@ Addr)
inDelims open close par
  = listenAddr $
    fusel $
      invoke (\ (_ :@ oaddr) (res :@ _) -> bimap (UnmatchedOpenDelim oaddr . failAddr) (const res)) ***
      (dummyCast "inDelims" //> open) ***
      par ***
      mixmap Right close

inParen
  :: (Writer Addr p, Parser Lexeme Addr p)
  => p ParseError (a :@ Addr) -> p ParseError (a :@ Addr)
inParen
  = inDelims (consumeIf isOpenParen) (consumeIf isCloseParen)

inBrace
  :: (Writer Addr p, Parser Lexeme Addr p)
  => p ParseError (a :@ Addr) -> p ParseError (a :@ Addr)
inBrace
  = inDelims (consumeIf isOpenBrace) (consumeIf isCloseBrace)


parseAll
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError (ProgAST String [InfixElem :@ Addr] [InfixElem :@ Addr] [InfixElem :@ Addr] :@ Addr)
parseAll
  = parseProg **~
    ifSucceeds isCloseBrace UnmatchedCloseDelim **~
    indentAt (rlx 0 Inf) (ifSucceeds true MisalignedDeclaration)

parseProg
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError (ProgAST String [InfixElem :@ Addr] [InfixElem :@ Addr] [InfixElem :@ Addr] :@ Addr)
parseProg
  = listenAddr $
    tokensAt ge $
      invoke Prog ***
      condStar (aligned $ consumeIf isInfix) (aligned parseInfix) ***
      condStar (aligned $ consumeIf isType) (aligned parseTyEq) ***
      condStar (aligned $ consumeIf (no isCloseBrace)) (aligned parseEq) **~
      ifSucceeds (no isCloseBrace) MisalignedDeclaration

parseBlock
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError (BlockAST [InfixElem :@ Addr] [InfixElem :@ Addr] :@ Addr)
parseBlock
  = listenAddr $ 
    tokensAt ge $
      invoke Block ***
      condStar (aligned $ consumeIf isInfix) (aligned parseInfix) ***
      condStar (aligned $ consumeIf (no isCloseBrace)) (aligned parseEq) **~
      ifSucceeds (no isCloseBrace) MisalignedDeclaration

parseInfix
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError (InfixDeclAST :@ Addr)
parseInfix
  = listenAddr $
    after (dummyCast "parseInfix" //> consumeIf isInfix) $
    tokensAt gt $
      invoke InfixDecl ***
      parsePrior ***
      parseOp ***
      parsePrior **~
      ifSucceeds (no isCloseBrace `andalso` no isInfix) ExtraToken

parsePrior
  :: (Parser Lexeme Addr p)
  => p ParseError (Int8 :@ Addr)
parsePrior
  = after (ifFails true EndOfSynUnit) $
      NotInt //>
      (failAddr //> consumeIf isNum) `pbind` \ (Lex _ str :@ addr) ->
      case findLegalPrior str of
        Just n
          -> Right (fromInteger n :@ addr)
        _
          -> Left addr

parseOp
  :: (Parser Lexeme Addr p)
  => p ParseError (String :@ Addr)
parseOp
  = after (ifFails true EndOfSynUnit) $
      NotOp . failAddr //> bimap source id **> consumeIf (isOpVar `orelse` isOpCon)

parseTyEq
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError (TyEqAST (ConAST String :@ Addr) [InfixElem :@ Addr] :@ Addr)
parseTyEq
  = listenAddr $
    after (dummyCast "parseType" //> consumeIf isType) $
      invoke TyEq ***
      parseTyCon ***
      parseRHS

parseTyCon
  :: (Writer Addr p, Parser Lexeme Addr p)
  => p ParseError (ConAST String :@ Addr)
parseTyCon
  = after (ifFails (isFunCon `orelse` isOpenParen) TyConExpected) $
      (dummyCast "parseTyCon, 1" //> bimap (Con . source) id **> consumeIf isFunCon) ~//
      inParen parseExtTyCon

parseExtTyCon
  :: (Writer Addr p, Parser Lexeme Addr p)
  => p ParseError (ConAST String :@ Addr)
parseExtTyCon
  = after (ifFails (isFunCon `orelse` isOpCon `orelse` isOpVar `orelse` isOpenParen) TyConExpected) $
      (dummyCast "parseTyCon, 2" //> bimap (Con . source) id **> consumeIf (isFunCon `orelse` isOpCon `orelse` isOpVar)) ~//
      inParen parseExtTyCon

parseEq
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError (EqAST [InfixElem :@ Addr] [InfixElem :@ Addr] :@ Addr)
parseEq
  = listenAddr $ 
      invoke Eq ***
      parseLHS ***
      parseRHS

parseLHS
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError [InfixElem :@ Addr]
parseLHS
  = tokensAt gt parseInfixSeq

parseInfixSeq
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError [InfixElem :@ Addr]
parseInfixSeq
  = alternate (concat **> condPlus (consumeIf (no isInfixSeqStop)) (invoke (++) *** parseLocalSeq *** parseArgSeq)) parseInfixElem

parseLocalSeq
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError [InfixElem :@ Addr]
parseLocalSeq
  = condStar (consumeIf isOpenBrace) (bimap EscElem id **> inBrace (indentAt gt parseBlock))

parseArgSeq
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError [InfixElem :@ Addr]
parseArgSeq
  = condPlus (consumeIf (no isInfixSeqStop `andalso` no isOpenBrace)) parseArg

-- Assumes that no open brace occurs at the current point
parseArg
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError (InfixElem :@ Addr)
parseArg
  = after (ifFails true EndOfSynUnit) $
    after (ifSucceeds isBracket MeaninglessDelim) $
    after (ifSucceeds isInfixSeqStop MissingSynUnit) $
    after (ifSucceeds isKey UnexpectedKey) $
      inParen parseParenContent //~
      bimap (\ (Lex (Atom cat) nam) -> FunElem (FunAtom cat nam)) id **> consumeIf isAtom

parseParenContent
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError (InfixElem :@ Addr)
parseParenContent
  = after (ifSucceedsSeq [isLamDot, isCloseParen] PrefixLamDot) $
      bimap Sub id **> listenAddr parseInfixSeq //~
      (dummyCast "parseParenContent" //> bimap (\ (Lex (Op cat) nam) -> PrefixElem (OpAtom cat nam)) id **> consumeIf isOp)

parseInfixElem
  :: (Parser Lexeme Addr p) 
  => p ParseError (InfixElem :@ Addr)
parseInfixElem
  = dummyCast "parseInfixElem" //> 
    bimap (\ (Lex (Op cat) nam) -> InfixElem (OpAtom cat nam)) id **> 
    consumeIf isOp

parseRHS
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError [InfixElem :@ Addr]
parseRHS
  = after (ifFails true EndOfSynUnit) $
    after (ifSucceeds isClose UnmatchedCloseDelim) $
    fusel $
      invoke (\ (_ :@ oaddr) res -> bimap (\ (_ :@ addr) -> ExtraEq oaddr addr) (const res)) ***
      (NotEq . failAddr //> consumeIf isEq) *** 
      tokensAt gt parseInfixSeq **~
      ifSucceeds isCloseParen UnmatchedCloseDelim ***
      mixmap Right (negation $ consumeIf isEq)

parseCmdLine
  :: (Writer Addr p, Parser Lexeme Addr p, Indenter p)
  => p ParseError [InfixElem :@ Addr]
parseCmdLine
  = parseInfixSeq **~
    ifSucceeds isClose UnmatchedCloseDelim **~
    ifSucceeds isKey UnexpectedKey
