{-# LANGUAGE
      TypeOperators
    #-}
module Disambiguator.Data 
  ( module Disambiguator.Data
  ) where


import Data.Maybe
import Data.Utils
import Address
import Phase
import Syntax



data OutStkElem
  = Node (OpAtom :@ Addr)
  | OrdArg (InfixTree :@ Addr)
  | EscArg (BlockAST (InfixTree :@ Addr) (InfixTree :@ Addr) :@ Addr)
type InStkElem
  = InfixElem :@ Addr


findNode
  :: OutStkElem -> Maybe (OpAtom :@ Addr)
findNode (Node atom)
  = Just atom
findNode _
  = Nothing

findOrdArg
  :: OutStkElem -> Maybe (InfixTree :@ Addr)
findOrdArg (OrdArg tree)
  = Just tree
findOrdArg _
  = Nothing

findEscArg
  :: OutStkElem -> Maybe (BlockAST (InfixTree :@ Addr) (InfixTree :@ Addr) :@ Addr)
findEscArg (EscArg block)
  = Just block
findEscArg _
  = Nothing

addrOf
  :: OutStkElem -> Addr
addrOf (Node (_ :@ addr))
  = addr
addrOf (OrdArg (_ :@ addr))
  = addr
addrOf (EscArg (_ :@ addr))
  = addr

withAddr
  :: Addr -> OutStkElem -> OutStkElem
withAddr addr (Node node)
  = Node (const addr **> node)
withAddr addr (OrdArg arg)
  = OrdArg (const addr **> arg)
withAddr addr (EscArg arg)
  = EscArg (const addr **> arg)

buildTree
  :: OpAtom :@ Addr -> OutStkElem -> OutStkElem -> InfixTree
buildTree (OpAtom JP nam :@ _) arg1 arg2
  | nam == ""
    = AppBranch (fromJust (findOrdArg arg1)) (fromJust (findOrdArg arg2))
  | nam == "{}"
    = LocalBranch (fromJust (findEscArg arg1)) (fromJust (findOrdArg arg2))
buildTree node                       arg1 arg2
  = OrdBranch node (fromJust (findOrdArg arg1)) (fromJust (findOrdArg arg2))


data DisambiguationError
  = MultipleInfixDecl Addr Addr
  | UndeclaredOp Addr
  | PriorityClash Addr Addr
  deriving (Eq)


instance Show DisambiguationError where
  
  show (MultipleInfixDecl oaddr addr)
    = formatError Disambiguation addr $
      "Infix operator already declared in this block at " ++ show oaddr
  show (UndeclaredOp addr)
    = formatError Disambiguation addr $
      "Undeclared infix operator"
  show (PriorityClash oaddr addr)
    = formatError Disambiguation addr $
      "Unresolved priority clash with infix operator at " ++ show oaddr
  

