{-# LANGUAGE 
      FlexibleContexts,
      TypeOperators,
      GeneralizedNewtypeDeriving
    #-}
module Disambiguator.Infix
  ( module Disambiguator.Infix
  ) where


import Control.Diapplicative
import Control.Diapplicative.Trans
import Stack
import Address
import Disambiguator.Data


newtype InfixT p e a
  = InfixT { runInfixT :: WriterT Addr (DblStkT OutStkElem (DblStkT InStkElem p)) e a }
  deriving (Functor, Bifunctor, Triable, LazyTriable, Catenative, Mixable, Dimonad, Writer Addr)

fellInfixT
  :: InfixT p e a -> [[InStkElem]] -> [[OutStkElem]] -> p e (((a , Addr) , [[OutStkElem]]) , [[InStkElem]])
fellInfixT p instk outstk
  = fellDblStkT (fellDblStkT (runWriterT (runInfixT p)) outstk) instk


instance Transformer InfixT where
  
  lift
    = InfixT . lift . lift . lift
  

applyOutStk
  :: (Bifunctor p)
  => DblStkT OutStkElem (DblStkT InStkElem p) e a -> InfixT p e a
applyOutStk
  = InfixT . lift

applyInStk
  :: (Bifunctor p)
  => DblStkT InStkElem p e a -> InfixT p e a
applyInStk
  = InfixT . lift . lift

popAndTell
  :: (Dimonad p)
  => InfixT p StackError OutStkElem
popAndTell
  = (applyOutStk $ pop) `bind` \ tree ->
    tell (addrOf tree) `bind` \ _ ->
    invoke tree
